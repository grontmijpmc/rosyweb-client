const path = require('path');
//const AssetsPlugin = require('assets-webpack-plugin');
const AssetsPlugin = require('webpack-assets-manifest');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    // Husk at tilføje polyfills her til brug for IE
    entry: './src/index.tsx',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: 'http://localhost:3000/dist/',
    },
    plugins: [
        //new AssetsPlugin({ filename: 'c:/code/RoSyWeb/RoSyWeb.Web/Content/clientside/assets.json' }),
        new AssetsPlugin({ writeToDisk: true, output: 'c:/code/RoSyWeb/RoSyWeb.Web/Content/clientside/assets.json', publicPath: true }),
/*        new UglifyJSPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
        })
        */
    ],
    module: {
        rules: [
            { test: /\.tsx?$/, use: 'ts-loader', exclude: /node_modules/ },
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                ],
            },
            {
                test: /\.less$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    {
                        loader: "less-loader",
                        options: {
                            strictMath: true,
                        }
                    },
                ],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
                loader: "file-loader",
            },
            { test: /\.png$/, loader: 'url-loader?limit=100000' },
            { test: /\.gif$/, loader: 'url-loader?limit=100000' },
            { test: /\.jpg$/, loader: 'url-loader?limit=100000' },
        ],
    },
    resolve: {
        plugins: [new TsConfigPathsPlugin({ configFile: './tsconfig.json'})],
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    externals: {
        'configuration': 'configuration',
        'localizer': 'jsLocalize',
        'openlayers': 'ol',
    },
    devtool: 'cheap-module-eval-source-map',
}