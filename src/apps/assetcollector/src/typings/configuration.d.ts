/// <reference path="../../../../../node_modules/@types/openlayers/index.d.ts" />


interface MapConfiguration {
    openLayers: any;
    layers: { () : JQueryPromise<ol.layer.Layer> }
}

interface Configuration {
    mapConfiguration: MapConfiguration;
}

declare const configuration: Configuration;

declare module 'configuration' {
    export = configuration;
}
