import { observable, action, computed, asFlat, IObservableArray } from 'mobx';
import ol from '@rosy/utils-openlayers';
import { AssetToDraw } from './../types';

export class MapStore {

    @observable extent: ol.Extent | null = null;
    @observable assetToDraw: AssetToDraw | null = null;

    @action setBounds(bounds: ol.Extent) {
        this.extent = bounds;
    }

    @action setAssetToDraw(assettodraw: AssetToDraw) {
        debugger;
        this.assetToDraw = assettodraw;
    }

}

export default MapStore;