import * as React from 'react';
import * as $ from 'jquery';
import { observer, Provider } from 'mobx-react';
import { debounce } from 'lodash-es';
import MapComponent from './components/map/map';
import MapRoadAutocompleter from './components/roadautocompleter/maproadautocompleter';
import { Splitter, Pane } from '@rosy/component-splitter';
import MenuBottom from './components/menubottom';

import store from './store';
import mapStore from './stores/map';
import Actions from './actions';
const actions = new Actions(store);
import TemplatePanel from './components/templatepanel';
import { relative } from 'path';

import { NavBarBottom } from '@rosy/component-navbarbottom';
import { Button } from 'react-bootstrap';

/*
import MapComponent from './components/map/newmap2';
import Legend from './components/legend';
import Menu from './components/menu';

import Actions from './actions';
const actions = new Actions(store);
import * as types from './types';

import Roadlist from './components/roadlist';
import Filter from './components/filter/filter';
import Leftbar from './layout/leftbar';
import MainTabs from './layout/maintabs';
import FilterWorkingset from './components/filter/workingset/workingset';

*/

function updatePaneSizes() {
    const $window = $(window);
    const element = $(".k-splitter:first");
    const registrationWidth = element.children(".k-pane:last").width() || 0;
    const registrationHeight = $window.height() || 0;

    const registration = {
        visible: true,
        size: {
            width: registrationWidth,
            height: registrationHeight,
        },
    };

    const mapElements = element.children(".k-pane:first");
    const mapAndWindowsWidth = mapElements.width() || 0;

    const innerPanes = $(".k-pane", mapElements);

    const mapElement = innerPanes.first();

    const mapHeight = mapElements.height() || 0;

    const map = {
        visible: true,
        size: {
            width: mapAndWindowsWidth,
            height: mapHeight,
        },
    };

    const windowsHeight = (innerPanes
        .filter((index, el) => (index > 0))
        .map((index, el) => $(el).height())
        .get() as any) as number[];

    const windowDimensions = windowsHeight.map(h => ({
        visible: true,
        size: {
            width: mapAndWindowsWidth,
            height: h,
        },
    }));

    actions.setPaneSizes({
        registration,
        map,
        windows: windowDimensions,
    });
}

const debouncedUpdatePaneSizes = debounce(updatePaneSizes, 400);

function updateWindowDimensions() {
    const $window = $(window);
    const width = $window.width();
    const height = $window.height();

    if (width && height) {
        actions.setWindowDimensions({ width, height });
        updatePaneSizes();
    }
}

const debouncedUpdateWindowDimension = debounce(updateWindowDimensions, 400);

$(window).ready(() => {
    updateWindowDimensions();
    const splitters = $('.k-splitter');
    splitters.each((index, element) => {
        const splitter = $(element).data('kendoSplitter');
        splitter.resize(true);
    });

    $(window).resize(debouncedUpdateWindowDimension);
});

@observer
class Application extends React.Component<{}, {}> {
    // tslint:disable-next-line:no-empty
    onclickDummy = () => {
    }

    render() {
        const autostyle = {
            width: '100%',
        };
        return (
            <Provider actions={actions} ui={store.components.ui}>
                <Splitter orientation="horizontal" >
                    <Pane size="80%">
                        <MapComponent store={store.components.mapStore} />
                        <MapRoadAutocompleter />
                        <MenuBottom/>
                    </Pane>
                    <Pane>
                        <p className="font-weight-bold">Select asset type to register</p>
                        <TemplatePanel />
                    </Pane>
                </Splitter>
            </Provider>
        );
    }
}

/*
 <Provider actions={actions} ui={store.components.ui}>
                <Splitter orientation="horizontal" onResize={debouncedUpdatePaneSizes}>
                    <Pane size="20%">
                        <Leftbar
                            leftbarStore={store.components.leftBar}
                            filterStore={store.components.filter}
                            roadlistStore={store.components.roadListStore}
                        />
                    </Pane>
                    <Pane>
                        <Splitter orientation="vertical" onResize={debouncedUpdatePaneSizes}>
                        <Pane>
                                <MapComponent store={store.components.mapStore} />
                                    <Menu />
                                    <Legend store={store.components.mapStore} />
                                </Pane>
                            <Pane>
                                    <MainTabs
                                        windowManager={store.windowManager}
                                        selectedPanel={store.components.leftBar.selectedPanel}
                                    />
                                </Pane>
                        </Splitter>
                        {store.components.filter.workset.visible
                            && <FilterWorkingset store={store.components.filter.workset} />}
                    </Pane>
                </Splitter>
            </Provider>
*/

export default Application;
