import * as React from 'react';
import { autorun, observe, IArrayChange, IArraySplice, IObservableArray, reaction } from 'mobx';
import { observer, inject } from 'mobx-react';
import ol from '@rosy/utils-openlayers';
import MapComponent from '@rosy/component-map';

import { MapStore } from '../../stores/map';
import UI from '../../stores/ui';
import Actions from '../../actions';

import * as  configuration from 'configuration';

export interface Properties {
    store: MapStore;
    ui?: UI;
    actions?: Actions;
}

type Disposer = () => void;

@inject('ui', 'actions') @observer
export default class Map extends React.Component<Properties, {}> {

    private layers?: ol.Collection<ol.layer.Layer>;
    private view?: ol.View;
    private extent?: ol.Extent;
    private drawLayerSource: ol.source.Vector;
    private drawLayer: ol.layer.Vector;
    private numberOfBaseLayers = 0;
    private drawInterAction: ol.interaction.Draw | undefined;

    private disposers: Disposer[] = [];
    private selectInteraction: ol.interaction.Select = new ol.interaction.Select();

    map: MapComponent | null | undefined;

    constructor(props: Properties) {
        super(props);
        const layers = [];
        for (const layer of Object.keys(configuration.mapConfiguration.openLayers.layers)) {
            layers.push(configuration.mapConfiguration.openLayers.layers[layer]);
        }
        this.drawLayerSource = new ol.source.Vector();
        const style = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#000000',
                width: 3,
            }),
            image: new ol.style.Circle({
                radius: 3,
                fill: new ol.style.Fill({ color: '#000000' }),
                stroke: new ol.style.Stroke({ color: '#000000', width: 1 }),
            }),
        });
        this.drawLayer = new ol.layer.Vector({ source: this.drawLayerSource, style });
        layers.push(this.drawLayer);

        this.numberOfBaseLayers = layers.length;

        this.layers = new ol.Collection<ol.layer.Layer>(layers);
        this.view = new ol.View(configuration.mapConfiguration.openLayers.view);
        this.extent = this.props.store.extent!;

    }

    componentDidMount() {
        const selectstyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#118e88',
                width: 3,
            }),
            image: new ol.style.Circle({
                radius: 3,
                fill: new ol.style.Fill({ color: '#118e88' }),
                stroke: new ol.style.Stroke({ color: '#118e88', width: 3 }),
            }),
        });

        this.disposers.push(autorun(() => {
            let extent: [number, number, number, number];
            if (this.props.store.extent) {
                extent = this.props.store.extent.slice() as [number, number, number, number];
                this.map!.zoomToExtent(extent);
            }
        }));

        this.disposers.push(reaction(() => this.props.ui!.panes.map.size, () => {
            this.map!.updateSize();
        }));

        this.disposers.push(autorun(this.drawNewPoint));

        const geolocation = new ol.Geolocation({
            projection: this.map!.map!.getView().getProjection(),
            tracking: true,
            trackingOptions: {
                enableHighAccuracy: true,
                maximumAge: 2000,
            },
        });
        geolocation.on('change', () => {
            const pos = geolocation.getPosition();
            this.view!.setCenter(pos);
        });

        this.map!.map!.on('movestart', (event: ol.ObjectEvent) => {
            const element = document.createElement('span');
            element.innerText = 'X';
            element.style.position = 'absolute';
            element.style.top = '500px';
            element.style.left = '500px';
            const custcontrol = new ol.control.Control({
                element,
            });
            this.map!.map!.addControl(custcontrol);
        });
        this.map!.map!.on("moveend", (event: ol.ObjectEvent) => {
            console.log("stop moving", event.oldValue);
        });
        /*  this.view!.on("change:center", (event: ol.ObjectEvent) => {
              const oldpos = event.oldValue as [number, number] ;
              const newpos = this.view!.getCenter();
              const deltaX = oldpos[0] - newpos[0];
              const deltaY = oldpos[1] - newpos[1];

              for (const feature of this.drawLayerSource.getFeatures()) {
                  const geom = feature.getGeometry() as ol.geom.Point;
                  const coord = geom.getCoordinates();
                  const newX = coord[0] - deltaX;
                  const newY = coord[1] - deltaY;
                  feature.setGeometry(new ol.geom.Point([newX, newY]));
              }
              if (this.drawLayerSource.getFeatures().length > 0) {
                  this.drawLayerSource.changed();
                  event.stopPropagation();
                  console.log("refresher");
              }
          });
          */
    }

    componentWillUnmount() {
        for (const disposer of this.disposers) {
            disposer();
        }

        this.map!.unMount();
    }

    drawNewPoint = () => {
        const newdrawPoint = this.props.store.assetToDraw;
        /*    if (newdrawPoint) {
                console.log(newdrawPoint!.model);
                const newPoint = new ol.geom.Point(this.view!.getCenter());
                const feature = new ol.Feature({
                    geometry: newPoint,
                });

                this.drawLayerSource.addFeature(feature);
                const features = this.drawLayerSource.getFeatures();
                debugger;
            }
            */
        if (newdrawPoint != null) {
            debugger;
            this.drawInterAction = new ol.interaction.Draw({
                source: this.drawLayerSource,
                type: ('Point'),
            });
            this.map!.addInteraction(this.drawInterAction);
        }

        if (this.drawInterAction) {
            this.drawInterAction!.on('drawend', () => {
                if (this.drawInterAction) {
                    this.map!.removeInteraction(this.drawInterAction);
                }
            });
        }
    }

    render() {
        return (
            <MapComponent
                layers={this.layers!}
                view={this.view!}
                extent={this.extent!}
                ref={el => this.map = el}
            />
        );
    }
}