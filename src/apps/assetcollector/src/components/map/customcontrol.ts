import ol from '@rosy/utils-openlayers';

class CustomControl extends ol.control.Control {
    private topposition: number;
    private leftposition: number;

    constructor(top: number, left: number, options: olx.control.ControlOptions) {
        super(options);
        this.topposition = top;
        this.leftposition = left;
    }


}