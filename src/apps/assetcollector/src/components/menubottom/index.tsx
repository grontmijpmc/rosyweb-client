import * as React from 'react';
import { NavBarBottom } from '@rosy/component-navbarbottom';
import { Button } from 'react-bootstrap';

export default class MenuBottom extends React.Component<{}, {}> {

    render() {
        return (
            <NavBarBottom>
                <Button>Menu</Button>
            </NavBarBottom>
        );
    }
}