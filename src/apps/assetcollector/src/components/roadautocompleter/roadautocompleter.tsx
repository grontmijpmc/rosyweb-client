import * as React from 'react';
import request from '@rosy/utils-request';
import * as localizer from 'localizer';
import '@progress/kendo-ui/js/kendo.data.js';
import '@progress/kendo-ui/js/kendo.autocomplete.js';
import * as $ from 'jquery';

interface Properties {
    onSelect: (e: kendo.ui.AutoCompleteSelectEvent) => void;
}

class RoadAutocompleter extends React.Component<Properties, {}> {

    private autocompleterRef: Element | null | undefined;

    componentDidMount() {
        const ds = new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: ((options: kendo.data.DataSourceTransportReadOptions) => {
                    // kendo ui typings is wrong
                    const requestTerm = (options.data.filter!.filters![0] as any).value || '_';

                    request.ajax({
                        url: '/roadautocompleters/filterroadautocomplete?term=' + requestTerm,
                        type: 'json',
                    }).then((data: any) => {
                        options.success(data);
                    }).catch((e: any) => {
                        options.error('Fail :-(');
                         /* tslint:disable-next-line:no-console */
                        console.error(e);
                    });
                // kendo typings are wrong as of 05-10-2017. Remove any cast when typings are ok
                }) as any,
            },
        });

        $(this.autocompleterRef!).kendoAutoComplete({
            select: this.props.onSelect,
            dataTextField: 'roadName',
            dataSource: ds,
        });
    }

  componentWillUnmount() {
      if (this.autocompleterRef) {
          const $el = $(this.autocompleterRef);
          $el.data('kendoAutoComplete').destroy();
      }
  }

  render() {
      return (
          <div className="maproadautocompleter__container">
              <input
                  title={localizer.FindRoad}
                  className="maproadautocompleter__input"
                  placeholder={localizer.FindRoad}
                  ref={ref => this.autocompleterRef = ref}
              />
          </div>
      );
  }
}

export default RoadAutocompleter;