import * as React from 'react';
import { inject } from 'mobx-react';
import RoadAutocompleter from './RoadAutocompleter';

import Actions from '../../actions';

interface Properties {
    actions?: Actions;
}

@inject('actions')
class MapRoadAutocompleter extends React.Component<Properties, {}> {

    roadSelected = (e: kendo.ui.AutoCompleteSelectEvent) => {
        if (e.item) {
            const selectedRoadId = e.sender.dataItem(e.item.index()).id;
            this.props.actions!.zoomToRoad(selectedRoadId);
        }
    }

    render() {
        return <RoadAutocompleter onSelect={this.roadSelected} />;
    }
}

export default MapRoadAutocompleter;
