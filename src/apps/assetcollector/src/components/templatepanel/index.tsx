import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { useStrict } from 'mobx';
import { inject } from 'mobx-react';
import { PanelGroup, Panel } from 'react-bootstrap';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import Actions from './../../actions';

export interface Properties {
    actions?: Actions;
}

@inject('actions')
class TemplatePanel extends React.Component<Properties, {}> {
    signClicked = () => {
        this.props.actions!.signOnClick();
    }

    render() {
        return (
            <PanelGroup id='templates' accordion={true} >
                <Panel eventKey="1">
                    <Panel.Heading>
                        <Panel.Title toggle={true}>Recent</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body collapsible={false}>
                        <ButtonToolbar bsSize="large">
                            <Button onClick={this.signClicked}>Sign</Button>
                            <Button >Gully</Button>
                        </ButtonToolbar>
                        <ButtonToolbar bsSize="large" className="btn-toolbar">
                            <Button >Crash barrier</Button>
                            <Button >Lightpost</Button>
                        </ButtonToolbar>
                    </Panel.Body>
                </Panel>
                <Panel eventKey="2">
                    <Panel.Heading>
                        <Panel.Title toggle={true}>Group 1</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body collapsible={true}>Group 1</Panel.Body>
                </Panel>
                <Panel eventKey="3">
                    <Panel.Heading>
                        <Panel.Title toggle={true}>Group 2</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body collapsible={true}>Group 2</Panel.Body>
                </Panel>
            </PanelGroup>
        );
    }
}
export default TemplatePanel;