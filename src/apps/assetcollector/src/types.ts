import reqwest from '@rosy/utils-request';

enum GuidBrand { }
export type Guid = GuidBrand & string;

enum RoadIdBrand { }
export type RoadId = RoadIdBrand & Guid;

type Method = "GET" | "POST";

export interface RequestDefinition {
    url: string;
    method?: Method;
    headers?: object;
    data?: string | object;
    type?: string;
    contentType?: string;
    crossOrigin?: boolean;
    jsonpCallback?: string;
}

type DrawType = "Point" | "Line";

export interface AssetToDraw {
    model: string;
    drawType: DrawType;

}
