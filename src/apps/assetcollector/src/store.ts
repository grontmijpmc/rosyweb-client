import { action, runInAction } from 'mobx';
import { MapStore} from './stores/map';
import UI from './stores/ui';
import request from '@rosy/utils-request';

export interface Components {
    mapStore: MapStore;
    ui: UI;
}
interface InitialData {
    initialExtent: { x1: number, y1: number, x2: number, y2: number };
}

export class Store {
    components: Components;

    constructor() {
        this.components = {
            mapStore: new MapStore(),
            ui: new UI(),
        };
    }

init() {
    this.setDefaultBounds();
}

@action async setDefaultBounds() {
    const data = await request.ajax<InitialData>({
        url: '/overview/initial-data',
        type: 'json',
    });

    runInAction(() => {
        const initialExtent = data.initialExtent;
        const bounds: ol.Extent = [initialExtent.x1, initialExtent.y1, initialExtent.x2, initialExtent.y2];
        this.components.mapStore.setBounds(bounds);
    });
}

}

export default new Store();