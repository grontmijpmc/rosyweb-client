import { Store } from './store';
import { action, runInAction } from 'mobx';
import * as types from './types';
import * as roadService from './services/roadservice';
import { PaneKey, Pane, Panes, Dimensions } from './stores/ui';

export default class Actions {

    store: Store;

    constructor(store: Store) {
        this.store = store;
    }

    @action setPaneSize(pane: PaneKey, size: Pane) {
        this.store.components.ui.setPaneSize(pane, size);
    }

    @action setPaneSizes(panes: Panes) {
        this.store.components.ui.setPaneSizes(panes);
    }

    @action setWindowDimensions(dimensions: Dimensions) {
        this.store.components.ui.windowDimensions = dimensions;
    }

    @action async zoomToRoad(id: types.RoadId) {
        const bounds = await roadService.getRoadBounds(id);
        runInAction(() => {
            this.store.components.mapStore.setBounds(bounds);
        });
    }

    @action signOnClick() {
        this.store.components.mapStore.setAssetToDraw( { model: 'sign', drawType: 'Point' } );
        console.log("sign clicked");
    }

}