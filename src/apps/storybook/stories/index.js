import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Button from '@rosy/component-button';

require('@rosy/component-style-bootstrap');

storiesOf('Button', module)
    .add('with text', () => (
        <Button onClick={action('clicked')}>Hello button!</Button>
    ));
