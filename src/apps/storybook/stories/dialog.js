
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Dialog from '@rosy/component-dialog';

require('@rosy/component-style-bootstrap');

storiesOf('Dialog', module)
    .add('default', () => (
        <Dialog>
            <Dialog.Header>
                Header
            </Dialog.Header>
            <Dialog.Body>
                <p>
                    Test
                </p>
            </Dialog.Body>
        </Dialog>
    ));
