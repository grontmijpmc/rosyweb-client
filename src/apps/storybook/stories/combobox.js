import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Combobox from '@rosy/component-combobox';

const data = ["Value 1", "Value 2", "Value 3"];

storiesOf('Combobox', module)
    .add('plain', () => (
        <Combobox dataSource={data}/>
    ));
