const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

const webpackServer = new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    hot: true,
    contentBase: __dirname,
    quiet: false,
    noInfo: false,
    stats: { colors: true },
    historyApiFallback: true,
    headers: {
         'Access-Control-Allow-Origin': '*'
    }
});

/* eslint-disable no-console */
webpackServer.listen(3000, '0.0.0.0', function (err) {
    if (err) {
        console.log(err);
    }

    console.log('Webpack server listening at 0.0.0.0:3000');
});
