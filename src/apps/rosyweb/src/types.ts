import reqwest from '@rosy/utils-request';

enum GuidBrand { }
export type Guid = GuidBrand & string;

enum RoadIdBrand { }
export type RoadId = RoadIdBrand & Guid;

enum RoadKeyBrand {}
export type RoadKey = RoadKeyBrand & string;

export interface Road {
    id: RoadId;
    roadKey: RoadKey;
    roadName: string;
}

enum WktBrand {}
export type Wkt = WktBrand & string;

enum CenterlineBrand {}
export type Centerline = CenterlineBrand & Wkt & string;

export interface RoadWithCenterline extends Road {
    centerline: Centerline;
}

type Method = "GET" | "POST";

export interface RequestDefinition {
    url: string;
    method?: Method;
    headers?: object;
    data?: string | object;
    type?: string;
    contentType?: string;
    crossOrigin?: boolean;
    jsonpCallback?: string;
}

enum DataWindowKeyBrand { }
export type DataWindowKey = DataWindowKeyBrand & string;

enum DataWindowCategoryBrand { }
export type DataWindowCategory = DataWindowCategoryBrand & string;

/* Identifier for a DataWindow
 *
 * The category is the main category, e.g. DIG, ROOA, STREETLIGHT
 * The key is a unique key under each category. It should be treated
 * an a opaque identifier. It will ususually either a filtername or a guid
 */
export interface DataWindow {
    category: DataWindowCategory;
    key: DataWindowKey;
}

export interface DataWindowField {
    name: string;
    type: ServerColumnType;
    flags: string;
}

export interface DataWindowEnumField extends DataWindowField {
    allValues: string[];
}

export interface DataWindowRelationField extends DataWindowField {
    model: string;
}

export function isEnumField(field: DataWindowField): field is DataWindowEnumField {
    return (field as DataWindowEnumField).allValues !== undefined;
}

export function isRelationField(field: DataWindowField): field is DataWindowRelationField {
    return (field as DataWindowRelationField).model !== undefined;
}

export interface DataWindowModel {
    model: string;
    relatedModel: string;
    listView: string;
    editView: string;
    fields: DataWindowField[];
}

export interface SelectedMapFeature {
    id: Guid;
    category: DataWindowCategory;
}

export interface DataWindowStore {
    loadData(roadId: RoadId | null): void;
    setSelectedMapFeature(input: SelectedMapFeature): void;
    readonly dataSource: kendo.data.DataSource;
    title: string[];
}

export type DataWindows =
    'DIG'
    | 'ROOA';

export type ServerColumnType = 'String' | 'Relation' | 'Lookup' | 'Enum' | 'Boolean' | 'Date' | 'Number';
export type ServerColumnFlags = 'None' | 'Readonly' | 'Nullable' | 'OnlyInList';

export type SelectedPanel = 'ROADS' | 'MAPLAYERS';

interface RelationValue {
    id: Guid;
    name: string;
}

type FormValue = string | RelationValue;

interface FormValues {
    [key: string]: FormValue;
}

export interface FormValuesWithId extends FormValues {
    id: Guid;
}
