import { action, runInAction } from 'mobx';
import Filter from './stores/filter';
import MapStore from './stores/map';
import RoadlistStore from './stores/roadlist';
// import EditWindowStore from './stores/editwindow';
import LeftBarStore from './stores/leftbar';

import UI from './stores/ui';
import DataWindowManager from './datawindows/datawindowmanager';
// import FeatureDetailsStore from './stores/featuredetails';
import filterService from './services/filterservice';
import * as roadService from './services/roadservice';
import request from '@rosy/utils-request';
import * as types from './types';

export interface Components {
    filter: Filter;
    mapStore: MapStore;
    // featureDetailsStore: FeatureDetailsStore;
    roadListStore: RoadlistStore;
    ui: UI;
    // editWindow: EditWindowStore;
    leftBar: LeftBarStore;
}

interface InitialData {
    initialExtent: { x1: number, y1: number, x2: number, y2: number };
}

export class Store {

    components: Components;
    windowManager: DataWindowManager;

    constructor() {
        this.components = {
            filter: new Filter(),
            mapStore: new MapStore(),
            // featureDetailsStore: new FeatureDetailsStore(),
            roadListStore: new RoadlistStore(),
            ui: new UI(),
            // editWindow: new EditWindowStore(),
            leftBar: new LeftBarStore(),
        };
        this.windowManager = new DataWindowManager();
    }

    init() {
        this.setDefaultBounds();
        this.loadFilters();
        // this.setInitialApplicationUrl();
        this.loadRoads();
    }

    @action async setDefaultBounds() {
        const data = await request.ajax<InitialData>({
            url: '/overview/initial-data',
            type: 'json',
        });

        runInAction(() => {
            debugger;
            const initialExtent = data.initialExtent;
            const bounds: ol.Extent = [initialExtent.x1, initialExtent.y1, initialExtent.x2, initialExtent.y2];
            this.components.mapStore.setBounds(bounds);
        });
    }

    @action async loadFilters() {
        const data = await filterService.loadFilterList();
        runInAction(() => {
            for (let i = 0; i < data.length; i++) {
                const { group, category,  filters  } = data[i];

                this.components.filter.panel.addCategory(group, category);
                for (const filter of filters) {
                    this.components.filter.panel.addFilter({
                        category, filterName: filter.filterName,
                    }, group, filter.color, filter.editable);
                }

                        }
        });
    }

    @action async loadRoads() {
        const data: types.Road[] = await roadService.loadRoads();
        runInAction(() => {
            this.components.roadListStore.replaceRoads(data);
        });
    }
/*
    @action setInitialApplicationUrl() {
        for (const app of Object.keys(configuration.layerGroups)) {
            const { mainApplication } = configuration.layerGroups[app];
            if (mainApplication) {
                this.components.featureDetailsStore.initialApplicationUrl = mainApplication;
                break;
            }
        }
}
*/
}

export default new Store();