import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { useStrict } from 'mobx';
import Application from './application';

/*
 * Disable React hot loading until https://github.com/gaearon/react-hot-loader/issues/313 is resolved
import { AppContainer } from 'react-hot-loader';

useStrict(true);
import store from './store';
store.init();

require('../less/main.less');

const render = (Component: any)  => {
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer>,
        document.getElementById('container')
    )
}

render(Application);

if (module.hot) {
    module.hot.accept('./application', () => {
        const NextApp = require("./application").default;
        render(NextApp);
    })
}
*/

useStrict(true);
import store from './store';
store.init();

require('../style/main.less'); // tslint:disable-line:no-var-requires

ReactDOM.render(<Application />, document.getElementById('container'));