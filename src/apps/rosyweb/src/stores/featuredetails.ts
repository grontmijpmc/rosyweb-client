import { observable, action, IObservableArray } from 'mobx';
import * as configuration from 'configuration';

export class FeatureDetails {

    @observable fields: IObservableArray<string> = observable([]);
    @observable category = '';
    @observable headline: IObservableArray<string> = observable([]);
    @observable title = '';
    @observable id = '';
    @observable visible = false;
    @observable showCaseInParentApplication = false;
    @observable canShowInParentApplication = false;
    @observable initialApplicationUrl: string;

    @action showDetails(category: string, id: string, title: string, headline: string[], fields: string[]) {
        this.category = category;
        this.id = id;
        this.title = title;
        this.headline.replace(headline);
        this.fields.replace(fields);
        this.canShowInParentApplication = configuration.layerGroups[category].mainApplication !== undefined;
        this.visible = true;
    }

    @action close() {
        this.visible = false;
    }

    @action openParentApplication() {
        this.showCaseInParentApplication = true;
    }

    @action closeParentApplication() {
        this.showCaseInParentApplication = false;
    }
}

export default FeatureDetails;

