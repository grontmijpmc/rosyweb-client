import { observable } from 'mobx';
import * as types from '../types';

export default class LeftBarStore {
    @observable selectedPanel: types.SelectedPanel = 'MAPLAYERS';
}