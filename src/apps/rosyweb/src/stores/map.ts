import { observable, action, computed, asFlat, IObservableArray } from 'mobx';
import ol from '@rosy/utils-openlayers';
import * as types from '../types';
import { FilterIdentifier } from '../interfaces/filter';
import * as MapInterfaces from '../interfaces/map';
import filterService from '../services/filterservice';

export interface Layer {
    key: string;
    window: types.DataWindow;
    color: string;
    olLayer: ol.layer.Layer;
    name: { group: string, text: string };
}

function getDefaultStyle(color: string) {
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color,
            width: 3,
        }),
        image: new ol.style.Circle({
            radius: 3,
            fill: new ol.style.Fill({ color }),
            stroke: new ol.style.Stroke({ color, width: 1 }),
        }),
    });
}

interface SelectedRowInfo {
    window: types.DataWindow;
    id: string;
}

function createVectorSource(model: string): ol.source.Vector {
    const source = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: (extent: number[]) => `/base/data?View=Geometries&Model=${model}&BBox=${extent.join(',')}`,
        strategy: ol.loadingstrategy.bbox,
    });
    return source;
}

function createVectorLayer(source: ol.source.Vector, color: string): ol.layer.Vector {

    return new ol.layer.Vector({
        source,
        style: getDefaultStyle(color),
        // @ts-ignore: renderMode is missing from typings
        renderMode: 'image',
    });
}

export class MapStore {

    layers: IObservableArray<Layer> = observable.shallowArray<Layer>([]);
    @observable extent: ol.Extent | null = null;
    @observable selectedFeature: ol.Feature | null = null;
    @observable selectedRow: SelectedRowInfo | null | undefined;

    @action addLayerFromFilter(ident: FilterIdentifier, color: string) {
        const model = ident.category;
        const layer = createVectorLayer(createVectorSource(model), color);

        const data = {
            key: this._getKey(ident),
            window: {
                category: ident.category as types.DataWindowCategory,
                key: ident.filterName as types.DataWindowKey,
            },
            name: {
                group: filterService.getCategory(ident).group,
                text: ident.filterName,
            },
            color,
            olLayer: layer,
        };

        this.layers.push(data);
    }

    @action removeLayer(ident: FilterIdentifier) {
        const key = this._getKey(ident);
        const layer = this.layers.find(x => x.key === key);
        if (layer) {
            this.layers.remove(layer);
        }
    }

    @action removeDatawindowLayer(window: types.DataWindow) {
        const key = `datawindow_${window.category}_${window.key}`;
        const layer = this.layers.find(x => x.key === key);
        if (layer) {
            this.layers.remove(layer);
        }
    }

    @action setBounds(bounds: ol.Extent) {
        this.extent = bounds;
    }

    @action setSelectedFeature(olFeature: ol.Feature) {
        this.selectedFeature = olFeature;
    }

    @action unselectFeature() {
        this.selectedFeature = null;
    }

    @action selectedDataWindowRow(selectedRowInfo: SelectedRowInfo) {
        this.selectedRow = selectedRowInfo;
    }

    _getKey(ident: FilterIdentifier) {
        return `layer_${ident.category}¤${ident.filterName}`;
    }
}

export default MapStore;