import { observable, action } from 'mobx';

export interface Dimensions {
    width: number;
    height: number;
}

export interface Pane {
    visible: boolean;
    size: Dimensions;
}

export interface Panes {
    roadList: Pane;
    map: Pane;
    windows: Pane[];
}

export type PaneKey = keyof Panes;

const defaultSize = { width: 100, height: 100 };

export default class UI {

    @observable panes: Panes = {
        roadList: { visible: true, size: defaultSize },
        map: { visible: true, size: defaultSize },
        windows: [{ visible: true, size: defaultSize }],
    };
    @observable windowDimensions: Dimensions = { width: 0, height: 0 };

    @action setPaneSize(pane: PaneKey, dimensions: Pane) {
        this.panes[pane] = dimensions;
    }

    @action setPaneSizes(panes: Panes) {
        this.panes = panes;
    }
}