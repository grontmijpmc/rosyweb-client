import { observable, IObservableArray } from 'mobx';
import * as types from '../types';

import '@progress/kendo-ui/js/kendo.data';

class RoadListStore {
    @observable selectedRoad: types.RoadId | null = null;

    dataSource: kendo.data.DataSource;

    constructor() {
        this.dataSource = new kendo.data.DataSource({
            schema: {
                model: {
                    id: "id",
                    fields: {
                        roadKey: { type: 'string' },
                        roadName: { type: 'string' },
                    },
                },
            },
        });
    }

    replaceRoads(newRoads: types.Road[]) {
        const sortedList = newRoads.sort((a, b) => a.roadName.localeCompare(b.roadName));
        this.dataSource.data(newRoads);
    }
}

export default RoadListStore;
