import { observable, action, asMap, extendObservable, ObservableMap, map, IObservableArray } from 'mobx';
import * as uuid from 'node-uuid';
import * as _ from 'lodash';
import filterservice from '../services/filterservice';

import { FilterIdentifier, Property, WorkingsetProperty, ValueObject, PropertyDetails } from '../interfaces/filter';

export interface FilterItem extends FilterIdentifier {
    group: string;
    color: string;
    editable: boolean;
    enabled: boolean;
    loading: boolean;
}

type CurrentlyShowing = 'filters' | 'properties';

export class Panel {
    @observable currentlyShowing: CurrentlyShowing = 'filters';

    @observable allProperties = new ObservableMap<IObservableArray<Property>>();
    @observable filters = new ObservableMap<IObservableArray<FilterItem>>();
    @observable groupsandcategories = new ObservableMap<IObservableArray<string>>();

    @observable currentCategory: string | null = null;
    @observable properties: Property[] = [];
    @observable currentFilterName: string | null = null;

    @observable expandedAccordions: string[] = [];

    @action showFilters() {
        this.currentlyShowing = 'filters';
    }

    // Available categories. Added here instead of takes from e.g.
    // addFilter, as there might be categories without filters
    @action addCategory(group: string, category: string) {
        const groupitem = this.groupsandcategories.get(group);
        if (groupitem) {
            groupitem.push(category);
        } else {
            this.groupsandcategories.set(group, observable([category]));
        }

        this.filters.set(category, observable([]));
        this.allProperties.set(category, observable([]));
    }

    @action addFilter(ident: FilterIdentifier, group: string, color: string, editable: boolean) {
        // We must make sure that filters are in the correct order.
        // Ineditable filters are always first - that's "New actions" from e.g. dig.
        // After that comes user defined filters, sorted alphabetically
        const data: FilterItem = {
            category: ident.category,
            filterName: ident.filterName,
            group, color,
            editable,
            enabled: false,
            loading: false,
        };
        const filters = this.filters.get(ident.category);
        if (filters) {
            filters.push(data);
            filters.replace(this._sortFilters(filters));
        }
    }

    @action updateFilter(ident: FilterIdentifier, color: string) {
        const filters = this.filters.get(ident.category);
        if (filters) {
            const filterIndex = filters.findIndex(x => x.filterName === ident.filterName);
            filters[filterIndex].color = color;
        }
    }

    _sortFilters(filters: FilterItem[]) {
        const ineditableFilters = filters.filter(x => !x.editable);
        const editableFilters = filters.filter(x => x.editable)
            .sort((a, b) => (a.group + a.filterName)
                .localeCompare(b.group + b.filterName));
        return ineditableFilters.concat(editableFilters);
    }

    @action setFilterLoading(ident: FilterIdentifier) {
        const filter = this.getFilter(ident);
        filter.loading = true;
        filter.enabled = false;
    }

    @action setFilterEnabled(ident: FilterIdentifier) {
        const filter = this.getFilter(ident);
        filter.loading = false;
        filter.enabled = true;
    }

    @action setFilterDisabled(ident: FilterIdentifier) {
        const filter = this.getFilter(ident);
        filter.loading = false;
        filter.enabled = false;
    }

    isFilterEnabledOrLoading(ident: FilterIdentifier) {
        const filter = this.getFilter(ident);
        return filter.loading || filter.enabled;
    }

    getFilterColor(ident: FilterIdentifier) {
        return this.getFilter(ident).color;
    }

    getFilter(ident: FilterIdentifier) {
        return this.filters.get(ident.category)!.find(filter => filter.filterName === ident.filterName);
    }

    @action addProperty(category: string, property: Property) {
        property.key = uuid.v4();
        this.allProperties.get(category)!.push(property);
    }

    @action showPropertiesFor(category: string) {

        this.currentlyShowing = 'properties';
        this.currentCategory = category;
        this.properties = this.allProperties.get(category)!;

        if (this.properties.length === 0) {
            const fetchedprops = filterservice.loadProperties(category).then((props) => {
                for (const prop of props) {
                    this.addProperty(category, prop);
                }
            });
            this.properties = this.allProperties.get(category)!;
        }
    }

    accordionExpanded(name: string) {
        this.expandedAccordions.push(name);
    }

    accordionCollapsed(name: string) {
        const index = this.expandedAccordions.indexOf(name);
        if (index !== -1) {
            this.expandedAccordions.splice(index, 1);
        }
    }
}

export class Workset {
    @observable visible = false;
    @observable filterName: string = '';
    @observable category: string = '';
    @observable color: string = '';
    @observable isNew: boolean = false;
    @observable properties: IObservableArray<WorkingsetProperty> = observable([]);

    @action show(ident: FilterIdentifier, color: string) {
        this.visible = true;
        this.category = ident.category;
        this.filterName = ident.filterName;
        this.color = color;
        this.isNew = false;
        this.properties.clear();
    }

    @action showForNewFilter(category: string) {
        this.visible = true;
        this.category = category;
        this.filterName = '';
        this.color = '#000000'; // Default black. Can we do better?
        this.isNew = true;
        this.properties.clear();
    }

    @action hide() {
        this.visible = false;
    }

    @action setProperties(properties: WorkingsetProperty[]) {
        this.properties = observable(properties);
    }

    @action addProperty(property: { translatedName: string, path: string }) {
        const defaultObject = {
            fieldType: null,
            options: [],
            key: uuid.v4(),
            filterValue: null,
            operator: '==',
        };

        const clone = Object.assign({}, defaultObject, property);
        this.properties.push(clone);
    }

    @action removeProperty(propertyIndex: number) {
        this.properties.splice(propertyIndex, 1);
    }

    @action setPropertyDetails(propertyPath: string, propertyDetails: PropertyDetails) {
        for (const property of this.properties) {
            if (property.path === propertyPath) {
                property.fieldType = propertyDetails.fieldType;
                property.options = propertyDetails.options;
            }
        }
    }
}

class Filter {
    panel: Panel;
    workset: Workset;

    constructor() {
        this.panel = new Panel();
        this.workset = new Workset();
    }
}

export default Filter;


