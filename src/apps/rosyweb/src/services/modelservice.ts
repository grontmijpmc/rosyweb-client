import * as types from '../types';
import request from '@rosy/utils-request';

export async function read(model: string, id: types.Guid) {

    const data = await request.ajax<any>({
        url: '/base/data',
        type: 'json',
        data: {
            Model: model,
            View: 'read',
        },
    });

    return data;
}