import request from '@rosy/utils-request';

import * as ol from 'openlayers';
import * as types from '../types';

export function getRoadBounds(id: types.RoadId): Promise<ol.Extent> {
    return request.ajax<{x1: number, y1: number, x2: number, y2: number}>({
        url: '/base/road/bounds',
        data: [{ name: 'Id', value: id }],
        type: 'json',
    }).then(data => {
        return ([data.x1, data.y1, data.x2, data.y2] as ol.Extent);
    });
}

export function loadRoads(): Promise<types.Road[]> {
    return request.ajax<{ roads: types.RoadWithCenterline[]}>({
        url: '/base/roads/centerlines',
        type: 'json',
    }).then(data => {
        return data.roads;
    });
}
