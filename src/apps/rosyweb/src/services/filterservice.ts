import request from '@rosy/utils-request';
import * as uuid from 'node-uuid';
import { Promise as P } from 'bluebird';
import { assign, filter, map, isArray, includes } from 'lodash-es';

import {
    FilterIdentifier,
    PropertyIdentifier,
    Property,
    PropertyDetails,
    CaseDetails,
    WorkingsetProperty,
    Categoryfilter,
    FieldType,
} from '../interfaces/filter';

import * as MapInterfaces from '../interfaces/map';

import * as configuration from 'configuration';

interface FilterAsTextReturn {
    filterString: string;
}

interface CaseIdCache {
    [index: string]: string[];
}

const caseIdsPerFilter: CaseIdCache = {};

type Wkts = string[];

interface WktsPerId {
    [index: string]: MapInterfaces.Feature;
}

interface Features {
    headlines: string[];
    attributes: WktsPerId;
}

interface FeaturesLookup {
    [id: string]: MapInterfaces.Feature;
}

const featuresLookup: FeaturesLookup = {};

type Headlines = string[];
interface HeadlinesPerCategory {
    [category: string]: Headlines;
}

const headlinesPerCategory: HeadlinesPerCategory = {};

interface filterNamesPerCategory {
    [category: string]: string[];
}

let existingfilterNames: filterNamesPerCategory = {};

let  filterCache: CategoryWithFilters[] = [];

function getTextualRepresentation(ident: FilterIdentifier): Promise<string> {
    const filterConfig = configuration.layerGroups[ident.category].filter;

    if (filterConfig.specialFilters && filterConfig.specialFilters[ident.filterName]) {
        const filterString = filterConfig.specialFilters[ident.filterName].filterString;
        return P.resolve(filterString) as any;
    }

    return request.ajax<FilterAsTextReturn>({
        url: '/overview/filter/astext',
        data: [{name: 'FilterPath', value: ident.filterName}, {name: 'Category', value: ident.category}],
        type: 'json',
    }).then(result => {
        return result.filterString;
    });
}

function getPropertyDetails(ident: PropertyIdentifier): Promise<PropertyDetails> {
    return request.ajax<PropertyDetails>({
        url: configuration.layerGroups[ident.category].filter.propertyDetails,
        type: 'json',
        data: { FieldName: ident.propertyPath },
    })
    .then(data => {
        return {
            // fieldType is uppercase from server, but we only use lowercase on the client
            fieldType: ((data.fieldType as any).toLowerCase()) as FieldType,
            options: data.options,
            translatedName: data.translatedName,
        };
    });
}

function storeIdsMatchingFilter(ident: FilterIdentifier, ids: string[]) {
    caseIdsPerFilter[ident.category + '¤' + ident.filterName] = ids;
}

function findMissingIds(expectedIds: string[]) {
    return filter(expectedIds, id => !featuresLookup.hasOwnProperty(id) );
}

function storeFeatures(attributes: WktsPerId) {
    assign(featuresLookup, attributes);
}

function getCachedIdsPerfilter(ident: FilterIdentifier) {
    return caseIdsPerFilter[ident.category + '¤' + ident.filterName];
}

interface FilterIdsReturn {
    ids: string[];
}

interface GeometriesJson {
    type: string;
    crs: {
          type: string;
          properties: string[];
         };
    features: FeatureJson[];
}
interface FeatureJson {
    type: string;
    id: string;
    geometry: {
                type: string,
                coordinates: number[],
                };
    }

function disableFilter(ident: FilterIdentifier) {
    // We don't actually do anything yet, when af filter is disabled,
    // but I'd like to have a call here if it turns out to be necessary later
}

function getFeaturesById(ids: string[]): MapInterfaces.Feature[] {
    const result: MapInterfaces.Feature[] = [];
    for (let i = 0; i < ids.length; i++) {
        const id = ids[i];
        const value = featuresLookup[id];
        if (value) { // Skip features without geometry
            value.id = id;
            result.push(value);
        }
    }

    return result;
}

function getHeadlines(category: string) {
    return headlinesPerCategory[category];
}

function getCaseDetails(category: string, id: string): Promise<CaseDetails> {
    return request.ajax<CaseDetails>({
        url: `/overview/case-details/${category}/${id}`,
        type: 'json',
    });
}

interface LoadFilterReturn {
    details: PropertyDetails;
    propertyPath: string;
    operator: string; // Can this be more specific?
    filterValue: string;
}

function loadFilter(ident: FilterIdentifier): Promise<WorkingsetProperty[]> {
    const filter = getFilter(ident);
    return request.ajax<LoadFilterReturn[]>({
        url: '/base/data',
        data: { View: 'FilterToProperties', Model: ident.category, Filter: filter.filterString },
        type: 'json',
    }).then(data => {
        return map(data, prop => {
            return {
                translatedName: prop.details.translatedName,
                path: prop.propertyPath,
                fieldType: prop.details.fieldType,
                options: prop.details.options,
                operator: prop.operator,
                filterValue: prop.filterValue,
                key: uuid.v4(),
            };
        });
    });
}

function saveFilter(ident: FilterIdentifier, color: string, filterString: string) {

    const filter = getFilter(ident);
    filter.color = color;
    filter.filterString = filterString;

    const url = configuration.layerGroups[ident.category].filter.saveUrl;

    return request.ajax({
        url: 'client-side-config',
        data: { Model: ident.category, FilterName: ident.filterName, Filter: filterString, Color: color },
        type: 'json',
        method: "POST",
    });
}

interface ListPropertiesReturn {
    data: { title: string };
    attr: { filterpath: string };
    children: ListPropertiesReturn[];
}

function mapServerProperty(prop: ListPropertiesReturn): Property {
    let children: Property[] = [];
    if (prop.children) {
        children = prop.children.map(mapServerProperty);
    }
    return {
        translatedName: prop.data.title,
        path: prop.attr.filterpath,
        children,
    };
}

async function loadProperties(category: string): Promise<Property[]> {
    let result: Property[] =  [];
    let properties = await request.ajax<ListPropertiesReturn[]>({
        url: '/base/data',
        data: {
                Model: category,
                View: 'FilterProperties',
                },
        type: 'json',
    });
    if (!isArray(properties)) {
        properties = [];
    }
    result = map(properties, mapServerProperty);
    return result;
}

interface PredefinedFilter {
    filterName: string;
    color: string;
    editable: boolean;
}

interface FiltersPerCategory {
    [category: string]: PredefinedFilter[];
}

interface AllCategoriesWithFilter {
    all: CategoryWithFilters[];
}

interface CategoryWithFilters {
    group: string;
    category: string;
    filters: Categoryfilter[];
}

async function loadFilters(): Promise<CategoryWithFilters[]> {

    const categoryAndFilters = await request.ajax<CategoryWithFilters[]>({
        url: 'client-side-config',
        type: 'json',
    });
    return categoryAndFilters;
}

interface SpecialFiltersPerCategory {
    [index: string]: SpecialFilters;
}

function loadSpecialFilters(): SpecialFiltersPerCategory {
    const result: SpecialFiltersPerCategory = {};
    for (const key of Object.keys(configuration.layerGroups)) {
        const config = configuration.layerGroups[key].filter;
        if (config.specialFilters) {
            result[key] = config.specialFilters;
        }
    }
    return result;
}

async function loadFilterList(): Promise<CategoryWithFilters[]> {
    const filters = await loadFilters();
    filterCache = filters;
    return filters;
}

function getFilter(identifier: FilterIdentifier): Categoryfilter {
    const category = filterCache.find(x => x.category === identifier.category);
    if (category) {
        const filter = category.filters.find(x => x.filterName === identifier.filterName);
        if (filter) {
            return filter;
        }
    }

    throw new Error(`Filter with category ${identifier.category} and name ${identifier.filterName} not found!`);

}

function getCategory(identifier: FilterIdentifier): CategoryWithFilters {
    const category = filterCache.find(x => x.category === identifier.category);
    if (category) {
        return category;
    }

    throw new Error(`Filter with category ${identifier.category} and name ${identifier.filterName} not found!`);

}

function filterExists(ident: FilterIdentifier): boolean {
    return includes(existingfilterNames[ident.category], ident.filterName);
}

export default {
    disableFilter,
    getFeaturesById,
    getHeadlines,
    getCaseDetails,
    saveFilter,
    loadFilterList,
    loadFilter,
    loadProperties,
    filterExists,
    getPropertyDetails,
    getFilter,
    getCategory,
};
