type HotKeyIdentifiers = 'moveUp' | 'moveDown' | 'moveRight';

type KeyMap = { [p in HotKeyIdentifiers]: string; };

export type HotKeyHandlers = { [p in HotKeyIdentifiers]?: (e: KeyboardEvent) => void; };

export const keyMap: KeyMap = {
    moveUp: 'up',
    moveDown: 'down',
    moveRight: 'right',
};