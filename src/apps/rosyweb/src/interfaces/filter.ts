export interface FilterIdentifier {
    category: string;
    filterName: string;
}

export interface Categoryfilter {
    filterString: string;
    filterName: string;
    color: string;
    editable: boolean;
}


export interface PropertyIdentifier {
    category: string;
    propertyPath: string;
}

export interface ValueObject {
    key: string;
    value: string;
}

export type FieldType = 'string' | 'numeric' | 'date' | 'select' | 'boolean' | null;

export interface Property {
    key?: string;
    translatedName: string;
    path: string;
    children: Property[];
}

export interface PropertyDetails {
    fieldType: FieldType;
    options: ValueObject[];
    translatedName: string;
}

export interface CaseDetails {
    id: string;
    fields: string[];
}

export type FilterValueType = string[] | string | null;

export interface WorkingsetProperty {
    translatedName: string;
    path: string;
    fieldType?: FieldType;
    options?: ValueObject[];
    operator?: string;
    filterValue?: FilterValueType;
    key?: string;
}

