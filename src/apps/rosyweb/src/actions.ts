import { Store } from './store';
import { action, runInAction } from 'mobx';
import { FilterIdentifier, WorkingsetProperty } from './interfaces/filter';
// import { getDataModel } from './datawindowmanager';
import { PaneKey, Pane, Panes, Dimensions } from './stores/ui';
// import { FormValuesWithId } from './stores/editwindow';
import filterService from './services/filterservice';
import * as roadService from './services/roadservice';
import * as types from './types';
import ol from '@rosy/utils-openlayers';

export default class Actions {

    store: Store;

    constructor(store: Store) {
        this.store = store;
    }

    @action setPaneSize(pane: PaneKey, size: Pane) {
        this.store.components.ui.setPaneSize(pane, size);
    }

    @action setPaneSizes(panes: Panes) {
        this.store.components.ui.setPaneSizes(panes);
    }

    @action setWindowDimensions(dimensions: Dimensions) {
        this.store.components.ui.windowDimensions = dimensions;
    }

    @action async selectRoad(roadId: types.RoadId) {
        if (this.store.components.roadListStore.selectedRoad === roadId) return;

        this.store.components.roadListStore.selectedRoad = roadId;
        this.zoomToRoad(roadId);
        this.store.windowManager.setSelectedRoad(roadId);
    }

    @action async newFilter(category: string) {
        this.store.components.filter.panel.showPropertiesFor(category);
        this.store.components.filter.workset.showForNewFilter(category);
    }

    @action async enableFilter(ident: FilterIdentifier) {
        this.store.components.filter.panel.setFilterLoading(ident);
        const extent = this.store.components.mapStore.extent;
        if (extent) {
            runInAction(() => {
                this.store.components.filter.panel.setFilterEnabled(ident);
                const color = this.store.components.filter.panel.getFilterColor(ident);
                this.store.components.mapStore.addLayerFromFilter(ident, color);
            });
        }
    }

    @action disableFilter(ident: FilterIdentifier) {
        this.store.components.filter.panel.setFilterDisabled(ident);
        this.store.components.mapStore.removeLayer(ident);
    }

    @action toggleFilter(ident: FilterIdentifier) {
        if (this.store.components.filter.panel.isFilterEnabledOrLoading(ident)) {
            this.disableFilter(ident);
            this.disableDataWindow({
                category: ident.category as types.DataWindowCategory,
                key: ident.filterName as types.DataWindowKey,
            });
        } else {
            this.enableFilter(ident);
            this.enableDataWindow({
                category: ident.category as types.DataWindowCategory,
                key: ident.filterName as types.DataWindowKey,
            }, ident);
        }
    }

    @action stopEditingFilter() {
        this.store.components.filter.panel.showFilters();
        this.store.components.filter.workset.hide();
    }

    @action async addPropertyToWorkset(property: WorkingsetProperty) {
        this.store.components.filter.workset.addProperty(property);

        /*const category = this.store.components.filter.workset.category;
        const propertyDetails = await filterService.getPropertyDetails({
            category,
            propertyPath: property.path,
        });

        runInAction(() => {
            this.store.components.filter.workset.setPropertyDetails(property.path, property.Details);
        });
        */
    }

    @action removePropertyFromWorkset(propertyIndex: number) {
        this.store.components.filter.workset.removeProperty(propertyIndex);
    }

    @action async saveFilter(ident: FilterIdentifier, color: string, filterString: string) {
        await filterService.saveFilter(ident, color, filterString);
        runInAction(() => {
            this.stopEditingFilter();
        });
    }

    @action async saveAndEnableFilter(ident: FilterIdentifier, color: string, filterString: string) {
        this.store.components.filter.panel.updateFilter(ident, color);
        await this.saveFilter(ident, color, filterString);
        this.disableFilter(ident);
        this.enableFilter(ident);
    }

    @action async createAndEnableFilter(ident: FilterIdentifier, color: string, filterString: string) {
        if (filterService.filterExists(ident)) {
            // notificationActions.showError({title: 'Filter already exists',
            //                                message: `There already exists a filter named "${name}".
            //                                          <br>Please choose another name.`});
        } else {
            await this.saveFilter(ident, color, filterString);
            const category = filterService.getCategory(ident);
            runInAction(() => {
                this.store.components.filter.panel.addFilter(ident, category.group, color, true);
                this.enableFilter(ident);
            });
        }
    }

    @action async editFilter(ident: FilterIdentifier) {
        this.store.components.filter.panel.showPropertiesFor(ident.category);
        this.store.components.filter.panel.currentFilterName = ident.filterName;
        const color = this.store.components.filter.panel.getFilterColor(ident);
        this.store.components.filter.workset.show(ident, color);

        const properties = await filterService.loadFilter(ident);

        runInAction(() => {
            properties.forEach(property => this.addPropertyToWorkset(property));
        });

    }
    @action filterAccordionExpanded(name: string) {
        this.store.components.filter.panel.accordionExpanded(name);
    }
    @action filterAccordionCollapsed(name: string) {
        this.store.components.filter.panel.accordionCollapsed(name);
    }

    @action enableDataWindow(window: types.DataWindow, filterIdentifier: FilterIdentifier) {
        const filter = filterService.getFilter(filterIdentifier);
        this.store.windowManager.activateWindow(
            window,
            filter.filterString,
            [filterIdentifier.category, filter.filterName]);
    }

    @action disableDataWindow(window: types.DataWindow) {
        this.store.windowManager.deactivateWindow(window);
    }

     @action roadlistPanelSelected() {
        this.store.components.leftBar.selectedPanel = 'ROADS';
    }
    @action maplayersPanelSelected() {
        this.unSelectRoad();
        this.store.components.leftBar.selectedPanel = 'MAPLAYERS';
    }

    @action async unSelectRoad() {
        this.store.components.roadListStore.selectedRoad = null;
        // this.store.windowManager.resetSelectedRoad();
    }

    @action async selectFeatureFromMap(olFeature: ol.Feature) {
        this.store.components.mapStore.setSelectedFeature(olFeature);
        // const windowManager = this.store.windowManager;
        const category = olFeature.get('category') as types.DataWindowCategory;
        const id = olFeature.getId() as types.Guid;

        // windowManager.markSelectedFeaturesfromMap(category, id);
    }

    @action unselectFeature() {
        this.store.components.mapStore.unselectFeature();
        // this.store.components.featureDetailsStore.close();
    }

    @action datawindowGridRowSelected(window: types.DataWindow, id: string) {
        this.store.components.mapStore.selectedDataWindowRow({ window, id });
    }
    @action showUpdateWindow(window: types.DataWindow, id: types.Guid | types.FormValuesWithId ) {
        // this.store.components.editWindow.showEditWindow(getDataModel(window.category), id);
    }

    @action pinDataWindow(window: types.DataWindow) {
        this.store.windowManager.pinWindow(window);
    }
    @action showCreateWindow(window: types.DataWindow) {
        // this.store.components.editWindow.showCreateWindow(getDataModel(window.category));
    }
    @action async zoomToRoad(id: types.RoadId) {
        const bounds = await roadService.getRoadBounds(id);
        runInAction(() => {
            this.store.components.mapStore.setBounds(bounds);
        });
    }
    @action openParentApplication() {
        // this.store.components.featureDetailsStore.openParentApplication();
    }
    @action closeParentApplication() {
     //   this.store.components.featureDetailsStore.closeParentApplication();
    }

}