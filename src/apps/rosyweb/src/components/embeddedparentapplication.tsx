import * as React from 'react';
import { observer, inject } from 'mobx-react';
import Iframe from './embeddediframe';

import Actions from '../actions';
import { FeatureDetails } from '../stores/featuredetails';

interface Properties {
    actions?: Actions;
    store: FeatureDetails;
}

@inject('actions') @observer
class EmbeddedParentApplication extends React.Component<Properties, {}> {

    close = () => {
        this.props.actions!.closeParentApplication();
    }

    render() {
        if (!this.props.store.initialApplicationUrl) return null;

        const style = {
            display: 'none',
        }

        if (this.props.store.showCaseInParentApplication) {
            style.display = 'block';
        }

        return (
            <div className="embeddedparentapplication__container" style={style}>
                <div className="embeddedparentapplication__header panel-heading">
                    <button type="button" className="close" aria-label="close" onClick={this.close}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 className="panel-title">
                        {this.props.store.title ? this.props.store.title : ''}
                    </h3>
                </div>

                <div className="embeddedparentapplication__body panel-body">
                    <Iframe
                        featureId={this.props.store.id}
                        category={this.props.store.category}
                        initialApplicationUrl={this.props.store.initialApplicationUrl}
                    />
                </div>
            </div>
        );
    }
}

export default EmbeddedParentApplication;

