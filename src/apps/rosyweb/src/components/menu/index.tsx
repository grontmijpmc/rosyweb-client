import * as React from 'react';
import { inject } from 'mobx-react';
import * as classNames from 'classnames';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import Actions from './../../actions';
import * as types from './../../types';

interface Properties {
    actions?: Actions;
}

@inject('actions')
export default class Menu2 extends React.Component<Properties, {}> {

    enableDig = () => {
    /*    this.props.actions!.enableDataWindow({
            category: 'DIG' as types.DataWindowCategory,
            key: 'COMPLETELY_MADEUP_VALUE' as types.DataWindowKey,
        });
        */
    }

    showTestTable = () => {
    /*    this.props.actions!.enableDataWindow({
            category: 'DIG' as types.DataWindowCategory,
            key: 'ALL' as types.DataWindowKey,
        },"","");
        */
    }

    showEditWindow = () => {
        this.props.actions!.showCreateWindow({
            category: 'TestTable' as types.DataWindowCategory,
            key: 'dummy' as types.DataWindowKey,
        });
        }

    render() {
        return (
            <Navbar className="menu__container">
                <Nav>
                    <NavDropdown eventKey={1} title="Data" id="data-dropdown">
                        <MenuItem onClick={this.enableDig} eventKey={1.1}>Gravetilladelser</MenuItem>
                        <MenuItem eventKey={1.2}>Råden over vej</MenuItem>
                    </NavDropdown>
                    <NavItem eventKey={2} onClick={this.showTestTable}>Test table</NavItem>
                    <NavItem eventKey={3} onClick={this.showEditWindow}>Test edit window</NavItem>
                </Nav>
            </Navbar>
       )
   }
}