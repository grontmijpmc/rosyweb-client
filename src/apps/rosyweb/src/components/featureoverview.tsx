import * as React from 'react';
import * as localizer from 'localizer';
import { observer, inject } from 'mobx-react';

import Actions from '../actions';
import { FeatureDetails} from '../stores/featuredetails';

import * as _ from 'lodash';

interface Properties {
    actions?: Actions;
    store: FeatureDetails;
}

@inject('actions') @observer
class FeatureOverview extends React.Component<Properties, {}> {

    onDetailsClicked = () => {
        this.props.actions!.openParentApplication();
    }

    closeDialog = () => {
        this.props.actions!.unselectFeature();
    }

    render() {
        const store = this.props.store;
        if (this.props.store.showCaseInParentApplication) return null;

        const headline = store.headline.map((field, index) =>
            <th className="featureoverview__headline" key={index}>{field}</th>
        );

        const columns = store.fields.map((field, index) =>
            <td className="featureoverview__field" key={index}>{field}</td>
        );

        return (<div className="featureoverview__container">
            <div className="featureoverview__header panel-heading">
                <button type="button" className="close" aria-label="close" onClick={this.closeDialog}>
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 className="panel-title">{store.title}</h3>
            </div>

            <div className="featureoverview__body panel-body">
                <table className="table">
                    <thead>
                        <tr>
                            {headline}
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {columns}
                        </tr>
                    </tbody>
                </table>
            </div>

            <div className="featureoverview__footer panel-footer">
                {(store.canShowInParentApplication
                  && <button
                      type="button"
                      className="btn btn-primary"
                      onClick={this.onDetailsClicked}
                  >{localizer['OverviewMap_Case_Open_Details']}
                  </button>)}
            </div>
        </div>);
    }
}

export default FeatureOverview;

