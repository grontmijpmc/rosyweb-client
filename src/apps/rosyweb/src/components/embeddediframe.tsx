import * as React from 'react';
import * as configuration from 'configuration';

interface Properties {
    featureId: string;
    category: string;
    initialApplicationUrl: string;
}

/*
 * This could probably be refactored to something
 * more simple leveraging the mobx autorun, but it
 * works, so I don't wanna touch it anymore :-)
 */
class Iframe extends React.Component<Properties, {}> {

    frameLoaded = false;
    currentCategory = 'DIG';
    queuedCategory: string | null = 'DIG';
    queuedId: string | null = null;

    element: HTMLIFrameElement | null | undefined;

    componentDidMount() {
        window.addEventListener('message', this.messageReceived);
        // @ts-ignore: for browser compatibilty
        window.addEventListener('onmessage', this.messageReceived);
    }

    componentWillUnmount() {
        window.removeEventListener('message', this.messageReceived);
        // @ts-ignore: for browser compatibilty
        window.removeEventListener('onmessage', this.messageReceived);
    }

    componentWillReceiveProps(nextProps: Properties) {
        const currentId = this.props.featureId;
        const nextId = nextProps.featureId;

        if (nextId) {
            if (this.frameLoaded) {
                if (!currentId || (currentId !== nextId)) {
                    this.showCase(nextProps.category, nextId);
                }
            } else {
                this.loadApplicationAndQueueCase(nextProps.category, nextId);
            }
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    messageReceived = (event: MessageEvent) => {
        if (event.data) {
            try {
                const data = $.parseJSON(event.data);
                if (data.message === 'frameLoaded') {
                    this.onFrameLoaded(data.application);
                }
            } catch (e) {
                // Why do we swallow this error?
            }
        }
    }

    onFrameLoaded(category: string) {
        if (this.queuedCategory === category) {
            this.queuedCategory = null;
            const queuedId = this.queuedId;
            this.queuedId = null;
            this.currentCategory = category;
            this.frameLoaded = true;
            this.loadCaseInFrame(queuedId);
        } else {
            // User clicked another feature from another application during
            // embedded application load. Retry with new application
            if (this.queuedCategory && this.queuedId) {
                this.loadApplicationAndQueueCase(this.queuedCategory, this.queuedId);
            }
        }
    }

    showCase(category: string, id: string) {
        if (configuration.layerGroups[category].mainApplication) {
            if (this.currentCategory === category) {
                this.loadCaseInFrame(id);
            } else {
                this.loadApplicationAndQueueCase(category, id);
            }
        }
    }

    loadCaseInFrame(id: string|null) {
        if (!id) return;
        const message = JSON.stringify({
            message: 'showCase',
            id,
        });
        this.element!.contentWindow!.postMessage(message, "*");
    }

    loadApplicationAndQueueCase(category: string, id: string) {
        this.queuedCategory = category;
        this.queuedId = id;
        const currentApplication = configuration.layerGroups[category].mainApplication;
        if (currentApplication) {
            this.frameLoaded = false;
            this.element!.src = currentApplication;
        }
    }

    render() {
        return <iframe src={this.props.initialApplicationUrl} ref={ref => this.element = ref} />;
    }
}

export default Iframe;

