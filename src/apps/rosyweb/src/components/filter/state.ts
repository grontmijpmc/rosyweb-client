import { observable, action, computed, autorun, IObservableArray } from 'mobx';
import { Panel } from '../../stores/filter';
import Actions from '../../actions';
import { Property } from '../../interfaces/filter';

class State {

    store: Panel;
    actions: Actions;

    @observable level = 0;
    @observable slideDirection = 'left';
    @observable propertyPath: IObservableArray<Property> = observable([]);
    @observable type = 'filters';

    constructor(store: Panel, actions: Actions) {

        this.store = store;
        this.actions = actions;

        autorun(() => {
            if (this.store.currentlyShowing === 'filters') {
                this.onShowFilters();
            } else {
                this.onShowProperties();
            }
        });
    }

    @computed get properties() {
        if (this.store.currentlyShowing === 'filters') return [];

        if (this.propertyPath.length > 0) {
            return this.propertyPath[this.propertyPath.length - 1].children;
        }

        return this.store.properties;
    }

    @action openProperty(property: Property) {
        this.level++;
        this.slideDirection = 'left';
        this.propertyPath.push(property);
    }

    @action closeProperty() {
        this.level--;
        this.propertyPath.pop();
        this.slideDirection = 'right';
        if (this.level === 0) {
            this.actions.stopEditingFilter();
        }
    }

    @action onShowProperties() {
        this.level++;
        this.slideDirection = 'left';
        this.type = 'properties';
    }

    @action onShowFilters() {
        this.level = 0;
        this.slideDirection = 'right';
        this.propertyPath.clear();
        this.type = 'filters';
    }
}

export default State;