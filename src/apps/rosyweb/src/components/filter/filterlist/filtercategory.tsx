import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';
import Actions from '../../../actions';
import { FilterItem } from '../../../stores/filter';

import Button from '@rosy/component-button';
import SavedFilter from './savedfilter';

import * as localizer from 'localizer';

import { Collapse } from 'react-bootstrap';

interface Properties {
    category: string;
    actions?: Actions;
    filters: FilterItem[];
}

@inject('actions') @observer
class FilterCategory extends React.Component<Properties, {}> {

    @observable open: boolean = false;

    @action toggleCollaps = (e: React.MouseEvent<HTMLLIElement>) => {
        this.open = !this.open;
        e.stopPropagation();
    }
    newFilterClick = () => {
        this.props.actions!.newFilter(this.props.category);
    }

    render() {
        return (
            <li key={this.props.category} className="filter__category" onClick={this.toggleCollaps}>
                { this.open ? (
                <span style={{float: 'left', fontSize:'13px', padding: '7px', paddingLeft: '19px', color: '##6c6c6c' }} className="glyphicon glyphicon-minus" ></span>
                ) : (
                    <span style={{float: 'left', fontSize:'13px', padding: '7px', paddingLeft: '19px', color: '##6c6c6c' }} className="glyphicon glyphicon-plus" ></span>
                )
                }
                <label className="filter__category-label">{localizer[`Category_Headline_${this.props.category}`]}</label>
                <div style={{clear: 'both'}}></div>
                <Collapse in={this.open} >
                <div>
                    <ul>
                        {
                            this.props.filters.map(filter =>
                                        <SavedFilter
                                            key={filter.filterName}
                                            filterName={filter.filterName}
                                            category={this.props.category}
                                            filter={filter}
                                        />
                                    )
                        }
                    </ul>

                    <li className="filter__new-filter-divider">
                        <Button className="filter__new-filter-button" onClick={this.newFilterClick}>{localizer['Filter_Dialog_NewFilter']}</Button>
                    </li>
                </div>
                </Collapse>
            </li>
        );
    }
}

export default FilterCategory;

