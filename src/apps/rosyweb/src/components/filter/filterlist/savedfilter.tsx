import * as React from 'react';
import { observer, inject } from 'mobx-react';
import * as classNames from 'classnames';

import { FilterItem } from '../../../stores/filter';
import Actions from '../../../actions';

import PromiseButton from '@rosy/component-promisebutton';
import Swatch from './swatch';
import * as localizer from 'localizer';
import * as configuration from 'configuration';

interface Properties {
    category: string;
    filterName: string;
    actions?: Actions;
    filter: FilterItem;
}

@inject('actions') @observer
class SavedFilter extends React.Component<Properties, {}> {

    filterRef: Element | null | undefined;

    onToggleFilter = (e: React.MouseEvent<HTMLButtonElement>) => {
        this.props.actions!.toggleFilter({ category: this.props.category, filterName: this.props.filter.filterName });
        e.stopPropagation();
    }

    onFilterOpen = () => {
        this.props.actions!.editFilter({ category: this.props.category, filterName: this.props.filter.filterName });
    }

    render() {
        const filter = this.props.filter;

        // tslint:disable-next-line:variable-name
        const filtertoggler_className = classNames({
            filter__singlefilter_toggler: true,
            enabled: filter.enabled,
        });

        // tslint:disable-next-line:variable-name
        const togglebutton_or_spinner = filter.loading
            ? <span className="filter__singlefilter_toggle--spinner" />
            : (
                <button
                    type="button"
                    className="filter__singlefilter_toggle btn btn-link"
                    onClick={this.onToggleFilter}
                >
                    <span className={filtertoggler_className} />
                </button>
            );

        return (
            <div className="filter__singlefilter" ref={ref => this.filterRef = ref}>
                {togglebutton_or_spinner}
                <label className="filter__singlefilter_label">{filter.filterName}</label>
                <div className="filter__singlefilter_divider">
                    <Swatch color={filter.color} />
                    {
                        filter.editable
                            ? (
                                <PromiseButton
                                    className="filter__singlefilter_button"
                                    onClick={this.onFilterOpen}
                                >
                                    {localizer['Filter_Edit_Filter']}
                                </PromiseButton>
                            )
                            : (
                                <button
                                    className="filter__singlefilter_button btn btn-disabled"
                                    disabled={true}
                                    title={localizer['Filter_Cannot_Edit_Filter']}
                                >
                                    {localizer['Filter_Edit_Filter']}
                                </button>
                            )
                    }
                </div>
            </div>
        );
    }
}

export default SavedFilter;