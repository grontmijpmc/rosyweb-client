import * as React from 'react';

function Swatch({ color }: { color: string }) {
    const style = {
        backgroundColor: color,
    };

    return <div style={style} className="filter__singlefilter_swatch"/>;
}

export default Swatch;
