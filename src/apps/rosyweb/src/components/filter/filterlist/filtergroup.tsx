import * as React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';
import { Panel } from '../../../stores/filter';

import FilterCategory from './filtercategory';

import { Collapse } from 'react-bootstrap';

interface Properties {
    store: Panel;
    group: string;
}

class  FilterGroup extends React.Component<Properties, {}> {
    @observable open: boolean = false;
    @action toggleCollaps = (e: React.MouseEvent<HTMLDivElement>) => {
        this.open = !this.open;
        e.stopPropagation();
    }
    render() {
        const categories = this.props.store.groupsandcategories.get(this.props.group);
        return (

            <div key={this.props.group} >
                <ul>
                <div  className="filter__group" onClick={this.toggleCollaps} >
                { this.open ? (
                <span style={{float: 'left', fontSize:'20px', padding: '5px', paddingLeft: '15px', color: '#6c6c6c' }} className="glyphicon glyphicon-minus" ></span>
                ) : (
                    <span style={{float: 'left', fontSize:'20px', padding: '5px', paddingLeft: '15px', color: '#6c6c6c' }} className="glyphicon glyphicon-plus" ></span>
                )
                }
                <label className="filter__group-label" style={{backgroundColor: '#e6e6e6'}}>{this.props.group}</label>
                <div style={{clear: 'both'}}></div>
                    <Collapse in={this.open}>
                        <div>
                            {
                                    categories!.map(category =>
                                    <FilterCategory key={category} category={category} filters={this.props.store.filters.get(category) || []} />)
                            }
                    </div>
                    </Collapse>
                    </div>
                </ul>
            </div>
        );
    }
}

export default observer(FilterGroup);