import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';
import { Panel } from '../../../stores/filter';
import Accordion from '@rosy/component-accordion';
import SavedFilter from './savedfilter';
import Actions from '../../../actions';
import FilterGroup from './filtergroup';

interface Properties {
    store: Panel;
    actions?: Actions;
}

@inject('actions') @observer
class FilterList extends React.Component<Properties, {}> {

    accordionExpanded = (name: string): void => {
        this.props.actions!.filterAccordionExpanded(name);
    }
    accordionCollapsed = (name: string): void => {
        this.props.actions!.filterAccordionCollapsed(name);
    }

    render() {
        const groups = this.props.store.groupsandcategories.keys();
        return (
            <div key="categories" className="filter__pane">
                <Accordion>
                    {
                        groups.map(group => {
                            const groupexpanded = this.props.store.expandedAccordions!.indexOf(group) > -1;
                            return (
                                <Accordion.Group
                                    key={group}
                                    name={group}
                                    label={group}
                                    expanded={groupexpanded}
                                    onExpanded={this.accordionExpanded}
                                    onCollapsed={this.accordionCollapsed}
                                >
                                    {
                                        this.props.store.groupsandcategories!.get(group)!.map(category => {
                                            const categoryexpanded
                                            = this.props.store.expandedAccordions!.indexOf(category) > -1;
                                            return (
                                                <Accordion.Group
                                                    key={category}
                                                    name={category}
                                                    label={category}
                                                    expanded={categoryexpanded}
                                                    onExpanded={this.accordionExpanded}
                                                    onCollapsed={this.accordionCollapsed}
                                                >
                                                    {
                                                        this.props.store.filters!.get(category)!.map(filter =>
                                                            <Accordion.Item key={filter.filterName}>
                                                                <SavedFilter
                                                                    category={category}
                                                                    filterName={filter.filterName}
                                                                    filter={filter}
                                                                />
                                                            </Accordion.Item>,
                                                        )
                                                    }
                                                </Accordion.Group>
                                            );
                                        })
                                    },
                                </Accordion.Group>
                            );
                        })
                    }
                </Accordion>
            </div>
        );
    }
}

export default FilterList;
