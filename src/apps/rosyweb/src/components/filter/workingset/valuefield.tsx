import * as React from 'react';
import { isObservable } from 'mobx';
import { observer } from 'mobx-react';
import KendoText from '@rosy/component-text';
import KendoMultiSelect from '@rosy/component-multiselect';
import KendoDatePicker from '@rosy/component-datepicker';
import DropDownList from '@rosy/component-dropdownlist';
import * as configuration from 'configuration';
import * as localizer from 'localizer';

import { FilterValueType, FieldType } from '../../../interfaces/filter';

const booleanValues = [
    { key: 'true', value: localizer.Filter_BooleanTrue },
    { key: 'false', value: localizer.Filter_BooleanFalse },
];

interface Properties {
    onChange: (value: FilterValueType) => void;
    fieldType: FieldType;
    value: any;
    propertyPath: string;
    category: string;
    dataSource?: any;
}

@observer
class ValueField extends React.Component<Properties, {}> {

    static defaultProps = {
        value: '',
    };

    onDatefieldChanged = (value: Date) => {
        const filterValue = `${value.getFullYear()}-${value.getMonth()}-${value.getDate()}`;
        this.props.onChange(filterValue);
    }

    render() {
        let value = this.props.value;
        if (isObservable(value)) {
            value = value.toJS();
        }

        if (this.props.fieldType === "date") {
            return <KendoDatePicker onChange={this.onDatefieldChanged} value={value} />;
        } else if (this.props.fieldType === "boolean") {
            return (
                <DropDownList
                    dataSource={booleanValues}
                    displayField="value"
                    valueField="key"
                    onChange={this.props.onChange}
                    value={value}
                />
            );
        } else if (this.props.fieldType === "select") {
            let dataSource = this.props.dataSource;
            if (isObservable(dataSource)) {
                dataSource = dataSource.toJS();
            }
            return (
                <KendoMultiSelect
                    dataSource={dataSource}
                    displayField="value"
                    valueField="key"
                    onChange={this.props.onChange}
                    value={value}
                />
            );
        } else {
            // tslint:disable-next-line:max-line-length
            const autocompleteUrl = `/base/data?Model=${this.props.category}&View=FilterAutoCompleter&PropertyPath=${this.props.propertyPath}`;

            return (
                <KendoText
                    onChange={this.props.onChange}
                    value={value}
                    autocompleteUrl={autocompleteUrl}
                />
            );
        }
    }
}

export default ValueField;
