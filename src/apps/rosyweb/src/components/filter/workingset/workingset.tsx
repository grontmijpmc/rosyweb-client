import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';

import * as _ from 'lodash';
import WorkingsetRow from './row';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
import KendoText from '@rosy/component-text';
import FilterColorPicker from '@rosy/component-colorpicker';
import Dialog from '@rosy/component-dialog';
import * as localizer from 'localizer';

import { Workset } from '../../../stores/filter';
import Actions from '../../../actions';

interface Properties {
    store: Workset;
    actions?: Actions;
}

function filterNull<T>(args: Array<T | null>): T[] {
    return (args.filter(x => x !== null) as T[]);
}

@inject('actions') @observer
class Workingset extends React.Component<Properties, {}> {

    store: Workset;

    @observable name: string;
    @observable color: string;
    @observable properties = [];
    @observable colorPickerOpen = false;

    constructor(props: Properties) {
        super(props);
        this.store = props.store;

        this.name = this.store.filterName || '';
        this.color = this.store.color;
    }

    getFilterstring(): string {
        // This is a little bit tricky.
        // The refs returned here is not the WorkingsetRow as expected,
        // but actually a HOC created by inject which allows access to the real
        // WorkingsetRow by accessing the wrappedInstance property
        const filterStrings = filterNull(
            _.map(this.refs,
                  ref => (ref as any).wrappedInstance.getFilterstring()));

        if (filterStrings.length === 1) return filterStrings[0];

        let result = '(&& ';
        for (const filterString of filterStrings) {
            result += filterString;
        }
        result += ')';
        return result;
    }

    saveFilter = () => {
        const name = this.name;
        const category = this.store.category;
        const color = this.color;
        const filterString = this.getFilterstring();
        if (this.store.isNew) {
            this.props.actions!.createAndEnableFilter({ category, filterName: name }, color, filterString);
        } else {
            this.props.actions!.saveAndEnableFilter({ category, filterName: name}, color, filterString);
        }
    }

    closeDialog = () => {
        this.props.actions!.stopEditingFilter();
    }

    @action closeColorPicker = () => {
        this.colorPickerOpen = false;
    }

    // Todo: Check if this declaration is necessary.
    // Could we drop @action?
    // tslint:disable-next-line:member-ordering - It's technically a method, not a property
    @action chooseColor = action((color: string) => {
        this.closeColorPicker();
        this.color = color;
    });

    @action openColorPicker = () => {
        this.colorPickerOpen = true;
    }

    filterNameChanged = (value: string) => {
        this.name = value;
    }

    getWorkingsetRows() {
        return this.store.properties.map((prop, index) => {
            return (
                <WorkingsetRow
                    category={this.store.category}
                    property={prop}
                    ref={index.toString()}
                    key={prop.key}
                    index={index}
                />
            );
        });
    }

    render_hint() {
        return <p>{localizer.Filter_Creation_Hint}</p>;
    }

    render_properties() {
        if (!this.store.properties.length) {
            return this.render_hint();
        } else {
            return (
                <table className="table">
                    <tbody>
                        {this.getWorkingsetRows()}
                    </tbody>
                </table>
            );
        }
    }

    render_body() {
        if (this.store.isNew) {
            let hint: any;
            if (!this.store.properties.length) {
                hint = <tr><td colSpan={3}>{this.render_hint()}</td></tr>;
            }

            return (
                <table className="table">
                    <tbody>
                        <tr>
                            <td>{localizer.Filter_Name}</td>
                            <td><KendoText value={this.name} onChange={this.filterNameChanged}/></td>
                        </tr>
                        {hint}
                        {this.getWorkingsetRows()}
                    </tbody>
                </table>
            );
        } else {
            return this.render_properties();
        }
    }

    renderDisabledSaveButton(): JSX.Element {
        return (
            <button
                type="button"
                className="btn btn-primary disabled"
                disabled={true}
            >
                {localizer.Filter_Dialog_Button_Save}
            </button>
        );
    }

    renderSaveButton(): JSX.Element {
        return (
            <button
                type="button"
                className="btn btn-primary"
                onClick={this.saveFilter}
            >
                {localizer.Filter_Dialog_Button_Save}
            </button>
        );
    }

    render() {

        let tooltip: JSX.Element;
        let saveButton: any;

        if (this.store.isNew && this.name.length === 0) {
            tooltip = <Tooltip id="workingset_save_hint">{localizer.Filter_Dialog_Save_Hint_Noname}</Tooltip>;
            saveButton = this.renderDisabledSaveButton();
        } else if (this.store.properties.length === 0) {
            tooltip = <Tooltip id="workingset_save_hint">{localizer.Filter_Dialog_Save_Hint_Empty}</Tooltip>;
            saveButton = this.renderDisabledSaveButton();
        } else {
            tooltip = <Tooltip id="workingset_save_hint">{localizer.Filter_Dialog_Button_Save}</Tooltip>;
            saveButton = this.renderSaveButton();
        }

        const changeColorTooltip = (
            <Tooltip id="workingset_change_color">
                {localizer.Filter_Dialog_Change_Color}
            </Tooltip>
        );

        return (
            <div>
                {
                    this.colorPickerOpen &&
                    (
                            <FilterColorPicker
                                onOkDialog={this.chooseColor}
                                onCloseDialog={this.closeColorPicker}
                                color={this.color}
                                className="filtercolorpicker__container"
                            />
                    )
                }
                <Dialog className="filter-workingset-dialog">
                    <Dialog.Header>
                        <div className="clearfix">
                            <h3 className="panel-title" style={{ float: 'left' }}>{this.name}</h3>
                            <OverlayTrigger
                                placement="bottom"
                                overlay={changeColorTooltip}
                            >
                                <button
                                    className="filter-workingset-dialog__swatch"
                                    style={{
                                        backgroundColor: this.color,
                                    }}
                                    onClick={this.openColorPicker}
                                />
                            </OverlayTrigger>
                        </div>
                    </Dialog.Header>

                    <Dialog.Body>
                        {this.render_body()}
                    </Dialog.Body>

                    <Dialog.Footer>
                        <Dialog.ButtonBar>
                            <button
                                type="button"
                                className="btn btn-default"
                                onClick={this.closeDialog}
                            >
                                {localizer.Dialog_Cancel}
                            </button>
                            <OverlayTrigger placement="top" overlay={tooltip}>
                                <div style={{ display: 'inline-block' }}>
                                    {saveButton}
                                </div>
                            </OverlayTrigger>
                        </Dialog.ButtonBar>
                    </Dialog.Footer>
                </Dialog>
            </div>
        );
    }
}

export default Workingset;