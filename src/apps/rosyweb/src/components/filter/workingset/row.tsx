import * as React from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import KendoDropDownList from '@rosy/component-dropdownlist';
import ValueField from './valuefield';
import * as localizer from 'localizer';

import { WorkingsetProperty, FilterValueType } from '../../../interfaces/filter';
import Actions from '../../../actions';

interface FilterOperator {
    display: string;
    filterOperator: string;
    unaryOperator?: boolean;
}

const booleanOperators: FilterOperator[] = [
    { display: "=", filterOperator: "==" },
    { display: "<>", filterOperator: "!=" },
    { display: localizer.Filter_NotFilled, filterOperator: "ISNULL", unaryOperator: true },
];

const comparisonOperators: FilterOperator[] = [
    { display: '=', filterOperator: '==' },
    { display: '<>', filterOperator: '!=' },
    { display: '>', filterOperator: '>' },
    { display: '<', filterOperator: '<' },
    { display: '>=', filterOperator: '>=' },
    { display: '<=', filterOperator: '<=' },
    { display: localizer.Filter_NotFilled, filterOperator: 'ISNULL', unaryOperator: true },
];

const stringOperators: FilterOperator[] = [
    { display: "=", filterOperator: "==" },
    { display: "<>", filterOperator: "!=" },
    { display: ">", filterOperator: ">" },
    { display: "<", filterOperator: "<" },
    { display: ">=", filterOperator: ">=" },
    { display: "<=", filterOperator: "<=" },
    { display: localizer.Filter_Contains, filterOperator: "%%" },
    { display: localizer.Filter_StartsWith, filterOperator: "=%" },
    { display: localizer.Filter_EndsWith, filterOperator: "%=" },
    { display: localizer.Filter_DoesNotContain, filterOperator: "!%" },
    { display: localizer.Filter_NotFilled, filterOperator: "ISNULL", unaryOperator: true },
];

interface Properties {
    property: WorkingsetProperty;
    index: number;
    category: string;
    actions?: Actions;
}

@inject('actions') @observer
class Row extends React.Component<Properties, {}> {

    // @observable filterValue;
    // @observable operator;
    /*
    constructor(props) {
        super(props);
        const property = this.props.property;
        this.filterValue = property.filterValue || '';
        this.operator = property.selectedOperator || comparisonOperators[0].filterOperator;
    }
    */
    getFilterObject() {
        return {
            path: this.props.property.path,
            operator: this.props.property.operator || comparisonOperators[0].filterOperator,
            value: this.props.property.filterValue,
        };
    }

    getFilterstring() {
        const filterValue = this.getFilterObject();
        if (filterValue.value === null || filterValue.value === '') {
            // This row has been added but no value has been entered for filtering
            // return null to signal that it should be ignored
            return null;
        }

        if (Array.isArray(filterValue.value!.slice())) {
            let result: string;
            const values = (filterValue.value as string[]).filter(x => x !== null);
            if (values.length === 0) return '';

            if (values.length > 1) {
                result = '(|| ';
                for (const value of values) {
                    result += `(${filterValue.operator} "${filterValue.path}" "${value}")`;
                }
                result += ')';
            } else {
                result = `(${filterValue.operator} "${filterValue.path}" "${values[0]}")`;
            }
            return result;
        } else {
            return `(${filterValue.operator} "${filterValue.path}" "${filterValue.value}")`;
        }
    }

    removeProperty = () => {
        this.props.actions!.removePropertyFromWorkset(this.props.index);
    }

    @action onValueFieldChanged = (value: FilterValueType) => {
        this.props.property.filterValue = value;
    }

    @action onComparatorFieldChanged = (value: string) => {
        this.props.property.operator = value;
    }

    render_loading_state(property: WorkingsetProperty) {
        return (
            <tr key={property.key}>
                <td>{property.translatedName}</td>
                <td>-- {localizer.Loading} -- </td>
                <td>-- {localizer.Loading} --</td>
                <td />
            </tr>
        );
    }

    render() {
        const property = this.props.property;
        if (!property.fieldType) {
            return this.render_loading_state(property);
        } else {
            let operators;
            if (property.fieldType === 'string') {
                operators = stringOperators;
            } else if (property.fieldType === 'boolean') {
                operators = booleanOperators;
            } else {
                operators = comparisonOperators;
            }

            return (
                <tr key={property.key}>
                    <td>{property.translatedName}</td>
                    <td>
                        <KendoDropDownList
                            dataSource={operators}
                            valueField="filterOperator"
                            displayField="display"
                            onChange={this.onComparatorFieldChanged}
                            value={property.operator}
                        />
                    </td>
                    <td>
                        <ValueField
                            onChange={this.onValueFieldChanged}
                            category={this.props.category}
                            value={property.filterValue}
                            dataSource={property.options}
                            fieldType={property.fieldType}
                            propertyPath={property.path}
                        />
                    </td>
                    <td>
                        <button
                            onClick={this.removeProperty}
                            className="k-button"
                        >
                            {localizer.Filter_Item_Remove}
                        </button>
                    </td>
                </tr>
            );
        }
    }
}

export default Row;