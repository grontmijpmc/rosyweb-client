import * as React from 'react';
import { observer, inject } from 'mobx-react';

import Actions from '../../../actions';
import { Property } from '../../../interfaces/filter';

interface Properties {
    actions?: Actions;
    category: string;
    property: Property;
    className: string;
}

@inject('actions') @observer
class SimpleProperty extends React.Component<Properties, {}> {

    addPropertyToWorkset = () => {
        this.props.actions!.addPropertyToWorkset(this.props.property);
    }

    render() {
        const { property, actions, children, category, ...rest } = this.props;

        return (
            <div key={this.props.property.path} {...rest} onClick={this.addPropertyToWorkset}>
                {this.props.property.translatedName}
            </div>
        );
    }
}

export default SimpleProperty;

