import * as React from 'react';
import { observer, inject } from 'mobx-react';

import State from '../state';

import { Property } from '../../../interfaces/filter';

interface Properties {
    state?: State;
    category: string;
    property: Property;
    className: string;
}

@inject('state') @observer
class ComplexProperty extends React.Component<Properties, {}> {

    openProperty = () => {
        this.props.state!.openProperty(this.props.property);
    }

    render() {
        return (
            <li
                key={this.props.property.path}
                className="filter__filter-item filter__filter-item--complex"
                onClick={this.openProperty}
            >
                <span className="filter__filter-item-label">{this.props.property.translatedName}</span>
                <span className="arrow" />
            </li>
        );
    }
}

export default ComplexProperty;