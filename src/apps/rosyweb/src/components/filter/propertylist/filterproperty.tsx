import * as React from 'react';
import { observer } from 'mobx-react';

import { Property } from '../../../interfaces/filter';

import ComplexProperty from './complexproperty';
import SimpleProperty from './simpleproperty';

interface Properties {
    category: string;
    property: Property;
    className: string;
}


function FilterProperty(props: Properties) {
    const property = props.property;

    return property.children.length
        ? <ComplexProperty {...props} property={property} />
        : <SimpleProperty {...props} property={property} />;
}

export default observer(FilterProperty);

