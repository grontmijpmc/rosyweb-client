import * as React from 'react';
import { observer, Provider } from 'mobx-react';

import FilterProperty from './filterproperty';
import State from '../state';
import { Panel } from '../../../stores/filter';

interface Properties {
    state: State;
    store: Panel;
    onBackbuttonClick: () => void;
}

function PropertyList({
    state,
    store,
    onBackbuttonClick }: Properties) {

    const category = store.currentCategory;
    const title = store.currentFilterName || 'New Filter';

    return (
        <Provider state={state}>
            <div key={title} className="filter__pane">
                <div className="filter__pane-header">
                    <span className="filter__pane-header-back" onClick={onBackbuttonClick} />
                    {title}
                </div>
                <ul>
                    {state.properties.map(property =>
                        <FilterProperty
                            category={category!}
                            className="filter__filter-item"
                            property={property}
                            key={property.path}
                        />)}
                </ul>
            </div>
        </Provider>);
}

export default observer(PropertyList);