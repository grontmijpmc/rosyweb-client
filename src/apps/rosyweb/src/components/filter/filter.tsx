import * as React from 'react';
// import * as ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

import { observer, inject } from 'mobx-react';

import { Panel } from '../../stores/filter';
import Actions from '../../actions';

import State from './state';

import PropertyList from './propertylist/propertylist';
import FilterList from './filterlist/filterlist';

interface Properties {
    store: Panel;
    actions?: Actions;
}

@inject('actions') @observer
class Filter extends React.Component<Properties, {}> {

    state: State;

    constructor(props: Properties) {
        super(props);
        this.state = new State(props.store, props.actions as Actions);
    }

    onBackbuttonClick = () => {
        this.state.closeProperty();
    }

    render() {
        const className = `filter__panes ${this.state.slideDirection}`;

        return (
            <div className="filter__viewport">
                    {
                        this.props.store.currentlyShowing === 'filters'
                        ? <FilterList
                            key="filters"
                            store={this.props.store}
                        />
                        : <PropertyList
                            key={this.state.level}
                            store={this.props.store}
                            state={this.state}
                            onBackbuttonClick={this.onBackbuttonClick}
                        />
                    }
            </div>
        );
    }
}

export default Filter;