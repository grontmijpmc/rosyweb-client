import * as React from 'react';
import { observer } from 'mobx-react';

interface Properties {
    color: string;
}

@observer
export default class Graphic extends React.Component<Properties, {}> {

    render() {
        const style = {
            backgroundColor: this.props.color,
        };

        return (
            <div className="legend-graphic" style={style} />
        );
    }
}