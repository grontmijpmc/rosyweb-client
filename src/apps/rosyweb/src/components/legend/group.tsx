import * as React from 'react';

interface Properties {
    group: string;
    category: string;
}

export default class extends React.Component<Properties, {}> {

    render() {
        return (
            <h4 className="legend-group-header">
                {this.props.group} <span style={{ color: "#ccc" }}>/</span> {this.props.category}
            </h4>
        );
    }
}