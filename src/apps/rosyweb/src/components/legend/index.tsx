import * as React from 'react';
import { IObservableArray, computed } from 'mobx';
import { observer } from 'mobx-react';

import LegendGroupHeader from './group';
import LegendGraphic from './graphic';
import LegendText from './text';
import Store, { Layer } from '../../stores/map';

interface LegendEntry {
    color: string;
    text: string;
}

interface LegendGroup {
    group: string;
    category: string;
    childs: LegendEntry[];
}

function convertGroups(layers: IObservableArray<Layer>) {
    let result: LegendGroup[] = [];
    for (const layer of layers) {
        let existingGroup = result.find(x => x.group == layer.name.group && x.category == layer.window.category);
        if (!existingGroup) {
            existingGroup = {
                group: layer.name.group,
                category: layer.window.category,
                childs: [],
            };
            result.push(existingGroup);
        }
        existingGroup.childs.push({ color: layer.color, text: layer.name.text });
        existingGroup.childs = existingGroup.childs.sort((a, b) => a.text.localeCompare(b.text));
    }

    result = result.sort((a, b) => (a.group + a.category).localeCompare(b.group + b.category));
    return result;
}

function getGroups(groups: LegendGroup[]) {
    return groups.map(getGroup);
}

function getGroup(group: LegendGroup) {
    return (
        <div key={group.group + "_" + group.category} className="legend-group">
            <LegendGroupHeader group={group.group} category={group.category} />
            {group.childs.map(getChild)}
        </div>
    );
}

function getChild(child: LegendEntry) {
    return (
        <div className="legend-item" key={child.text}>
            <LegendGraphic color={child.color} />
            <LegendText>{child.text}</LegendText>
        </div>
    );
}

interface Properties {
    store: Store;
}

@observer
export default class Legend extends React.Component<Properties, {}> {

    render() {
        if (this.props.store.layers.length) {
            return (
                <div className="legend legend-container">
                    <div className="legend-content">
                        {getGroups(convertGroups(this.props.store.layers))}
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }
}