import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { autorun } from 'mobx';
import { inject, observer } from 'mobx-react';
import Store from '../../stores/roadlist';
import * as types from '../../types';
import Actions from '../../actions';
import UI from '../../stores/ui';
import { debounce } from 'lodash-es';
import * as $ from 'jquery';
import '@progress/kendo-ui/js/kendo.grid';

interface Properties {
    store: Store;
    actions?: Actions;
    ui?: UI;
}

type returnedRoad = kendo.data.ObservableObject & types.Road;

type ActionFunction = () => void;

@inject('actions', 'ui') @observer
class List extends React.Component<Properties, {}> {

    disposers: ActionFunction[] = [];

    componentDidMount() {
        const el = ReactDOM.findDOMNode(this);
        const $el = $(el);
        $el.kendoGrid({
            dataSource: this.props.store.dataSource,
            columns: [
                {
                    field: "roadKey",
                    title: "Vejnummer",
                },
                {
                    field: "roadName",
                    title: "Vejnavn",
                },
            ],
            selectable: 'row',
            sortable: true,
            groupable: true,
            navigatable: true,
            change: this.onRowSelected,
            dataBound: this.onDataBound,
            navigate: this.onNavigate,
            height: "100%",
        });

        this.disposers.push(autorun(() => this.setHeight()));
    }

    componentWillUnmount() {
        const el = ReactDOM.findDOMNode(this);
        $(el).data('kendoGrid').destroy();
        for (const disposer of this.disposers) {
            disposer();
        }
    }

    setHeight() {
        const $el = $(ReactDOM.findDOMNode(this));
        const height = this.props.ui!.windowDimensions.height;
        // Hack. Figure out how we can avoid hardcoding this value
        $el.height(height - 71);
        $el.data('kendoGrid').resize(true);
    }

    onDataBound = (e: kendo.ui.GridDataBoundEvent) => {
        const grid = e.sender;
        this.collapseAllGroups(grid);
    }

    onRowSelected = (e: kendo.ui.GridChangeEvent) => {
        // Notice. When navigating by mouse, this event might be called twice due to the onNavigate
        // event handler. actions.selectRoad ignore duplicate calls
        const grid = e.sender;
        const selectedRoad = this.getCurrentSelectedRow(grid);
        if (selectedRoad) {
            this.props.actions!.selectRoad(selectedRoad.id);
        }
    }

    onNavigate = (e: kendo.ui.GridNavigateEvent) => {
        if (e.element) {
            e.sender.select(e.element.parent());
        }
    }

    getCurrentSelectedRow(grid: kendo.ui.Grid): returnedRoad | null {
        return grid.dataItem(grid.select()) as returnedRoad;
    }

    collapseAllGroups(grid: kendo.ui.Grid) {
        const el = ReactDOM.findDOMNode(this);
        if (grid.dataSource.group().length > 0) {
            const groups = $('.k-grouping-row', el);
            // tslint:disable-next-line:prefer-for-of - jquery has no iterator method
            for (let i = 0; i < groups.length; i++) {
                grid.collapseGroup(groups[i]);
            }
        }
    }

    render() {
        return <div style={{ height: "100%" }}/>;
    }
}

export default List;