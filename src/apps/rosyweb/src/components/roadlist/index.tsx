import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, IObservableArray, computed } from 'mobx';
import * as smoothScroll from 'smoothscroll';
import KendoText from '@rosy/component-text';
import * as types from '../../types';
import Store from '../../stores/roadlist';
import Actions from '../../actions';
import { throttle } from 'lodash-es';
import RoadFilter from './roadfilter';
import List from './list';

interface Properties {
    store: Store;
    actions?: Actions;
}

@inject('actions') @observer
class RoadList extends React.Component<Properties, {}> {

    panel: HTMLElement;
    @observable filterValue: string = "";
    debouncedOnRoadFilterChanged: (value: string) => void;

    constructor(props: Properties) {
        super(props);
        this.debouncedOnRoadFilterChanged = throttle(this.onRoadFilterChanged, 1000);
    }

    scrollItemIntoView = (item: HTMLElement) => {
        if (item.offsetTop > this.panel.scrollTop + this.panel.offsetHeight
            || item.offsetTop < this.panel.scrollTop) {
            smoothScroll(item, null, null, this.panel);
        }
    }

    selectRoad = (roadId: types.RoadId) => {
        this.props.actions!.selectRoad(roadId);
    }

    onRoadFilterChanged = (value: string) => {
        this.filterValue = value;

        this.props.store.dataSource.filter({
            logic: 'or',
            filters: [
                { field: 'roadName', operator: 'startswith', value },
                { field: 'roadKey', operator: 'startswith', value },
            ],
        });

    }

    render() {
        return (
            <div style={{ height: "100%" }}>
                <div style={{ display: 'table-row' }}>
                    <RoadFilter onRoadFilterChanged={this.debouncedOnRoadFilterChanged} />
                </div>
                <div style={{ display: 'table-row', height: "100%" }}>
                   <List store={this.props.store} />
                </div>
            </div>
        );
    }
}

export default RoadList;
