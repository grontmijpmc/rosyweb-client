import * as React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';
import KendoText from '@rosy/component-text';

interface Properties {
    onRoadFilterChanged: (value: string) => void;
}

@observer
class RoadFilter extends React.Component<Properties, {}> {

    @observable filterValue: string = "";

    @action onRoadFilterChanged = (value: string) => {
        this.filterValue = value;
        this.props.onRoadFilterChanged(value);
    }

    render() {
        return (
            <KendoText
                value={this.filterValue}
                placeholder="Søg vej"
                style={{ width: '100%' }}
                onChange={this.onRoadFilterChanged}
            />
        );
    }
}

export default RoadFilter;
