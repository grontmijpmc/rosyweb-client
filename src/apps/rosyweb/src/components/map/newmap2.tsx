import * as React from 'react';
import { autorun, observe, IArrayChange, IArraySplice, IObservableArray, reaction } from 'mobx';
import { observer, inject } from 'mobx-react';
import ol from '@rosy/utils-openlayers';
import MapComponent from '@rosy/component-map';

import { MapStore, Layer } from '../../stores/map';
import UI from '../../stores/ui';
import Actions from '../../actions';

import * as  configuration from 'configuration';

export interface Properties {
    store: MapStore;
    ui?: UI;
    actions?: Actions;
}

type Disposer = () => void;

@inject('ui', 'actions') @observer
export default class Map extends React.Component<Properties, {}> {

    private layers?: ol.Collection<ol.layer.Layer>;
    private view?: ol.View;
    private extent?: ol.Extent;

    private numberOfBaseLayers = 0;

    private disposers: Disposer[] = [];
    private selectInteraction: ol.interaction.Select = new ol.interaction.Select();

    map: MapComponent | null | undefined;

    constructor(props: Properties) {
        super(props);
        const layers = [];
        for (const layer of Object.keys(configuration.mapConfiguration.openLayers.layers)) {
            layers.push(configuration.mapConfiguration.openLayers.layers[layer]);
        }

        this.numberOfBaseLayers = layers.length;

        this.layers = new ol.Collection<ol.layer.Layer>(layers);
        this.view = new ol.View(configuration.mapConfiguration.openLayers.view);
        debugger;
        this.extent = this.props.store.extent!;

    }

    componentDidMount() {
        debugger;
        const selectstyle = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#118e88',
                width: 3,
            }),
            image: new ol.style.Circle({
                radius: 3,
                fill: new ol.style.Fill({color: '#118e88' }),
                stroke: new ol.style.Stroke({ color: '#118e88', width: 3 }),
            }),
        });

        this.selectInteraction = new ol.interaction.Select({style: selectstyle});
        this.selectInteraction.on('select', (e: ol.interaction.Select.Event) => {
            const features = (e.target as ol.interaction.Select).getFeatures().getArray();
            if (features.length > 0) {
                this.props.actions!.selectFeatureFromMap(features[0]);
            } else {
                this.props.actions!.unselectFeature();
            }
        });

        this.map!.addInteraction(this.selectInteraction);

        this.disposers.push(autorun(() => {
            let extent: [number, number, number, number];
            if (this.props.store.extent) {
                extent = this.props.store.extent.slice() as [number, number, number, number];
                this.map!.zoomToExtent(extent);
            }
        }));

        this.disposers.push(autorun(() => this.setSelectedFeatureFromDataWindow()));
        this.disposers.push(observe(this.props.store.layers, this.layersChanged.bind(this)));

        this.disposers.push(reaction(() => this.props.ui!.panes.map.size, () => {
            this.map!.updateSize();
        }));
    }

    componentWillUnmount() {
        for (const disposer of this.disposers) {
            disposer();
        }

        this.map!.unMount();
    }

    setSelectedFeatureFromDataWindow() {
        const selectedRow = this.props.store.selectedRow!;
        if (selectedRow) {
            const id = selectedRow.id;
            const key = `layer_${selectedRow.window.category}¤${selectedRow.window.key}`;
            const layer = this.props.store.layers.find(x => x.key === key);
            const source = layer.olLayer.getSource() as ol.source.Vector;
            const features = source.getFeatures();
            const featuresToSelect = features.filter((feature, index) => {
                const fId = feature!.get('id');
                return fId === id;
            });

            this.selectInteraction.getFeatures().clear();
            featuresToSelect.map(feature => {
                this.selectInteraction.getFeatures().push(feature);
            });
        }
    }

    layersChanged(change: IArrayChange<Layer> | IArraySplice<Layer>) {
        if (change.type === "splice") {
            for (const layer of change.removed) {
                this.layers!.remove(layer.olLayer);
            }
            let i = this.numberOfBaseLayers;
            for (const layer of change.added) {
                this.layers!.insertAt(change.index + i, layer.olLayer);
                i++;
            }
        }
    }


    render() {
        return (
            <MapComponent
                layers={this.layers!}
                view={this.view!}
                extent={this.extent!}
                ref={el => this.map = el}
            />
        );
    }
}