import React from 'react';
import ReactDOM from 'react-dom';
import { autorun, reaction } from 'mobx';
import { inject } from 'mobx-react';
import _ from 'lodash';
import OpenLayers from '../../openlayers.js';
import configuration from 'configuration';
import Bounds from '../../bounds';

@inject('actions', 'ui')
class Map extends React.Component {

    /* eslint-disable react/sort-comp */
    dispatchers = [];
    currentLayers = [];
    /* eslint-enable react/sort-comp */

    store;

    selectedFeature = null;

    constructor(props) {
        super(props);
        this.store = props.store;
    }

    componentDidMount() {
        const mapDiv = ReactDOM.findDOMNode(this);
        const mapOptions = Object.assign({}, {
            theme: null,
            controls: [
                new OpenLayers.Control.Navigation(),
                new OpenLayers.Control.PanZoomBar(),
                new OpenLayers.Control.KeyboardDefaults({ observeElement: mapDiv }),
                new OpenLayers.Control.ZoomBox(),
                new OpenLayers.Control.Attribution(),
            ],
        }, configuration.mapConfiguration.openLayers);
        this.map = new OpenLayers.Map(mapDiv, mapOptions);
        this.initializing = this.initialize();
        this.dispatchers.push(autorun(() => this.zoomToBounds(this.store.bounds)));
        this.dispatchers.push(autorun(() => this.changeLayers(this.store.layers)));
        this.dispatchers.push(autorun(() => {
            if (this.store.selectedFeature !== null) {
                this.selectFeature(this.store.selectedFeature);
            } else {
                this.unselectFeature();
            }
        }));
        this.dispatchers.push(reaction(() => this.props.container.width, () => this.map.updateSize()));
        this.dispatchers.push(reaction(() => this.props.container.height, () => this.map.updateSize()));
    }

    componentWillUnmount() {
        for (const dispatcher of this.dispatchers) {
            dispatcher();
        }

        this.map.destroy();
    }

    onSelect = (feature) => {
        this.props.actions.selectFeatureFromMap(feature);
    }

    // Called from actions.js
    selectFeature(feature) {
        this.selectedFeature = feature;
        const layer = feature.layer;
        layer.drawFeature(feature, 'select');
    }

    onUnselect = (feature) => {
        this.props.actions.unselectFeature();
    }

    // Called from action.js
    unselectFeature() {
        const feature = this.selectedFeature;
        if (feature) {
            const layer = feature.layer;
            layer.drawFeature(feature, 'default');
            this.selectedFeature = null;
            this.clickHandler.unselectAll();
        }
    }

    initialize = () => this.addBaseLayers()
        .then(this.getBaseIndex)
        .then(this.addClickHandler)
        .then(this.setInitialZoom)
        .fail(error => {
            /* eslint-disable no-console */
            console.error(error);
            /* eslint-enable no-console */
        });

    addBaseLayers = () => configuration.mapConfiguration.layers().done((...layers) => {
        for (const layer of layers) {
            this.map.addLayer(layer);
        }
    });

    getBaseIndex = () => {
        const featuresLayer = new OpenLayers.Layer.Vector('featuresLayer');
        this.map.addLayer(featuresLayer);
        this.baseIndex = this.map.getLayerIndex(featuresLayer);
        this.map.removeLayer(featuresLayer);
    }

    addClickHandler = () => {
        // We create the hover- and clickHandler, but does not assign any layers yet
        // Layers are assigned during componentWillReceiveProps when layers are created or destroyed
        this.hoverHandler = new OpenLayers.Control.SelectFeature([], {
            hover: true,
            highlightOnly: true,
            selectStyle: {
                fillColor: '#f7b756',
                strokeColor: '#ee9900',
                strokeWidth: 10,
            },
        });

        this.map.addControl(this.hoverHandler);

        this.clickHandler = new OpenLayers.Control.SelectFeature([], { clickout: false });
        this.clickHandler.onSelect = this.onSelect;
        this.clickHandler.onUnselect = this.onUnselect;
        this.map.addControl(this.clickHandler);
    }

    zoomToBounds(bounds) {
        // There might be a race condition between bootstrap returning bounds
        // and OpenLayers initializing layers. We want OpenLayers to initialize first
        // and then zoom into the correct extent.
        if (bounds) {
            this.initializing.then(() => {
                this.map.zoomToExtent(Bounds.toOpenLayersBounds(bounds), true);
            });
        }
    }

    changeLayer = (layer) => {
        const foundLayers = this.map.getLayersBy('key', layer.key);
        let mapLayer = foundLayers.length ? foundLayers[0] : null;

        const styleMap = new OpenLayers.StyleMap({
            default: { ...OpenLayers.Feature.Vector.style.default,
                fillColor: layer.color,
                strokeColor: layer.color,
                strokeWidth: 3,
            },
            select: { ...OpenLayers.Feature.Vector.style.select,
                fillColor: '#f7b756',
                strokeColor: '#ee9900',
                strokeWidth: 3,
                strokeDashstyle: 'dashdot',
            },
        });

        if (mapLayer) {
            if (mapLayer.changed === layer.changed) {
                return;
            }
            mapLayer.removeAllFeatures();
        } else {
            mapLayer = new OpenLayers.Layer.Vector(layer.name, {
                styleMap,
            });

            _.assign(mapLayer, {
                key: layer.key,
                isQueryable: true,
                changed: layer.changed,
            });

            this.map.addLayer(mapLayer);
        }

        this.drawFeatures(mapLayer, layer);
    }

    changeLayers(layers) {
        for (const layer of layers) {
            this.changeLayer(layer);
        }

        for (const layer of this.map.getLayersBy('isQueryable', true)) {
            const found = layers.find(x => x.key === layer.key);
            if (!found) {
                this.map.removeLayer(layer);
            }
        }

        this.updateClickHandler();
    }

    drawFeatures(mapLayer, layer) {
        const featuresToDraw = [];
        const features = layer.features;
        const category = layer.category;
        for (let i = 0; i < features.length; i++) {
            for (let j = 0; j < features[i].wkts.length; j++) {
                const wkt = features[i].wkts[j];
                const geometry = OpenLayers.Geometry.fromWKT(wkt);
                const feature = new OpenLayers.Feature.Vector(geometry, {
                    id: features[i].id,
                    fields: features[i].fields,
                    category,
                });
                featuresToDraw.push(feature);
            }
        }

        mapLayer.addFeatures(featuresToDraw);
    }

    updateClickHandler() {
        // HoverHandler (and clickHandler) is first defined after baselayer is loaded
        if (!this.hoverHandler) return;

        const layers = this.map.getLayersBy('isQueryable', true);
        this.hoverHandler.setLayer(layers);
        this.clickHandler.setLayer(layers);
        if (layers.length) {
            this.hoverHandler.activate();
            this.clickHandler.activate();
        } else {
            this.hoverHandler.deactivate();
            this.clickHandler.deactivate();
        }
    }

    render() {
        return (<div className="mapcomponent">
            {this.props.children}
        </div>);
    }
}

export default Map;

