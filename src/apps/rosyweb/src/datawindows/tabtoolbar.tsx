import * as React from 'react';
import { inject } from 'mobx-react';
import ToolBar from '@rosy/component-toolbar';
import Actions from '../actions';
import * as types from '../types';

interface Properties {
    actions?: Actions;
    window: types.DataWindow;
}

@inject('actions')
export default class TabToolBar extends React.Component<Properties, {}> {

    pinWindow = () => {
        this.props.actions!.pinDataWindow(this.props.window);
    }

    toolbarOptions: kendo.ui.ToolBarOptions = {
        items: [
            { type: "button", text: "Pin window", click: this.pinWindow },
        ],
    };

    render() {
        return (
            <ToolBar options={this.toolbarOptions} />
        );
    }
}