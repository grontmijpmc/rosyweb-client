import * as React from 'react';
import { inject, observer } from 'mobx-react';
import * as $ from 'jquery';
import * as types from '../types';
// import * as reqwest from 'q-reqwest';
import Actions from '../actions';
import { FormValuesWithId } from '../stores/editwindow';
// import ToolBar from 'components/kendo/toolbar';
import TabToolBar from './tabtoolbar';
import PinnedToolBar from './pinnedtoolbar';
import { Dimensions } from '../stores/ui';
import * as moment from 'moment';
import { getDataModel } from './datawindowmanager';
moment.locale('da');
import ColumnsCustomizerFactory from './columnscustomizers/columnscustomizersfactory';
import CreateFieldDefinition from './fieldsfactory';
import CreateColumnDefinition from './columnsfactory';

function createKendoDataSource(model: types.DataWindowModel, filterString: string): kendo.data.DataSource {

    const fields = CreateFieldDefinition(model);

    const schema: kendo.data.DataSourceSchema = {
        model: {
            id: 'id',
            fields,
        },
        total: 'total',
        data: 'data',
    };
debugger;
    const ds = new kendo.data.DataSource({
        schema,
        // we have to handle dataoperations serverside because of potential large amounts of data
        pageSize: 100,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        serverGrouping: true,
        serverAggregates: true,
        transport: {
            read: {
                url: '/base/data',
                data: {
                    Model: model.model,
                    View: 'List',
                    FilterString: filterString,
                },
            },
        },
    });

    return ds;
}

export function createStore(model: types.DataWindowModel,
                            filterString: string,
                            title: string[]): types.DataWindowStore {
    const store: types.DataWindowStore = {
        dataSource: createKendoDataSource(model, filterString),

        loadData: function loadData(roadId: types.RoadId | null): void {
            // Kendo typings is wrong
            if (roadId) {
                (this.dataSource.options.transport!.read as any)!.data.RoadId = roadId;
            } else {
                (this.dataSource.options.transport!.read as any)!.data.RoadId = null;
            }
            this.dataSource.read();
        },

        setSelectedMapFeature: function setSelectedMapFeature(input: types.SelectedMapFeature): void {
            // do something
        },

        title,

    };

    return store;

}

function createGridConfiguration(
    model: types.DataWindowModel,
    store: types.DataWindowStore,
    hideRoadInfo: boolean,
    additionalOptions: Partial<kendo.ui.GridOptions> = {}) {

    const columns = CreateColumnDefinition(model, hideRoadInfo);

    const options: kendo.ui.GridOptions = Object.assign({
        dataSource: store.dataSource,
        columns,
        selectable: 'row',
        navigatable: true,
        sortable: {
            mode: "multiple",
            allowUnsort: true,
            showIndexes: true,
        },
        resizable: true,
        height: '100%',
        columnMenu: true,
        reorderable: true,
        pageable: true,
    }, additionalOptions);

    if (model.relatedModel) {

        const relatedModel = configuration.models.find(x => x.model === model.relatedModel);
        if (!relatedModel) {
            throw new Error(`model ${model.relatedModel} not found in configuration`);
        }
        const relfields = CreateFieldDefinition(relatedModel);

        const schema: kendo.data.DataSourceSchema = {
            model: {
                relfields,
            },
            data: 'data',
        };

        const relds = new kendo.data.DataSource({
            schema,
            transport: {
                read: {
                    url: '/base/data',
                    data: {
                        Model: model.relatedModel,
                        View: 'List',
                    },
                },
            },
        });

        const relColumns = CreateColumnDefinition(relatedModel, hideRoadInfo);
        options.detailInit = (e: kendo.ui.GridDetailInitEvent) => {
            (relds.options.transport!.read as any)!.data.Id = (e.data! as any).id;
            $("<div/>").appendTo(e.detailCell!).kendoGrid({
                dataSource: relds,
                columns: relColumns,
                scrollable: true,
            });
            relds.read();
        };
    }
    return options;
}

export function createReactComponent(
    window: types.DataWindow,
    model: types.DataWindowModel,
    store: types.DataWindowStore,
    hideRoadInfo: boolean,
    pinned = false) {

    interface Properties {
        actions?: Actions;
        gridOptions?: Partial<kendo.ui.GridOptions>;
        container?: Dimensions;
    }

    @inject('actions') @observer
    class DataWindow extends React.Component<Properties, {}> {
        private readonly dataSource: kendo.data.DataSource;
        private readonly gridConfiguration: kendo.ui.GridOptions;

        private root: Element | null | undefined;

        constructor(props: Properties) {
            super(props);
            this.dataSource = store.dataSource;
            this.gridConfiguration = createGridConfiguration(model, store, hideRoadInfo, { navigate: this.onNavigate });
        }

        componentDidMount() {
            const $el = $(this.root!);
            const grid = $el.kendoGrid(this.gridConfiguration);
            $el.on("dblclick", "tbody>tr", this.selectRow);
        }

        componentWillUnmont() {
            const $el = $(this.root!);
            $el.off("dblclick", "tbody>tr", this.selectRow);
            $el.data('kendoGrid').destroy();
        }

        private onNavigate = (e: kendo.ui.GridNavigateEvent) => {
            // When navigating the grid using the keyboard, make the whole row selectable
            if (e.element) {
                const row = e.element.parent();
                e.sender.select(row);
                const id = row.find('td.Id').text();
                this.props.actions!.datawindowGridRowSelected(window, id);
            }
        }

        selectRow = (e: JQuery.Event<HTMLElement>) => {
            const selectedRow = $(e.delegateTarget).data('kendoGrid').select();
            const grid = $(this.root!).data('kendoGrid');
            const selectedData = grid.dataItem(selectedRow);
            this.props.actions!.showUpdateWindow(window, selectedData.toJSON() as FormValuesWithId);
        }

        render() {
            return (
                <div style={{ height: '100%' }}>
                    {
                        pinned
                            ? <PinnedToolBar window={window} title={store.title} />
                            : <TabToolBar window={window} />
                    }
                    <div ref={element => this.root = element} />
                </div>
            );
        }
    }

    return DataWindow;
}
