import * as types from '../../types';
import { columnCustomizers } from '../customerizers';

export function createStyledColumn(column: kendo.ui.GridColumn, styleFunction: (value: any) => string) {

    const context = {
        func: styleFunction,
    };

    return kendo.template(
        "<div style='#= this.func(" + column.field + ") #'>#= " + column.field + "#</div>",
    ).bind(context);
}

export type GridColumnCustomizer = (column: kendo.ui.GridColumn) => void;

class ColumnsCustomizerFactory {

    columnsCustomizer(model: types.DataWindowModel): GridColumnCustomizer | undefined {
        const customizer = columnCustomizers[model.model];
        return customizer;
    }
}
export default new ColumnsCustomizerFactory();
