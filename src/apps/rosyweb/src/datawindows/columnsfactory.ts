import * as types from '../types';
import ColumnsCustomizerFactory from './columnscustomizers/columnscustomizersfactory';

export default function CreateColumnDefinition(model: DataWindowModel, hideRoadInfo: boolean): kendo.ui.GridColumn[] {
    const columns: kendo.ui.GridColumn[] = [{
        field: 'id',
        title: 'id',
        hidden: true,
        attributes: {
            class: 'Id',
        },
    }];

    const columnCustomizer = ColumnsCustomizerFactory.columnsCustomizer(model);
    for (const column of model.fields) {
        if (column.name === 'roadname' && hideRoadInfo) {
            continue;
        }
        const col: kendo.ui.GridColumn = {
            field: column.name,
            title: column.name,
            hidden: false,
            attributes: {},
        };

        if (column.type === 'Date') {
            col.format = `{0:d}`;
        }

        if (column.type === 'Boolean') {
            col.template = `#= ${column.name} ? 'yes' : 'no'  #`;
        }
        if (columnCustomizer) {
            columnCustomizer(col);
        }
        columns.push(col);
    }
    return columns;
}