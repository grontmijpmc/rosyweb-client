import { GridColumnCustomizer } from './columnscustomizers/columnscustomizersfactory';

import DamageSectionCustomizer from './columnscustomizers/damagesection';

interface ColumnCustomizers {
    [model: string]: GridColumnCustomizer;
}

export const columnCustomizers: ColumnCustomizers = {
    DamageSection: DamageSectionCustomizer,
};
