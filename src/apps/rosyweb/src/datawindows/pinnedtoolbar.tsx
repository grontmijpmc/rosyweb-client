import * as React from 'react';
import { inject } from 'mobx-react';
import ToolBar from '@rosy/component-toolbar';
import Actions from '../actions';
import * as types from '../types';

interface Properties {
    actions?: Actions;
    window: types.DataWindow;
    title: string[];
}

@inject('actions')
export default class PinnedToolBar extends React.Component<Properties, {}> {

    onCloseWindow = () => {
        this.props.actions!.disableDataWindow(this.props.window);
    }

    toolbarOptions: kendo.ui.ToolBarOptions = {
        items: [
            { type: 'separator',
              template: `<span style='padding: 10px 15px'>
                         ${this.props.title.join("<span style='color: #ccc'> / </span>")}
                         </span>` },
            { type: 'button', text: 'Close', click: this.onCloseWindow },
        ],
    };

    render() {
        return (
            <ToolBar options={this.toolbarOptions} />
        );
    }
}