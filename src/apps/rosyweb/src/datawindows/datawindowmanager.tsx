import * as React from 'react';
import { observable, action, computed, asMap, IObservableArray } from 'mobx';
import * as types from './../types';
import * as reqwest from '@rosy/utils-request';
import * as MapInterfaces from './../interfaces/map';

import { createStore, createReactComponent } from './configurationfactory';
import * as configuration from 'configuration';

interface Geometry {
    id: string;
    wkt: string;
}

interface Category { category: string; geometries: Geometry[]; }

interface GeometryData {
    categories?: Category[];
}

interface Window {
    identifier: types.DataWindow;
    refCount: number;
}

interface WindowConfiguration {
    identifier: types.DataWindow;
    store: types.DataWindowStore;
    model: types.DataWindowModel;
    title: [string, string];
}

export function getDataModel(category: types.DataWindowCategory): types.DataWindowModel {
    const model = configuration.models.find(x => x.model === category);
    if (!model) {
        throw new Error(`model ${category} not found in configuration`);
    }
    return model;
}

interface MapGeometries { category: string; geometries: MapInterfaces.Feature[]; }

export default class DataWindowManager {

    private selectedRoad: types.RoadId | null = null;

    private _activeWindows: IObservableArray<Window> = observable([]);
    private _configuration: WindowConfiguration[] = [];

    private _mapGeometries: MapGeometries[] = [];

    setSelectedRoad(road: types.RoadId) {
        this.selectedRoad = road;
        this.loadNewRoadInActiveWindows(road);
    }
    resetSelectedRoad() {
        this.selectedRoad = null;
        this.loadNewRoadInActiveWindows(null);
    }

    @action activateWindow(window: types.DataWindow, filterString: string, title: [string, string]) {
        if (!this.isWindowActive(window)) {
            this._activeWindows.push({ identifier: window, refCount: 1 });
            const model = getDataModel(window.category);
            const store: types.DataWindowStore = createStore(model, filterString, title);
            const config: WindowConfiguration = {
                identifier: window,
                store,
                model,
                title,
            };
            this._configuration.push(config);

            if (this.selectedRoad) {
                store.loadData(this.selectedRoad);
            }
        } else {
            const w = this.getWindow(window);
            w.refCount++;
        }
    }

    @action deactivateWindow(window: types.DataWindow) {
        const w = this.getWindow(window);
        if (w.refCount > 0) {
            w.refCount--;
        }

        if (w.refCount <= 0) {
            // Window is completely closed
            // Remove configuration to allow the store to
            // be GC'ed
            this._activeWindows.remove(w);
            const index = this._configuration.findIndex(x => this.identifierMatch(x.identifier, window));
            if (index > -1) {
                this._configuration.splice(index, 1);
            }
        }
    }

    @action pinWindow(window: types.DataWindow) {
        const w = this.getWindow(window);
        w.refCount++;
    }

    getTitle(window: types.DataWindow): [string, string] {
        const conf =  this._configuration.find(x => this.identifierMatch(x.identifier, window));
        if (!conf) {
            throw new Error("Model not found");
        }
        return conf.title;
    }

    getReactElement(window: types.DataWindow, hideRoadInfo: boolean, pinned: boolean) {
        const model = getDataModel(window.category);
        const c = this.getConfiguration(window);
        const Component = createReactComponent(window, // tslint:disable-line:variable-name
            c.model,
            c.store,
            hideRoadInfo,
            pinned,
        );
        return <Component />;
    }

    @computed get activeWindows(): types.DataWindow[] {
        return this._activeWindows.filter(x => x.refCount > 0).map(x => x.identifier);
    }

    @computed get hasPinnedWindows(): boolean {
        return this._activeWindows.filter(x => x.refCount > 1).length > 0;
    }

    @computed get pinnedWindows(): types.DataWindow[] {
        return this._activeWindows.filter(x => x.refCount > 1).map(w => w.identifier);
    }

    private getStore(category: types.DataWindowCategory): types.DataWindowStore {
        const c = this._configuration.find(x => x.identifier.category === category);
        if (!c) {
            throw new Error("Model not found");
        }
        return c.store;

    }

    private getWindow(window: types.DataWindow): Window {
        const w = this._activeWindows.find(x => this.identifierMatch(x.identifier, window));
        if (!w) {
            throw new Error(`Window ${window.category} ${window.key} not found in _activeWindows`);
        }
        return w;
    }

    private getConfiguration(window: types.DataWindow): WindowConfiguration {
        const c = this._configuration.find(x => this.identifierMatch(x.identifier, window));
        if (!c) {
            throw new Error("Model not found");
        }
        return c;
    }

    private identifierMatch(w1: types.DataWindow, w2: types.DataWindow) {
        return w1.category === w2.category && w1.key === w2.key;
    }

    private isWindowActive(window: types.DataWindow): boolean {
        if (!this._activeWindows.find(x => this.identifierMatch(x.identifier, window))) {
            return false;
        }
        return this.getWindow(window).refCount > 0;
    }

    private loadNewRoadInActiveWindows(selectedRoad: types.RoadId | null) {
        for (const window of this.activeWindows) {
            const model = getDataModel(window.category);
            if (model) {
                const config = this.getConfiguration(window);
                config.store.loadData(selectedRoad);
            }
        }
    }

    markSelectedFeaturesfromMap(category: types.DataWindowCategory, id: types.Guid) {
        const model = getDataModel(category);
        const store = this.getStore(category);
        store.setSelectedMapFeature({ category, id });
    }
}