import * as types from '../types';

export default function CreateFieldDefinition(model: types.DataWindowModel): { [key: string]: any } {
    const fields: { [key: string]: any } = {};
    for (const field of model.fields) {
        fields[field.name] = createFieldConfiguration(field);
    }
    return fields;
}

function parseDate(input: string | null): Date | null | undefined {
    if (input === null) return null;
    const b = input.split(/\D+/) as any;
    const date = new Date(Date.UTC(b[0], b[1] - 1, b[2], b[3], b[4], b[5]));
    return date;
}

function createFieldConfiguration(field: types.DataWindowField) {
    switch (field.type) {
        case 'Date':
            return { type: 'date', parse: parseDate };
    }
    return { type: field.type };
}