interface Localizer {
    [index: string]: string;
}

declare module 'localizer' {
    export = localizer;
}

declare const localizer: Localizer;
