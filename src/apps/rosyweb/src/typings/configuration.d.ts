/// <reference path="../../../../../node_modules/@types/openlayers/index.d.ts" />



interface Filter {
    filterString: string;
    color: string;
}



interface LayerGroups {
    [index: string]: Layer;
}

interface Layer {
    filter: FilterConfiguration;
    mainApplication?: string;
}

interface FilterConfiguration {
    autocomplete: (propertyPath: string) => string;
    propertyDetails: string;
    listPropertiesUrl: string;
    listFiltersUrl: string;
    saveUrl: string;
    loadUrl: string;
    specialFilters?: SpecialFilters;
}
interface SpecialFilters {
    [index: string]: Filter;
}

interface MapConfiguration {
    openLayers: any;
    layers: { () : JQueryPromise<ol.layer.Layer> }
}

type ServerColumnType = 'String' | 'Relation' | 'Lookup' | 'Enum' | 'Boolean' | 'Date' | 'Number';

interface DataWindowField {
    name: string;
    type: ServerColumnType;
    flags: string;
}

interface DataWindowEnumField extends DataWindowField {
    allValues: string[];
}


interface DataWindowModel {
    model: string;
    relatedModel: string;
    listView: string;
    editView: string;
    fields: Array<DataWindowField | DataWindowEnumField>;
}



interface Configuration {
    layerGroups: LayerGroups;
    mapConfiguration: MapConfiguration;
    models: DataWindowModel[];
}

declare const configuration: Configuration;

declare module 'configuration' {
    export = configuration;
}
