import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';
import { ButtonGroup, Button } from 'react-bootstrap';
import FilterStore from '../../stores/filter';
import RoadlistStore from '../../stores/roadlist';
import LeftBarStore from '../../stores/leftbar';
import Actions from '../../actions';
import Filter from '../../components/filter/filter';
import Roadlist from '../../components/roadlist';

interface Properties {
    leftbarStore: LeftBarStore;
    filterStore: FilterStore;
    roadlistStore: RoadlistStore;
    actions?: Actions;
}

@inject('actions') @observer
class Leftbar extends React.Component<Properties, {}> {

    @action onRoadsSelected = () => {
        this.props.actions!.roadlistPanelSelected();
    }

    @action onMapLayersSelected = () => {
        this.props.actions!.maplayersPanelSelected();
    }

    render() {
        return (
            <div style={{ height: '100%', display: 'table', tableLayout: 'fixed' }}>
                <div style={{ display: 'table-row', height: "1px" }}>
                    <ButtonGroup justified={true}>
                        <Button
                            href="#"
                            onClick={this.onRoadsSelected}
                            active={this.props.leftbarStore.selectedPanel === 'ROADS'}
                        >
                            Vejliste
                        </Button>
                        <Button
                            href="#"
                            onClick={this.onMapLayersSelected}
                            active={this.props.leftbarStore.selectedPanel === 'MAPLAYERS'}
                        >
                            Kortlag
                        </Button>
            </ButtonGroup>
            </div>
            {
                this.props.leftbarStore.selectedPanel === 'ROADS'
                ? <Roadlist store={this.props.roadlistStore} />
                : <Filter store={this.props.filterStore.panel} />
            }
            </div>
        );
    }
}

export default Leftbar;
