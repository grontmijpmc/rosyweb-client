import * as React from 'react';
import { inject, observer } from 'mobx-react';
import * as types from '../../types';

interface Properties {
    window: types.DataWindow;
    title: [string, string];
    closeClicked(window: types.DataWindow): void;
    closeable: boolean;
}

function generateTitleMarkup(title: [string, string]) {
    return (
        <span> {title[0]}<span style={{ color: "#ccc" }}> / </span>{title[1]}</span>
    );
}

@inject('actions') @observer
class TabHeader extends React.Component<Properties, {}> {

    closeTab = () => {
        this.props.closeClicked(this.props.window);
    }

    render() {
        return (
            <span className="tab-title">
                <span className="tab-title-label">
                    {generateTitleMarkup(this.props.title)}
                    {
                        this.props.closeable
                        && <button className="close tab-title-close" onClick={this.closeTab}>×</button>
                    }
                </span>
            </span>
        )

    }
}

export default TabHeader;