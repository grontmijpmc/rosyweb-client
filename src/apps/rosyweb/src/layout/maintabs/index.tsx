import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { Tabs, Tab, Panel, Nav, NavItem } from 'react-bootstrap';
import { Dimensions } from '../../stores/ui';
import WindowManager from '../../datawindows/datawindowmanager';
import Actions from '../../actions';
import * as types from '../../types';
import TabHeader from './tabheader';

interface Properties {
    windowManager: WindowManager;
    container?: Dimensions;
    actions?: Actions;
    selectedPanel: types.SelectedPanel;
}

function getUniqueKey(identifier: types.DataWindow) {
    return `${identifier.category}-${identifier.key}`;
}

@inject('actions') @observer
class MainTabs extends React.Component<Properties, {}> {

    onClose = (window: types.DataWindow) => {
        this.props.actions!.disableDataWindow(window);
    }

    renderNavItems() {
        return this.props.windowManager.activeWindows
            .map(window => {
                return (
                    <NavItem eventKey={getUniqueKey(window)} key={getUniqueKey(window)}>
                        <TabHeader
                            key={getUniqueKey(window)}
                            window={window}
                            title={this.props.windowManager.getTitle(window)}
                            closeable={true}
                            closeClicked={this.onClose}
                        />
                    </NavItem>
                );
            });
    }

    renderPanes(style: React.CSSProperties) {
        return this.props.windowManager.activeWindows
            .map(window => {
                return (
                    <Tab.Pane
                        // @ts-ignore: mountonenter missing in d.ts
                        mountOnEnter={true}
                        key={getUniqueKey(window)}
                        eventKey={getUniqueKey(window)}
                        animation={true}
                        style={style}
                    >
                        {this.props.windowManager.getReactElement(window, this.props.selectedPanel === 'ROADS', false)}
                    </Tab.Pane>
                );
            });
    }

    render() {

        const width = this.props.container && (this.props.container.width - 19) + 'px';
        const height = this.props.container && (this.props.container.height - 88) + 'px';

        const style = {
            width,
            height,
        };

        return (
            <Tab.Container id="maintabs" defaultActiveKey="overview">
                <div>
                    <Nav bsStyle="tabs">
                        <NavItem eventKey="overview">Overblik</NavItem>
                        {this.renderNavItems()}
                    </Nav>
                    <Tab.Content animation={true}>
                        <Tab.Pane eventKey="overview" animation={true}>
                            Data her
                    </Tab.Pane>
                        {this.renderPanes(style)}
                    </Tab.Content>
                </div>
            </Tab.Container>
        );
    }
}

export default MainTabs;