import * as React from 'react';
import { observer } from 'mobx-react';
import { Dimensions } from "../../stores/ui";

interface Properties {
    container?: Dimensions;
}

@observer
export default class PinnedWindow extends React.Component<Properties, {}> {

    render() {
        const height = (this.props.container) && (this.props.container.height - 48) + "px";
        const style = {
            height,
        };

        return (
            <div style={style}>
                {this.props.children}
            </div>
        );
    }
}