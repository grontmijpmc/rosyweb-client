import * as React from 'react';
import * as $ from 'jquery';

import '@progress/kendo-ui/js/kendo.datepicker.js';

export interface Properties extends kendo.ui.DatePickerOptions {
    disabled?: boolean;
    readonly?: boolean;
    format?: string;
    onChange: (value: Date, event?: kendo.ui.DatePickerChangeEvent) => void;
}

class DateInput extends React.Component<Properties, {}> {

    static defaultProps = {
        format: 'dd-MM-yyyy',
    };

    element: Element | null | undefined;

    componentDidMount() {
        const $el = $(this.element!);
        const { disabled, readonly, ...options } = this.props;

        $el.kendoDatePicker(options);
        const widget = $el.data('kendoDatePicker');

        if (this.props.disabled) {
            widget.enable(false);
        } else if (this.props.readonly) {
            widget.readonly(true);
        }
    }

    toUiFormat(date: Date) {
        return kendo.toString(date, this.props.format as string);
    }

    fromUiFormat(value?: string) {
        if (value) {
            return kendo.parseDate(value, this.props.format);
        } else {
            return undefined;
        }
    }

    componentDidUpdate(prevProps: Properties) {
        const widget = this.getWidget();

        if (this.props.value && this.props.value !== prevProps.value) {
            widget.value(this.props.value);
        }

        if (this.props.disabled !== prevProps.disabled) {
            widget.enable(!this.props.disabled);
        } else if (this.props.readonly !== prevProps.readonly) {
            widget.readonly(!!this.props.readonly);
        }
    }

    getWidget() {
        return $(this.element!).data('kendoDatePicker');
    }

    onChange(event: kendo.ui.DatePickerChangeEvent) {
        const widget = event.sender;
        this.props.onChange(widget.value(), event);
    }

    render() {
        return <input type="text" ref={el => this.element = el} />;
    }

}

export default DateInput;
