import * as React from 'react';
import * as $ from 'jquery';

// tslint:disable-next-line:no-var-requires
require('@progress/kendo-ui/js/kendo.combobox.js');

export interface Properties extends kendo.ui.ComboBoxOptions {
    width?: string | number;
    displayField: string;
    valueField: string;
    onChange: (e: any) => void;
    dataSource: any;
    id: string;
    // Properties here
}

export default class Component extends React.Component<Properties, { }> {

    instance: Element | null | undefined;
    $instance: JQuery | null | undefined;

    componentDidMount() {
        if (!this.instance) return;

        this.$instance = $(this.instance);

        if (this.props.width) {
            this.$instance.width(this.props.width);
        }

        const { onChange, displayField, valueField, ...other } = this.props;

        const kendoProperties = {
            autoBind: Array.isArray(this.props.dataSource),
            dataTextField: displayField,
            dataValueField: valueField,
            change: this.onChange,
        };

        const properties = { ...kendoProperties, ...other };

        this.$instance.kendoComboBox(properties);

        if (this.props.value) {
            this.$instance.data('kendoComboBox').value(this.props.value);
        }

    }

    componentWillReceiveProps(nextProps: Properties) {
        if (Array.isArray(this.props.dataSource) && this.props.dataSource.length > 0) {
            const elementType = typeof this.props.dataSource[0];
            if (!this.props.displayField
                && elementType !== 'string'
                && elementType !== 'number'
                && elementType !== 'boolean') {
                // tslint:disable-next-line:no-console
                console.warn('When dataSource is an array of objects, the prop displayField is mandatory!');
            }
        }
    }

    componentDidUpdate(prevProps: Properties) {
        const widget = this.$instance!.data('kendoComboBox');

        if (prevProps.dataSource !== this.props.dataSource) {
            widget.setDataSource(this.props.dataSource);
        }

        if (this.props.value) {
            widget.value(this.props.value);
        }
    }

    componentWillUnmount() {
        this.$instance!.data('kendoComboBox').destroy();
        this.instance = null;
        this.$instance = null;
    }

    onChange(event: kendo.ui.ComboBoxChangeEvent) {
        const model = event.sender.dataItem();

        if (!model) return;

        let nextValue: any;
        if (this.props.valueField) {
            nextValue = model.get(this.props.valueField);
        } else {
            nextValue = model.toJSON ? model.toJSON() : model;
        }

        // Reset the newly selected value and let the one-way databinding work
        if (this.props.value) {
            event.sender.value(this.props.value);
        }

        this.props.onChange(nextValue);
    }

    render() {
        const props: { id?: string } = {};
        if (this.props.id) {
            props.id = this.props.id;
        }

        return (
            <select {...props} ref={el => this.instance = el} />
        );
    }
}