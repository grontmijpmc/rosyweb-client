interface Localizer {
    [key: string]: string;
}

declare const localizer: Localizer;

import * as React from 'react';
import { extend, clone } from 'lodash-es';

import '@progress/kendo-ui/js/kendo.multiselect.js';

export interface Properties extends kendo.ui.MultiSelectOptions {
    displayField?: string;
    valueField?: string;
    minWidth?: string | number;
    disabled?: boolean;
    readonly?: boolean;
    onChange: (value: any, event: kendo.ui.MultiSelectEvent) => void;
}

class MultiSelect extends React.Component<Properties, {}> {
    static defaultProps = {
        minWidth: 150,
    };

    element: Element | null | undefined;

    componentDidMount() {
        const $el = $(this.element!);

        const { displayField, valueField, minWidth, ...options } = this.props;

        extend(options, {
            dataTextField: displayField,
            dataValueField: valueField,
        });

        $el.kendoMultiSelect(options);

        if (this.props.disabled) {
            $el.data('kendoMultiSelect').enable(false);
        } else if (this.props.readonly) {
            $el.data('kendoMultiSelect').readonly(true);
        }
    }

    componentDidUpdate(prevProps: Properties) {
        const widget = this.getWidget();

        if (this.props.value !== prevProps.value) {
            widget.value(this.props.value);
        }

        if (this.props.disabled !== prevProps.disabled) {
            widget.enable(!this.props.disabled);
        } else if (this.props.readonly !== undefined && this.props.readonly !== prevProps.readonly) {
            widget.readonly(this.props.readonly);
        }
    }

    componentWillUnmount() {
        this.getWidget().destroy();
    }

    onChange(event: kendo.ui.MultiSelectChangeEvent) {
        const widget = event.sender;
        const values = clone(widget.value());

        widget.value(this.props.value);
        this.props.onChange(values, event);
    }

    getWidget(): kendo.ui.MultiSelect {
        return $(this.element!).data('kendoMultiSelect');
    }

    render() {
        return (
            <select
                data-placeholder={localizer.Selectbox_select}
                style={{ minWidth: this.props.minWidth }}
                ref={el => this.element = el}
            />
        );
    }
}

export default MultiSelect;
