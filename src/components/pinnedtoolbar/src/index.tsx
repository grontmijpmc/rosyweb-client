import * as React from 'react';
import ToolBar from '@rosy/component-toolbar';

export interface Properties {
    title: string[];
    callOnClosePinnedWindow?: () => void;
}

export default class PinnedToolBar extends React.Component<Properties, {}> {

    onCloseWindow = ( ) => {
        if (this.props.callOnClosePinnedWindow) {
        this.props.callOnClosePinnedWindow();
        }
    }

    toolbarOptions: kendo.ui.ToolBarOptions = {
        items: [
            { type: 'separator',
              template:
              `<span style='padding: 10px 15px'>
                ${this.props.title.join("<span style='color: #ccc'> / </span>")}
              </span>` },
            { type: 'button', text: 'Close', click: this.onCloseWindow },
        ],
    };

    render() {
        return (
            <ToolBar options={this.toolbarOptions} />
        );
    }
}
