import * as React from 'react';
import * as $ from 'jquery';
import Pane, { Properties as PaneProperties } from './pane';
import { observable, action, IObservableArray } from 'mobx';
import { observer } from 'mobx-react';
import { debounce } from 'lodash-es';

import '@progress/kendo-ui/js/kendo.splitter';

export interface Properties {
    orientation: 'vertical' | 'horizontal';
    onCollapse?(e: kendo.ui.SplitterCollapseEvent): void;
    onContentLoad?(e: kendo.ui.SplitterContentLoadEvent): void;
    onError?(e: kendo.ui.SplitterErrorEvent): void;
    onExpand?(e: kendo.ui.SplitterExpandEvent): void;
    onResize?(e: kendo.ui.SplitterEvent): void;
}

export interface Dimensions {
    width: number;
    height: number;
}

@observer
export class Splitter extends React.Component<Properties, { }> {

    private element: Element | null | undefined;
    private instance: JQuery | undefined;

    debouncedOnResize: (e: kendo.ui.SplitterEvent) => void;
    @observable paneDimensions: IObservableArray<Dimensions>;

    constructor(props: Properties) {
        super(props);
        this.debouncedOnResize = debounce(this.onResize, 200);
        this.paneDimensions = observable([]);
        this.initializePaneDimensions();
    }

    private initializePaneDimensions() {
        const paneDimensions: Dimensions[] = [];
        React.Children.forEach(this.props.children, () => paneDimensions.push({ width: 0, height: 0 }));
        this.paneDimensions.replace(paneDimensions);
    }

    onResize = (e: kendo.ui.SplitterEvent) => {
        if (this.props.onResize) {
            this.props.onResize(e);
        }
        this.populateDimensions();
    }

    @action populateDimensions() {
        if (this.instance) {
            this.instance.children(".k-pane").each((i, child) => {
                const $child = $(child);
                if (this.paneDimensions.length < i + 1) {
                    this.paneDimensions.push({ width: 0, height: 0 });
                }
                this.paneDimensions[i].width = $child.width() || 0;
                this.paneDimensions[i].height = $child.height() || 0;
            });
        }
    }

    componentDidMount() {
        this.instance = $(this.element!);
        this.initializeSplitter();
        this.populateDimensions();
    }

    componentDidUpdate() {
        // We need to cast splitter to any as we call
        // private methods on it.
        const splitter = this.instance!.data('kendoSplitter') as any;
        splitter.options.panes = this.getPaneProperties();
        splitter._initPanes();
        splitter._removeSplitBars();
        splitter.resize(true);
    }

    componentWillUnmount() {
        if (this.instance) {
            this.instance.data('kendoSplitter').destroy();
        }
    }

    private initializeSplitter() {
        if (this.instance) {
            const configuration: kendo.ui.SplitterOptions = {
                orientation: this.props.orientation,
                collapse: this.props.onCollapse,
                contentLoad: this.props.onContentLoad,
                error: this.props.onError,
                expand: this.props.onExpand,
                resize: this.onResize,
            };
            configuration.panes = this.getPaneProperties();
            this.instance.kendoSplitter(configuration);
        }
    }

    private getPaneProperties(): kendo.ui.SplitterPane[] {
        const paneProperties: kendo.ui.SplitterPane[] = [];
        React.Children.forEach(this.props.children, child => {
            if (child) {
                const props = (child as any).props;
                const { children, ...properties } = props;
                paneProperties.push(properties);
            }
        });
        return paneProperties;
    }

    applyContainerToChilds(children: React.ReactNode) {
        let i = 0;
        if (!this.paneDimensions) {
            // Usually done in componentWillMount, but because of hot loading
            // it might not happen
            this.initializePaneDimensions();
        }

        const clonedChildren = React.Children.map(this.props.children, child => {
            let result: React.ReactChild;
            if (React.isValidElement(child)) {
                const container = this.paneDimensions[i];
                i++;
                result = React.cloneElement(child as any, { container });
            } else {
                result = child;
            }
            return result;
        });

        return clonedChildren;
    }

    render() {
        const style = {
            height: "100%",
            width: "100%",
            border: 0,
        };
        return (
            <div style={style} ref={el => this.element = el}>
                {this.applyContainerToChilds(this.props.children)}
            </div>
        );
    }
}

export { Pane };