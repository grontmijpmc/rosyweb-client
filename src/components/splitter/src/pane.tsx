import * as React from 'react';
import { observer } from 'mobx-react';

export interface Properties extends kendo.ui.SplitterPane {
    container?: {
        width: number,
        height: number,
    };
}

@observer
export default class Pane extends React.Component<Properties, {}> {

    render() {
        return (
            <div>
                {
                    React.Children.map(this.props.children, child => {
                        if (React.isValidElement(child)) {
                            console.log(child);
                            return React.cloneElement(child as any, { container: this.props.container });
                        } else {
                            console.log("Not element", child);
                            return child;
                        }
                    })
                }
            </div>
        );
    }
}