import * as React from 'react';

export interface Properties {
    visible?: boolean;
    width?: number;
    height?: number;
    hideModal?: (e: kendo.ui.WindowCloseEvent) => void;
}

export default class Window extends React.Component<Properties, {}> {

    private element: Element | null | undefined;

    private getOptionsFromProps(props: Properties): kendo.ui.WindowOptions {
        const options: kendo.ui.WindowOptions = {
            visible: this.props.visible || false,
            width: this.props.width || 600,
        };

        options.position = {
            top: 200,
            left: "40%",
        };

        options.close = this.onClose;

        return options;
    }

    private onClose = (e: kendo.ui.WindowCloseEvent) => {
        if (this.props.hideModal) {
            this.props.hideModal(e);
        }
    }

    componentDidMount() {
        const options = this.getOptionsFromProps(this.props);
        $(this.element!).kendoWindow(options);
    }

    componentWillUnmount() {
        $(this.element!).data('kendoWindow').destroy();
    }

    componentWillReceiveProps(props: Properties) {
        const options = this.getOptionsFromProps(props);
        const window = $(this.element!).data('kendoWindow');
        window.setOptions(options);
        if (props.visible) {
            window.open();
        }
    }

    render() {
        return (
            <div ref={el => this.element = el}>
                {this.props.visible && this.props.children}
            </div>
        );
    }
}