import * as React from 'react';

export interface Properties {
    title?: string;
}

class DialogHeader extends React.Component<Properties, {}> {

    render() {
        let headerComponent = this.props.children;
        if (this.props.title) {
            headerComponent = <h3 className="panel-title">{this.props.title}</h3>;
        }

        return <div className="dialog panel-heading">{headerComponent}</div>;
    }
}

export default DialogHeader;
