import * as React from 'react';

class Body extends React.Component<{}, {}> {

    render() {
        return (
            <div className="dialog panel-body">
                {this.props.children}
            </div>
        );
    }
}

export default Body;
