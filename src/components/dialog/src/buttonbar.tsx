import * as React from 'react';

class ButtonBar extends React.Component<{}, {}> {

    render() {
      return <div className="dialog buttonbar">{this.props.children}</div>;
    }
}

export default ButtonBar;

