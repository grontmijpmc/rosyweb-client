import * as React from 'react';

class Footer extends React.Component<{}, {}> {

    render() {
		return <div className="dialog panel-footer">{this.props.children}</div>;
    }
}

export default Footer;
