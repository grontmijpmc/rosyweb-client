import * as React from 'react';
import Header from './header';
import Footer from './footer';
import Body from './body';
import ButtonBar from './buttonbar';

export interface Properties {
    className?: string;
}

class Dialog extends React.Component<Properties, {}> {

    static Header = Header;
    static Body = Body;
    static Footer = Footer;
    static ButtonBar = ButtonBar;

    render() {
        const className = this.props.className
            ? `${this.props.className} dialog panel panel-default`
            : 'dialog panel panel-default';

            return <div className={className}>{this.props.children}</div>;
    }
}

export default Dialog;
