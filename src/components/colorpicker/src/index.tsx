import * as React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';
import { SwatchesPicker } from 'react-color';
import Dialog from '@rosy/component-dialog';

export interface Properties {
    color: string;
    onChange?: (color: string) => void;
    onCloseDialog: () => void;
    onOkDialog: (color: string) => void;
    className?: string;
}

@observer
class ColorPicker extends React.Component<Properties, {}> {
    @observable color: string;

    constructor(props: Properties) {
        super(props);
        this.color = this.props.color;
    }

    closeDialog = () => {
        this.props.onCloseDialog();
    }

    okDialog = () => {
        this.props.onOkDialog(this.color);
    }

    @action
    handleChange = (color: any) => {
        const hexColor: string = color.hex;
        this.color = hexColor;
        if (this.props.onChange) {
            this.props.onChange(hexColor);
        }
        this.okDialog();
    }

    render() {
        return (
            <div className="filtercolorpicker__container">
                <SwatchesPicker color={this.color} onChangeComplete={this.handleChange} />
            </div>
        );
    }
}

export default ColorPicker;