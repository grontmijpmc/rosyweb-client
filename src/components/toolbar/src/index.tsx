import * as React from 'react';
import * as $ from 'jquery';

// tslint:disable-next-line:no-var-requires
require('@progress/kendo-ui/js/kendo.toolbar.js');

export interface Properties {
    options: kendo.ui.ToolBarOptions;
}

export default class Toolbar extends React.Component<Properties, { }> {

    private element: Element | null | undefined;

    componentDidMount() {
        const $el = $(this.element!);
        $el.kendoToolBar(this.props.options);
    }

    componentWillUnmount() {
        const $el = $(this.element!);
        $el.data('kendoToolBar').destroy();
    }

    render() {
        return <div ref={el => this.element = el} />;
    }
}