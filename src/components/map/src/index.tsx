import * as React from 'react';
import ol from '@rosy/utils-openlayers';

export interface Properties {
    // Properties here
    layers: ol.Collection<ol.layer.Layer>;
    view: ol.View;
    extent: ol.Extent;
    width?: number;
    height?: number;

}

export default class Component extends React.Component<Properties, { }> {

    domElement: Element | null | undefined;
    map: ol.Map | undefined;

    setRef(domElement: Element | null) {
        this.domElement = domElement;
    }

    componentDidMount() {
        const config: olx.MapOptions = {
            layers: this.props.layers,
            view: this.props.view,
            target: this.domElement!,
        };

        this.map = new ol.Map(config);
    }

    componentWillUnmount() {
        this.map!.setTarget(null);
    }

    componentWillReceiveProps(newProps: Properties) {
        if (newProps.extent !== this.props.extent) {
            this.zoomToExtent(newProps.extent);
        }
    }

    zoomToExtent(extent: ol.Extent | null) {
        if (extent) {
            const options: olx.view.FitOptions = {
                size: this.map!.getSize(),
            };

            this.map!.getView().fit(extent, options);
        }
    }

    addControl(control: ol.control.Control) {
        this.map!.addControl(control);
    }

    addInteraction(interaction: ol.interaction.Interaction) {
        this.map!.addInteraction(interaction);
    }
    removeInteraction(interaction: ol.interaction.Interaction) {
        this.map!.removeInteraction(interaction);
    }

    updateSize() {
        this.map!.updateSize();
    }

    unMount() {
        this.map!.setTarget(null);
    }

    render() {
        return <div ref={el => this.domElement = el} style={{ height: "100%" }} />;
    }
}