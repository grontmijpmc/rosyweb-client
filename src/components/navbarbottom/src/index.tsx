import * as React from 'react';

export  class NavBarBottom extends React.Component<{}, {}> {

    render() {
        const style = {
            paddingTop: '7px',
        };
        return (
            <div className="navbar navbar-default navbar-fixed-bottom" style={style}>
                <div className="container-fluid">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
