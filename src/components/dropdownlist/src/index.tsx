import * as React from 'react';
import * as $ from 'jquery';
import { extend, isArray, isEqual, pick, isObject, isFunction } from 'lodash-es';

import '@progress/kendo-ui/js/kendo.dropdownlist.js';

export interface Properties extends kendo.ui.DropDownListOptions {
    width?: string | number;
    displayField?: string;
    valueField?: string;
    disabled?: boolean;
    readonly?: boolean;
    onChange: (e: any) => void;
    id?: string;
}

class DropDownList extends React.Component<Properties, {}> {

    static defaultProps = {
        disabled: false,
        readonly: false,
        filter: 'startwith',
    };

    selectElement: Element | null | undefined;

    componentDidMount() {
        const $el = $(this.selectElement!);

        if (this.props.width) {
            $el.css({ width: this.props.width });
        }

        const { onChange, displayField, valueField, ...other } = this.props;

        const kendoProperties = {
            autoBind: isArray(this.props.dataSource),
            dataTextField: displayField,
            dataValueField: valueField,
            change: this.onChange,
        };

        extend(kendoProperties, other);
        $el.kendoDropDownList(kendoProperties);

        if (this.props.value) {
            $el.data('kendoDropDownList').value(this.props.value);
        }
    }

    componentWillReceiveProps(nextProps: Properties) {
        /* tslint:disable:no-console */
        const cantChange = ['template', 'valueField', 'displayField', 'placeholder', 'filter'];
        console.assert(isEqual(pick(nextProps, cantChange), pick(this.props, cantChange)),
            'these props cant change after mount');

        if (isArray(this.props.dataSource) && this.props.dataSource.length > 0) {
            const element = this.props.dataSource[0];
            if (isObject(element) && !this.props.displayField) {
                console.error('When dataSource is an array of objects, the prop displayField is mandatory!');
            }
        }
        /* tslint:enable:no-console */
    }

    componentDidUpdate(prevProps: Properties) {
        const widget = this.getWidget();

        if (prevProps.dataSource !== this.props.dataSource) {
            widget.setDataSource(this.props.dataSource);
        }

        if (this.props.value) {
            widget.value(this.props.value);
        }
    }

    componentWillUnmount() {
        const widget = this.getWidget();
        widget.destroy();
    }

    onChange = (event: kendo.ui.DropDownListChangeEvent) => {
        const model = event.sender.dataItem();

        let nextValue;
        if (this.props.valueField) {
            nextValue = model ? model.get(this.props.valueField) : model;
        } else {
            nextValue = isFunction(model.toJSON) ? model.toJSON() : model;
        }

        if (this.props.value) {
            event.sender.value(this.props.value);
        }

        this.props.onChange(nextValue);
    }

    getWidget() {
        return $(this.selectElement!).data('kendoDropDownList');
    }

    render() {
        const props = this.props.id ? { id: this.props.id } : {};

        return <select {...props} ref={ref => this.selectElement = ref} />;
    }
}

export default DropDownList;
