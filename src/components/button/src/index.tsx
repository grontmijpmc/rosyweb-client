import * as React from 'react';
import * as classNames from 'classnames';

export type ButtonType = 'default' | 'primary' | 'success' | 'info' | 'warning' | 'danger' | 'link';
export type State =  'active' | 'disabled';

function generateClassNames(buttonType: ButtonType, state?: State  , additionalClasses?: string) {
    let classes = classNames({
        btn: true,
        active: state === 'active',
        [`btn-${buttonType}`]: true,
    });

    if (additionalClasses) {
        classes = `${classes} ${additionalClasses}`;
    }

    return classes;
}

export interface Properties {
    buttonType?: ButtonType;
    state?: State;
    className?: string;
    onClick: () => void;
}

class Button extends React.Component<Properties, {}> {

    render() {

        const { className, onClick, buttonType = 'default', state, children, ...rest } = this.props;

        return (
            <a
                className={generateClassNames(buttonType, state, className)}
                role="button"
                // @ts-ignore
                disabled={state === 'disabled'}
                onClick={onClick}
                {...rest}
            >
                {this.props.children}
            </a>
        );
    };

}


export default Button;
