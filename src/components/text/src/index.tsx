import * as React from 'react';
import * as url from 'url';
import * as $ from 'jquery';
import request from '@rosy/utils-request';

// tslint:disable-next-line:no-var-requires
require('@progress/kendo-ui/js/kendo.autocomplete.js');

export type onChangeEventType = React.FormEvent<HTMLInputElement> | kendo.ui.AutoCompleteSelectEvent;

export interface Properties {
    onChange: (value: string, event: onChangeEventType) => void;
    maxLength?: number;
    autocompleteUrl?: string;
    readonly?: boolean;
    value?: string;
    placeholder?: string;
    style?: React.CSSProperties;
}

interface AutocompleteReturn {
    possibleValues: string[];
}

class KendoText extends React.Component<Properties, {}> {

    static defaultProps = {
        maxLength: 0,
    };

    textField: Element | undefined;

    componentDidMount() {
        if (this.props.autocompleteUrl) {
            this.mountAutocomplete(this.props.autocompleteUrl);
        }
    }

    componentWillUnmount() {
        if (this.props.autocompleteUrl) {
            this.unmountAutocomplete();
        }
    }

    mountAutocomplete = (autoCompleteUrl: string) => {
        const urlObject = url.parse(autoCompleteUrl, true);

        const ds = new kendo.data.DataSource({
            serverFiltering: true,
            transport: {
                read: ((options: kendo.data.DataSourceTransportReadOptions) => {
                    // cast options to any to avoid TS complaining about possible null
                    const requestTerm = (options as any).data.filter.filters[0].value || '_';
                    delete urlObject.search;
                    if (typeof urlObject.query !== "string") {
                        urlObject.query.Term = requestTerm;
                    }
                    const autocompleteUrl = url.format(urlObject);
                    request.ajax<AutocompleteReturn>({
                        url: autocompleteUrl,
                        type: 'json',
                    }).then(data => {
                        options.success(data.possibleValues);
                    }).catch(e => {
                        options.error('Fail :-(');
                        // tslint:disable-next-line no-console
                        console.error(e);
                    });
                // kendo typings are wrong as of 05-10-2017. Remove any cast when typings are ok
                }),
            },
        });

        const self = this;
        $(this.textField).kendoAutoComplete({
            select: function select(this: any, e) {
                const value = this.dataItem(e.item!.index());
                self.props.onChange(value, e);
            },
            dataSource: ds,
            minLength: 0,
        });
    }

    unmountAutocomplete = () => {
        const $el = $(this.textField);
        $el.data('kendoAutoComplete').destroy();
    }

    onChange = (event: React.FormEvent<HTMLInputElement>) => {
        if (this.props.readonly) return;
        const value = event.currentTarget.value;
        if (this.props.maxLength && value.length > this.props.maxLength) return;

        this.props.onChange(value, event);
    }

    setTextFieldReference = (ref: Element | null) => {
        if (ref) {
            this.textField = ref;
        }
    }

    render() {
        const { maxLength, onChange, value, autocompleteUrl, readonly, ...other } = this.props;

        return (
            <input
                {...other}
                className="k-textbox"
                value={value || ''}
                onChange={this.onChange}
                ref={this.setTextFieldReference}
            />
        );
    }
}

export default KendoText;