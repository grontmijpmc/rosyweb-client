import * as React from 'react';

export interface Properties {
    onClick?: () => void;
}

export default class Item extends React.Component<Properties, {}> {

    onClick = () => {
        if (this.props.onClick) {
            this.props.onClick();
        }
    }

    render() {
        return (
            <li className='cd-accordion-item' onClick={this.onClick}>
                {this.props.children}
            </li>
        );
    }
}
