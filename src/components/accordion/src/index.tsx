import * as React from 'react';
import * as $ from 'jquery';
import Group from './group';
import Item from './item';

export default class Accordion extends React.Component<{}, {}> {

    static Group = Group;
    static Item = Item;

    private element: Element | null | undefined;

    componentDidMount() {
        $(this.element!).on('change', '.cd-accordion-toggle', this.onToggle);
    }

    componentWillUnmount() {
        $(this.element!).off('change', '.cd-accordion-toggle', this.onToggle);
    }

    onToggle() {
        const checkbox = $(this);
        if (checkbox.prop('checked')) {
            checkbox.siblings('.cd-accordion-groupcontainer').attr('style', 'display:none;').slideDown(300);
        } else {
            checkbox.siblings('.cd-accordion-groupcontainer').attr('style', 'display:block;').slideUp(300);
        }
    }

    render() {
        return (
            <ul ref={el => this.element = el} className="cd-accordion-menu animated">
                {this.props.children}
            </ul>
        );
    }
}