import * as React from 'react';

export interface Properties {
    name: string;
    label: string;
    expanded: boolean;
    onExpanded?: (name: string) => void;
    onCollapsed?: (name: string) => void;
}

export default class Group extends React.Component<Properties, {}> {

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            if (this.props.onExpanded) {
                this.props.onExpanded(e.target.name);
            }
        } else {
            if (this.props.onCollapsed) {
                this.props.onCollapsed(e.target.name);
            }
        }
    }

    render() {
        const classname = this.props.expanded ? 'cd-accordion-toggle checked' : 'cd-accordion-toggle';
        return (
            <li className='cd-accordion-group has-children'>
                <input
                    type="checkbox"
                    className={classname}
                    name={this.props.name}
                    defaultChecked={this.props.expanded}
                    id={this.props.name}
                    onChange={this.handleChange}
                />
                <label className='cd-accordion-groupname' htmlFor={this.props.name}>{this.props.label}  </label>
                <ul className='cd-accordion-groupcontainer' >
                    {this.props.children}
                </ul>
            </li>
        );
    }
}