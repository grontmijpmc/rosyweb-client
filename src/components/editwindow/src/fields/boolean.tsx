import * as React from 'react';
import * as types from '././../types';
import { FormControl } from 'react-bootstrap';

export interface Properties {
    field: types.DataWindowField;
    placeholder: string;
    value: boolean;
    onChange: types.ChangeEvent;
    disabled?: boolean;
}

export default class BooleanField extends React.Component<Properties, {}> {

    render() {
        return (
            <FormControl
                componentClass="select"
                placeholder={this.props.placeholder}
                value={this.props.value.toString()}
            >
                <option value="" />
                <option value="true">Yes</option>
                <option value="false">No</option>
            </FormControl>
        );
    }
}