import * as React from 'react';
import * as types from '././../types';
import { FormGroup, FormControl, ControlLabel, HelpBlock, InputGroup } from 'react-bootstrap';
// import DatePicker from '@progress/kendo-ui/js/kendo.datepicker.js';

// tslint:disable-next-line:no-var-requires
const  DatePicker = require('@progress/kendo-ui/js/kendo.datepicker.js');

type ChangeEvent = (fieldid: string, value: any) => void;

export interface Properties {
    field: types.DataWindowField;
    placeholder: string;
    value: any;
    onChange: types.ChangeEvent;
    disabled?: boolean;
}

export default class TextField extends React.Component<Properties, {}> {

    onChange = (e: any) => {
        const event = e as React.ChangeEvent<HTMLInputElement>;
        const newValue = event.target.value;
        this.props.onChange(this.props.field.name, newValue);
    }

    render() {
        const { placeholder, value, onChange } = this.props;
        return (
            <FormControl
                placeholder={placeholder}
                value={value}
                onChange={this.onChange}
                componentClass={DatePicker}
                disabled={this.props.disabled}
            />
        );
    }
}
