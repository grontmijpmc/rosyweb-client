import * as React from 'react';
import { inject, observer } from 'mobx-react';
import * as types from '././../types';
import { FormGroup, InputGroup, FormControl } from 'react-bootstrap';
import Button from '@rosy/component-button';

type ChangeEvent = (fieldid: string, value: any) => void;

export interface RelationValue {
    id: types.Guid;
    name: string;
}

export interface Properties {
    field: types.DataWindowRelationField;
    placeholder: string;
    value: RelationValue;
    onChange: types.ChangeEvent;
    onOpenGrid: (field: types.DataWindowRelationField) => void;
}

@observer
export default class Relation extends React.Component<Properties, {}> {

    openGrid = () => {
        this.props.onOpenGrid(this.props.field);
    }

    render() {
        return (
            <div>
                <InputGroup>
                    <FormControl readOnly={true} value={this.props.value.name} placeholder={this.props.placeholder} />
                    <InputGroup.Button>
                        <Button onClick={this.openGrid}>
                            <span className="glyphicon glyphicon-folder-open" />
                        </Button>
                    </InputGroup.Button>
                </InputGroup>
            </div>
        );
    }
}