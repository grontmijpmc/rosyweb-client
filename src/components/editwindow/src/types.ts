export enum GuidBrand { }
export type Guid = GuidBrand & string;

export interface FormValues {
    [key: string]: FormValue;
}
export type FormValue = string | RelationValue;

export interface RelationValue {
    id: Guid;
    name: string;
}

export interface FormValuesWithId extends FormValues {
    id: Guid;
}

export interface Chooser {
    open: boolean;
    model: DataWindowModel | null | undefined;
    field: string | null;
}

export interface DataWindowModel {
    model: string;
    fields: DataWindowField[];
}
export interface DataWindowField {
    name: string;
    type: ServerColumnType;
    flags: string;
}

export interface DataWindowRelationField extends DataWindowField {
    model: string;
}

export interface DataWindow {
    category: DataWindowCategory;
    key: DataWindowKey;
}
export enum DataWindowKeyBrand { }
export type DataWindowKey = DataWindowKeyBrand & string;

export enum DataWindowCategoryBrand { }
export type DataWindowCategory = DataWindowCategoryBrand & string;

export type ServerColumnType = 'String' | 'Relation' | 'Lookup' | 'Enum' | 'Boolean' | 'Date' | 'Number';

export type ChangeEvent = (fieldid: string, value: any) => void;

export interface DataWindowEnumField extends DataWindowField {
    allValues: string[];
}
export type ServerColumnFlags = 'None' | 'Readonly' | 'Nullable' | 'OnlyInList';