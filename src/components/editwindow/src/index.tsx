import * as React from 'react';
import { observer, inject } from 'mobx-react';
import EditWindowStore from './store';
import { Modal } from 'react-bootstrap';
import Button from '../../button/';
import * as uuid from 'node-uuid';
import * as types from './types';
import Form from './form';
import Window from '@rosy/component-window';

export interface Properties {
    store: EditWindowStore;
    onInsertAsset: (model: types.DataWindowModel, values: types.FormValues) => void;
    onUpdateAsset: (model: types.DataWindowModel, values: types.FormValues) => void;
}

interface FormValues {
    [key: string]: any;
}

export default class EditWindow extends React.Component<Properties, {}> {

    formValues: types.FormValues = {};

    onChange = (values: types.FormValues) => {
        this.formValues = values;
    }

    save = () => {
        if (this.props.store.createOrUpdate === "create") {
            // Generate id
            const id = uuid.v4();
            this.formValues.id = id;
            this.props.onInsertAsset(this.props.store.model, this.formValues);
        } else {
            this.props.onUpdateAsset(this.props.store.model, this.formValues);
        }
    }

    hideModal = () => {
        this.props.store.hideWindow();
    }

    render() {
        return (
            <Window visible={this.props.store.visible} hideModal={this.hideModal}>
                    <Form
                        model={this.props.store.model}
                        onChange={this.onChange}
                        state={this.props.store.state}
                        store={this.props.store}
                    />
                    <div>
                    <Button buttonType="primary" onClick={this.save}>Save</Button>
                    </div>
            </Window>
        );
    }
}