import * as React from 'react';
import { FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

export interface Properties {
    id: string;
    label: string;
}

export default class Fieldset extends React.Component<Properties, {}> {

    render() {
        return (
            <FormGroup controlId={this.props.id}>
                <ControlLabel>{this.props.label}</ControlLabel>
                {this.props.children}
            </FormGroup>
        );
    }
}