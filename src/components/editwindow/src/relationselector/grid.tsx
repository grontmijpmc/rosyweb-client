import * as React from 'react';
import { inject, observer } from 'mobx-react';
import * as types from '././../types';

export interface Properties {
    model: types.DataWindowModel;
    onRowSelected: (id: types.Guid, description: string) => void;
}

function createKendoDataSource(model: types.DataWindowModel): kendo.data.DataSource {

    const fields: { [key: string]: any } = {};
    for (const field of model.fields) {
        fields[field.name] = createFieldConfiguration(field);
    }

    const schema: kendo.data.DataSourceSchema = {
        model: {
            id: 'id',
            fields,
        },
        total: 'total',
        data: 'data',
    };

    const ds = new kendo.data.DataSource({
        schema,
        // we have to handle dataoperations serverside because of potential large amounts of data
        pageSize: 100,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        serverGrouping: true,
        serverAggregates: true,
        transport: {
            read: {
                url: '/base/data',
                data: {
                    Model: model.model,
                    View: 'List',
                },
            },
        },
    });

    return ds;
}

function createFieldConfiguration(field: types.DataWindowField) {
    return { type: 'string' };
}

function createGridConfiguration(
    model: types.DataWindowModel,
    dataSource: kendo.data.DataSource,
    additionalOptions: Partial<kendo.ui.GridOptions> = {}) {

    const columns: kendo.ui.GridColumn[] = [{
        field: 'id',
        title: 'id',
        hidden: true,
        attributes: {
            class: 'Id',
        },
    }];

    for (const column of model.fields) {
        columns.push({
            field: column.name,
            title: column.name,
            hidden: false,
            attributes: {},
        });
    }

    const options: kendo.ui.GridOptions = Object.assign({
        dataSource,
        columns,
        selectable: 'row',
        navigatable: true,
        sortable: true,
        height: '100%',
        columnMenu: true,
        reorderable: true,
        pageable: true,
        filterable: {
            mode: 'row',
        },
    }, additionalOptions);

    return options;
}

export class DataWindow extends React.Component<Properties, {}> {
}
export function createReactComponent(model: types.DataWindowModel, dataSource: kendo.data.DataSource) {

    interface GridProperties {
        window: types.DataWindow;
        onRowSelected: (id: types.Guid, description: string) => void;
    }

    // tslint:disable:max-classes-per-file
    class DataWindowImpl extends React.Component<GridProperties, {}> {
        private readonly dataSource: kendo.data.DataSource;
        private readonly gridConfiguration: kendo.ui.GridOptions;

        private root: Element | null | undefined;

        constructor(props: GridProperties) {
            super(props);
            this.dataSource = dataSource;
            this.gridConfiguration = createGridConfiguration(model, dataSource, { navigate: this.onNavigate });
        }

        componentDidMount() {
            const $el = $(this.root!);
            const grid = $el.kendoGrid(this.gridConfiguration);
            $el.on("dblclick", "tbody>tr", this.selectRow);
        }

        componentWillUnmont() {
            const $el = $(this.root!);
            $el.off("dblclick", "tbody>tr", this.selectRow);
            $el.data('kendoGrid').destroy();
        }

        private onNavigate = (e: kendo.ui.GridNavigateEvent) => {
            // When navigating the grid using the keyboard, make the whole row selectable
            if (e.element) {
                const row = e.element.parent();
                e.sender.select(row);
            }
        }

        selectRow = (e: JQuery.Event<HTMLElement>) => {
            const selectedRow = $(e.delegateTarget).data('kendoGrid').select();
            const grid = $(this.root!).data('kendoGrid');
            const foo = grid.dataItem(selectedRow);
            this.props.onRowSelected(foo.get('id'), foo.get('name'));
        }

        render() {
            return <div ref={element => this.root = element} />;
        }
    }

    return DataWindow;
}

// tslint:disable:max-classes-per-file
export default class SelectorGrid extends React.Component<Properties, {}> {

    private component: any;

    constructor(props: Properties) {
        super(props);
        this.initializeGrid();
    }

    componentWillReceiveProps(newProps: Properties) {
        if (newProps.model !== this.props.model) {
            this.initializeGrid();
        }
    }

    private initializeGrid() {
        const dataSource = createKendoDataSource(this.props.model);
        this.component = createReactComponent(this.props.model, dataSource);
    }

    render() {
        const Component = this.component; // tslint:disable-line:variable-name
        const window: types.DataWindow = {
            category: (this.props.model as any) as types.DataWindowCategory,
            key: 'relationChooser' as types.DataWindowKey,
        };

        // const windowHeight = this.props.ui!.windowDimensions.height;
        const windowHeight = 1000;
        const selectorHeight = windowHeight / 2.5;
        return (
            <div style={{ height: selectorHeight }}>
                <Component window={window} onRowSelected={this.props.onRowSelected} />
            </div>
        );
    }
}