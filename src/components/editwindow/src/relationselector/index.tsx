import * as React from 'react';
import { inject, observer } from 'mobx-react';
import { Modal } from 'react-bootstrap';
import Button from '@rosy/component-button';
import * as types from '././../types';
import Grid from './grid';
import EditWindowStore from '././../store';

export interface Properties {
    store: EditWindowStore;
    onRelationSelected: (id: types.Guid, name: string) => void;
    onCloseRelationSelector: () => void;
}

@observer
export default class RelationSelector extends React.Component<Properties, {}> {

    hideModal = () => {
        this.props.onCloseRelationSelector();
    }

    select = (id: types.Guid, description: string) => {
        this.props.onRelationSelected(id, description);
    }

    render() {
        return (
            <Modal show={this.props.store.chooser.open} onHide={this.hideModal} bsSize="large">
                <Modal.Header closeButton={true}>
                    <Modal.Title>Select a Value</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Grid model={this.props.store.chooser.model!} onRowSelected={this.select}/>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.hideModal}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}