import { observable, computed, extendObservable } from 'mobx';
import * as types from './types';

/*
interface RelationValue {
    id: types.Guid;
    name: string;
}

type FormValue = string | RelationValue;

interface FormValues {
    [key: string]: FormValue;
}

export interface FormValuesWithId extends FormValues {
    id: types.Guid;
}

interface Chooser {
    open: boolean;
    model: types.DataWindowModel | null | undefined;
    field: string | null;
}

*/
function isGuid(input: any): input is types.Guid {
    return (typeof input === 'string');
}

export default class EditWindow {
    @observable private _visible = false;
    @observable.shallow private _model: types.DataWindowModel;
    @observable private _id: types.Guid | null = null;

    @observable state: types.FormValues = {};

    @observable chooser: types.Chooser = {
        open: false,
        model: null,
        field: null,
    };

    @computed get visible() {
        return this._visible;
    }

    @computed get model() {
        return this._model;
    }

    @computed get id() {
        return this._id;
    }

    @computed get createOrUpdate(): "create" | "update" {
        return this._id === null ? "create" : "update";
    }

    constructor(model: types.DataWindowModel) {
        this._model = model;
    }

    initState() {
        const obj: types.FormValues = {};
        for (const field of this._model.fields) {
            obj[field.name] = "";
        }
        this.state = observable(obj);
    }

    showCreateWindow(model: types.DataWindowModel) {
        this._visible = true;
        this._model = model;
        this._id = null;
        this.initState();
    }

    showEditWindow(model: types.DataWindowModel, id: types.Guid | types.FormValuesWithId) {
        this._visible = true;
        this._model = model;
        if (isGuid(id)) {
            this._id = id;
        } else {
            const data = id;
            this._id = data.id;
            extendObservable(this.state, data);
        }
    }

    hideWindow() {
        this._visible = false;
    }

    updateValue(fieldName: string, value: types.FormValue) {
        this.state[fieldName] = value;
    }

    showRelationChooser(field: types.DataWindowRelationField) {
        this.chooser.open = true;
        this.chooser.model = this._model;
        this.chooser.field = field.name;
    }

    closeRelationChooser() {
        this.chooser.open = false;
        this.chooser.model = null;
        this.chooser.field = null;
    }

    selectValueInRelationChooser(value: types.FormValue) {
        if (!this.chooser.field) {
            throw new Error("editWindow.selectValueInRelationChooser was called without relation chooser open");
        }

        this.state[this.chooser.field] = value;
    }
}