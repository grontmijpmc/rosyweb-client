import * as React from 'react';
import { observable, action, IAction, extendObservable } from 'mobx';
import { observer } from 'mobx-react';
import * as types from '././types';
import { FormGroup, FormControl, ControlLabel, HelpBlock, InputGroup } from 'react-bootstrap';
import Fieldset from './fieldset';
import Button from '@rosy/component-button';
import DropDownList from '@rosy/component-dropdownlist';
import RelationField from './fields/relation';
import TextField from './fields/text';
import DateField from './fields/date';
import BooleanField from './fields/boolean';
import EditWindowStore from './store';

type ChangeEvent = (fieldid: string, value: any) => void;

export interface Properties {
    model: types.DataWindowModel;
    state: types.FormValues;
    onChange?: (values: types.FormValues) => void;
    store: EditWindowStore;
}

interface FormValues {
    [key: string]: any;
}

function renderEnumField(field: types.DataWindowEnumField, placeholder: string, value: any, onChange: ChangeEvent) {

    function myOnChange(newValue: any) {
        onChange(field.name, newValue);
    }

    function MyDropDownList() {
        return (
            <div>
                <DropDownList
                    onChange={myOnChange}
                    dataSource={field.allValues}
                    id={field.name}
                    filter="none"
                    value={value}
                    width="100%"
                    animation={false}
                />
            </div>
        );
    }

    return (
        <FormControl
            componentClass={MyDropDownList}
        />
    );
}

function hasFlag(flags: string, flag: types.ServerColumnFlags): boolean {
    return flags.indexOf(flag) >= 0;
}

@observer
export default class Form extends React.Component<Properties, {}> {

    // @observable private values: FormValues;

    // FormControl has a wrong declaration for it's onChange handler
    // It should be React.FormEvent<HTMLInputElement>, but it is declared
    // as React.FormEvent<FormControlProps>
    onChange = action((fieldid: string, value: any) => {

        this.props.state[fieldid] = value;
        if (this.props.onChange) {
            this.props.onChange(this.props.state);
        }
    });

    onOpenGrid = (field: types.DataWindowRelationField) => {
        this.props.store.showRelationChooser(field);
    }

    renderField(field: types.DataWindowField, value: any, onChange: types.ChangeEvent) {
        const readonly = hasFlag(field.flags, 'Readonly');

        switch (field.type) {
            case 'String':
                return (
                    <TextField
                        field={field}
                        placeholder={field.name}
                        value={value}
                        onChange={onChange}
                        disabled={readonly}
                    />
                );
            case 'Relation':
                return (
                    <RelationField
                        field={field as types.DataWindowRelationField}
                        placeholder={field.name}
                        value={value}
                        onChange={onChange}
                        onOpenGrid={this.onOpenGrid}
                    />
                );
            case 'Date':
                return (
                    <DateField
                        field={field}
                        placeholder={field.name}
                        value={value}
                        onChange={onChange}
                        disabled={readonly}
                    />
                );
            case 'Boolean':
                return (
                    <BooleanField
                        field={field}
                        placeholder={field.name}
                        value={value}
                        onChange={onChange}
                        disabled={readonly}
                    />
                );
            default:
                return <TextField field={field} placeholder={field.name} value={value} onChange={onChange} />;
        }
    }

    renderFields = (field: types.DataWindowField) => {
        return (
            <Fieldset id={field.name} label={field.name} key={field.name}>
                {this.renderField(field, this.props.state[field.name], this.onChange)}
            </Fieldset>
        );
    }

    render() {
        return (
            <form>
                {this.props.model.fields.map(this.renderFields)}
            </form>
        );
    }
}