import * as React from 'react';
import ToolBar from '@rosy/component-toolbar';

export interface Properties {
    callPinWindow?: () => void;
}

export default class TabToolBar extends React.Component<Properties, {}> {

    pinWindow = () => {
       if (this.props.callPinWindow) {
           this.props.callPinWindow();
       }
    }

    toolbarOptions: kendo.ui.ToolBarOptions = {
        items: [
            { type: "button", text: "Pin window", click: this.pinWindow },
        ],
    };

    render() {
        return (
            <ToolBar options={this.toolbarOptions} />
        );
    }
}