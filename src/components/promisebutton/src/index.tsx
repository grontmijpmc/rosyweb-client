import * as React from 'react';
import { observer } from 'mobx-react';
import { observable, action, IAction } from 'mobx';
import { Promise } from 'bluebird';

const VISIBLE = { display: 'inline-block' };
const INVISIBLE = { display: 'none' };

export interface Properties {
    label?: string;
    className?: string;
    disabled?: boolean;
    terminateChain?: boolean;
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => any;
}

@observer
class PromiseButton extends React.Component<Properties, {}> {

    @observable busy = false;

    static defaultProps = {
        className: 'k-button',
        disabled: false,
        terminateChain: true,
        label: '',
    };

    onSettled = action(() => {
        this.busy = false;
    });

    @action clickHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
        const maybePromise = this.props.onClick(event);

        this.busy = true;

        const promise = Promise.resolve(maybePromise).finally(this.onSettled);

    }

    render() {
        return (
            <button className={this.props.className} disabled={this.props.disabled} onClick={this.clickHandler}>
                {this.props.label || this.props.children}
                <i className="k-loading" style={this.busy ? VISIBLE : INVISIBLE} />
            </button>
        );
    }
}

export default PromiseButton;
