import * as React from 'react';
import * as types from './types';
import * as datamodel from './datamodel';
import { IObservableArray, observable, action, computed } from 'mobx';
import { WindowConfiguration } from './index';
import { createStore } from './configurationfactory';
import { createReactComponent } from './createcomponent';

let configuration: datamodel.DataWindowModel[] = [];

function readConfiguration(c: datamodel.DataWindowModel[]) {
    configuration = c;
}

export function getDataModel(category: types.DataWindowCategory): datamodel.DataWindowModel {
    const model = configuration.find(x => x.model === category);
    if (!model) {
        throw new Error(`model ${category} not found in configuration`);
    }
    return model;
}

interface MapGeometries {
    category: string;
    geometries: types.MapFeature[];
}

interface Window {
    identifier: datamodel.DataWindow;
    refCount: number;

}

export interface WindowConfiguration {
    identifier: datamodel.DataWindow;
    store: datamodel.DataWindowStore;
    model: datamodel.DataWindowModel;
    title: [string, string];
}

export default class DataWindowManager {
    private _selectedRoad: types.RoadId | null = null;
    private _activeWindows: IObservableArray<Window> = observable([]);
    private _configuration: WindowConfiguration[] = [];
    private _mapGeometries: MapGeometries[] = [];

    setSelectedRoad(road: types.RoadId) {
        this._selectedRoad = road;
        this.loadNewRoadInActiveWindows(road);
    }

    resetSelectedRoad() {
        this._selectedRoad = null;
        this.loadNewRoadInActiveWindows(null);
    }

    @action activateWindow(
        window: datamodel.DataWindow,
        filterString: string,
        title: [string, string],
        onInitialized: (callback: WindowConfiguration) => void) {

        if (!this.isWindowActive(window)) {
            this._activeWindows.push({ identifier: window, refCount: 1 });
            const model = getDataModel(window.category);
            const store: datamodel.DataWindowStore = createStore(model, filterString, title);
            const config: WindowConfiguration = {
                identifier: window,
                store,
                model,
                title,
            };
            this._configuration.push(config);

            onInitialized(config);
        } else {
            const w = this.getWindow(window);
            w.refCount++;
        }
    }

    @action deactivateWindow(window: datamodel.DataWindow) {
        const w = this.getWindow(window);
        if (w.refCount > 0) {
            w.refCount--;
        }

        if (w.refCount <= 0) {
            // Window is completely closed
            // Remove configuration to allow the store to
            // be GC'ed
            this._activeWindows.remove(w);
            const index = this._configuration.findIndex(x => this.identifierMatch(x.identifier, window));
            if (index > -1) {
                this._configuration.splice(index, 1);
            }
        }
    }

    @action pinWindow(window: datamodel.DataWindow) {
        const w = this.getWindow(window);
        w.refCount++;
    }

    getTitle(window: datamodel.DataWindow): [string, string] {
        const conf =  this._configuration.find(x => this.identifierMatch(x.identifier, window));
        if (!conf) {
            throw new Error("Model not found");
        }
        return conf.title;
    }

    getReactElement(window: datamodel.DataWindow,
                    hideRoadInfo: boolean,
                    pinned: boolean,
                    callOnShowUpdate: () => void) {
        const model = getDataModel(window.category);
        const c = this.getConfiguration(window);
        const Component = createReactComponent(window, // tslint:disable-line:variable-name
            c.model,
            c.store,
            hideRoadInfo,
            pinned,
            [],
        );
        return (
            <Component
                onShowUpdateWindow={this.onShowUpdateWindow}
                onDataGridNavigate={this.onDataGridNavigate}
                onDeactivatePinnedWindow={this.deactivateWindow}
                onPinWindow={this.pinWindow}
            />);

        }

    onShowUpdateWindow(window: types.DataWindow, data: types.FormValuesWithId): void {
    }

    onDataGridNavigate(window: types.DataWindow, id: string): void {
    }

    @computed get activeWindows(): datamodel.DataWindow[] {
        return this._activeWindows.filter(x => x.refCount > 0).map(x => x.identifier);
    }

    @computed get hasPinnedWindows(): boolean {
        return this._activeWindows.filter(x => x.refCount > 1).length > 0;
    }

    @computed get pinnedWindows(): datamodel.DataWindow[] {
        return this._activeWindows.filter(x => x.refCount > 1).map(w => w.identifier);
    }

    private getStore(category: types.DataWindowCategory): datamodel.DataWindowStore {
        const c = this._configuration.find(x => x.identifier.category === category);
        if (!c) {
            throw new Error("Model not found");
        }
        return c.store;

    }

    private getWindow(window: datamodel.DataWindow): Window {
        const w = this._activeWindows.find(x => this.identifierMatch(x.identifier, window));
        if (!w) {
            throw new Error(`Window ${window.category} ${window.key} not found in _activeWindows`);
        }
        return w;
    }

    private getConfiguration(window: datamodel.DataWindow): WindowConfiguration {
        const c = this._configuration.find(x => this.identifierMatch(x.identifier, window));
        if (!c) {
            throw new Error("Model not found");
        }
        return c;
    }

    private identifierMatch(w1: datamodel.DataWindow, w2: datamodel.DataWindow) {
        return w1.category === w2.category && w1.key === w2.key;
    }

    private isWindowActive(window: datamodel.DataWindow): boolean {
        if (!this._activeWindows.find(x => this.identifierMatch(x.identifier, window))) {
            return false;
        }
        return this.getWindow(window).refCount > 0;
    }

    private loadNewRoadInActiveWindows(selectedRoad: types.RoadId | null) {
        for (const window of this.activeWindows) {
            const model = getDataModel(window.category);
            if (model) {
                const config = this.getConfiguration(window);
                config.store.loadData(selectedRoad);
            }
        }
    }

    public callOnActiveWindows(callBack: (x: WindowConfiguration) => void) {
        for (const window of this.activeWindows) {
            const model = getDataModel(window.category);
            if (model) {
                const config = this.getConfiguration(window);
                callBack(config);
            }
        }
    }
}