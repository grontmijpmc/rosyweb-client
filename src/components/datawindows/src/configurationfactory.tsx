import * as React from 'react';
import { inject, observer } from 'mobx-react';
import * as types from './types';
import * as moment from 'moment';
import { getDataModel } from './datawindowmanager';
moment.locale('da');
import ColumnsCustomizerFactory from './columnscustomizers/columnscustomizersfactory';
import CreateFieldDefinition from './fieldsfactory';
import CreateColumnDefinition from './columnsfactory';

function createKendoDataSource(model: types.DataWindowModel, filterString: string): kendo.data.DataSource {

    const fields = CreateFieldDefinition(model);

    const schema: kendo.data.DataSourceSchema = {
        model: {
            id: 'id',
            fields,
        },
        total: 'total',
        data: 'data',
    };

    const ds = new kendo.data.DataSource({
        schema,
        // we have to handle dataoperations serverside because of potential large amounts of data
        pageSize: 100,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        serverGrouping: true,
        serverAggregates: true,
        transport: {
            read: {
                url: '/base/data',
                data: {
                    Model: model.model,
                    View: 'List',
                    FilterString: filterString,
                },
            },
        },
    });

    return ds;
}

export function createStore(model: types.DataWindowModel,
                            filterString: string,
                            title: string[])
                            : types.DataWindowStore {
    const store: types.DataWindowStore = {
        dataSource: createKendoDataSource(model, filterString),

        loadData: function loadData(roadId: types.RoadId | null): void {
            // Kendo typings is wrong
            if (roadId) {
                (this.dataSource.options.transport!.read as any)!.data.RoadId = roadId;
            } else {
                (this.dataSource.options.transport!.read as any)!.data.RoadId = null;
            }
            this.dataSource.read();
        },

        setSelectedMapFeature: function setSelectedMapFeature(input: types.SelectedMapFeature): void {
            console.log(input);
            // do something
        },

        title,

    };

    return store;

}

export function createGridConfiguration(
    model: types.DataWindowModel,
    store: types.DataWindowStore,
    hideRoadInfo: boolean,
    additionalOptions: Partial<kendo.ui.GridOptions> = {},
    models: types.DataWindowModel[]) {

    const columns = CreateColumnDefinition(model, hideRoadInfo);

    const options: kendo.ui.GridOptions = Object.assign({
        dataSource: store.dataSource,
        columns,
        selectable: 'row',
        navigatable: true,
        sortable: {
            mode: "multiple",
            allowUnsort: true,
            showIndexes: true,
        },
        resizable: true,
        height: '100%',
        columnMenu: true,
        reorderable: true,
        pageable: true,
    }, additionalOptions);

    if (model.relatedModel) {

        const relatedModel = models.find(x => x.model === model.relatedModel);
        if (!relatedModel) {
            throw new Error(`model ${model.relatedModel} not found in configuration`);
        }
        const relfields = CreateFieldDefinition(relatedModel);

        const schema: kendo.data.DataSourceSchema = {
            model: {
                relfields,
            },
            data: 'data',
        };

        const relds = new kendo.data.DataSource({
            schema,
            transport: {
                read: {
                    url: '/base/data',
                    data: {
                        Model: model.relatedModel,
                        View: 'List',
                    },
                },
            },
        });

        const relColumns = CreateColumnDefinition(relatedModel, hideRoadInfo);
        options.detailInit = (e: kendo.ui.GridDetailInitEvent) => {
            (relds.options.transport!.read as any)!.data.Id = (e.data! as any).id;
            $("<div/>").appendTo(e.detailCell!).kendoGrid({
                dataSource: relds,
                columns: relColumns,
                scrollable: true,
            });
            relds.read();
        };
    }
    return options;
}
