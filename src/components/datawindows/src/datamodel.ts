import { ServerColumnType, DataWindowCategory, DataWindowKey, RoadId, Guid } from "./types";

export interface DataWindowField {
    name: string;
    type: ServerColumnType;
    flags: string;
}

export interface DataWindowEnumField extends DataWindowField {
    allvalues: string[];
}

export interface DataWindowModel {
    model: string;
    relatedModel: string;
    listView: string;
    editView: string;
    fields: Array<DataWindowField | DataWindowEnumField>;
}

export interface DataWindow {
    category: DataWindowCategory;
    key: DataWindowKey;
}
export interface SelectedMapFeature {
    id: Guid;
    category: DataWindowCategory;
}

export interface DataWindowStore {
    loadData(roadId: RoadId | null): void;
    setSelectedMapFeature(input: SelectedMapFeature): void;
    readonly dataSource: kendo.data.DataSource;
    title: string[];
}