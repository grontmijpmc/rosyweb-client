export type ServerColumnType = 'String' | 'Relation' | 'Lookup' | 'Enum' | 'Boolean' | 'Date' | 'Number';
export type ServerColumnFlags = 'None' | 'Readonly' | 'Nullable' | 'OnlyInList';

export enum DataWindowKeyBrand { }
export type DataWindowKey = DataWindowKeyBrand & string;

export enum DataWindowCategoryBrand { }
export type DataWindowCategory = DataWindowCategoryBrand & string;

export interface MapFeature {
    id: string;
    wkts: string[];
}

export enum GuidBrand { }
export type Guid = GuidBrand & string;

export enum RoadIdBrand { }
export type RoadId = RoadIdBrand & Guid;

export interface DataWindow {
    category: DataWindowCategory;
    key: DataWindowKey;
}

export interface DataWindowModel {
    model: string;
    relatedModel: string;
    listView: string;
    editView: string;
    fields: DataWindowField[];
}
export interface DataWindowField {
    name: string;
    type: ServerColumnType;
    flags: string;
}
export interface DataWindowStore {
    loadData(roadId: RoadId | null): void;
    setSelectedMapFeature(input: SelectedMapFeature): void;
    readonly dataSource: kendo.data.DataSource;
    title: string[];
}
export interface SelectedMapFeature {
    id: Guid;
    category: DataWindowCategory;
}
export interface FormValues {
    [key: string]: FormValue;
}

export interface FormValuesWithId extends FormValues {
    id: Guid;
}

export type FormValue = string | RelationValue;

export interface RelationValue {
    id: Guid;
    name: string;
}
export interface Dimensions {
    width: number;
    height: number;
}
