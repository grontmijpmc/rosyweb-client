import * as React from 'react';
import * as types from './types';
import * as $ from 'jquery';
import PinnedToolBar from '@rosy/component-pinnedtoolbar';
import TabToolBar from '@rosy/component-tabtoolbar';
import { createGridConfiguration } from './configurationfactory';

export interface Properties {
    gridOptions?: Partial<kendo.ui.GridOptions>;
    container?: types.Dimensions;
    onShowUpdateWindow: (window: types.DataWindow, data: types.FormValuesWithId) => void;
    onDataGridNavigate: (window: types.DataWindow, id: string) => void;
    onDeactivatePinnedWindow: (window: types.DataWindow) => void;
    onPinWindow: (window: types.DataWindow) => void;
}

// Must redeclare class so Typescript is able to
// put it in a declaration file
export class DataWindow extends React.Component<Properties, {}> {
}

export function createReactComponent(
    window: types.DataWindow,
    model: types.DataWindowModel,
    store: types.DataWindowStore,
    hideRoadInfo: boolean,
    pinned = false,
    models: types.DataWindowModel[],
    ): typeof DataWindow {

    // tslint:disable:max-classes-per-file
    class DataWindowImpl extends DataWindow {
        private readonly dataSource: kendo.data.DataSource;
        private readonly gridConfiguration: kendo.ui.GridOptions;

        private root: Element | null | undefined;

        constructor(props: Properties) {
            super(props);
            this.dataSource = store.dataSource;
            this.gridConfiguration = createGridConfiguration(
                                            model,
                                            store,
                                            hideRoadInfo,
                                            { navigate: this.onNavigate },
                                            models);
        }

        componentDidMount() {
            const $el = $(this.root!);
            const grid = $el.kendoGrid(this.gridConfiguration);
            $el.on("dblclick", "tbody>tr", this.selectRow );
        }

        componentWillUnmont() {
            const $el = $(this.root!);
            $el.off("dblclick", "tbody>tr", this.selectRow);
            $el.data('kendoGrid').destroy();
        }

        private onNavigate = (e: kendo.ui.GridNavigateEvent) => {
            // When navigating the grid using the keyboard, make the whole row selectable
            if (e.element) {
                const row = e.element.parent();
                e.sender.select(row);
                const id = row.find('td.Id').text();
                this.props.onDataGridNavigate(window, id);
            }
        }

        selectRow = (e: JQuery.Event<HTMLElement>) => {
            const selectedRow = $(e.delegateTarget).data('kendoGrid').select();
            const grid = $(this.root!).data('kendoGrid');
            const selectedData = grid.dataItem(selectedRow);
            this.props.onShowUpdateWindow(window, selectedData.toJSON() as types.FormValuesWithId);
        }

        pinnedToolBarOnClose = () => {
            this.props.onDeactivatePinnedWindow(window);
        }

        tabtoolbarPinWindow = () => {
            this.props.onPinWindow(window);
        }

        render() {
            return (
                <div style={{ height: '100%' }}>
                    {
                        pinned
                            ? <PinnedToolBar  title={store.title} callOnClosePinnedWindow={this.pinnedToolBarOnClose}/>
                            : <TabToolBar callPinWindow={this.tabtoolbarPinWindow} />
                    }
                    <div ref={element => this.root = element} />
                </div>
            );
        }
    }

    return DataWindow;
}
