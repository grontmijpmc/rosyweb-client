import * as types from '././../types';
import * as moment from 'moment';
import { createStyledColumn } from './columnscustomizersfactory';

function getStyle(resudiallifetime: number) {
    if (resudiallifetime < moment().year()) {
        return "background-color: #76abd9";
    } else {
        return "background-color: #dddddd";
    }
}

export default function DamageSectionsColumns(column: kendo.ui.GridColumn) {
    if (column.field === 'calcresudiallifetime') {
        column.template = createStyledColumn(column, getStyle);
    }
}
