import { GridColumnCustomizer } from './columnscustomizers/columnscustomizersfactory';

import DamageSectionCustomizer from './columnscustomizers/damagesection';

export interface ColumnCustomizers {
    [model: string]: GridColumnCustomizer;
}

export const columnCustomizers: ColumnCustomizers = {
    DamageSection: DamageSectionCustomizer,
};
