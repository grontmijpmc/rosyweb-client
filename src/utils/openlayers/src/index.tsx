// tslint:disable:no-internal-module
import * as ol from 'openlayers';
import * as proj4x from 'proj4';
const proj4 = (proj4x as any).default;

ol.proj.setProj4(proj4);

proj4.defs(
    'EPSG:25832',
    '+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
);

const dkProjection = new ol.proj.Projection({
    code: 'EPSG:25832',
    extent: [120000, 5661139.2, 958860.8, 6500000],
    units: 'm',
});

ol.proj.addProjection(dkProjection);

export type FilterPredicate = (feature: ol.Feature) => boolean;

declare module 'openlayers' {

    module source {
        interface Vector {
            getClosestFeatureWithinDistance(
                coordinate: ol.Coordinate, distance: number, filter: FilterPredicate): ol.Feature | null;
        }
    }

    interface Map {
        // Typings is wrong. Missed null possibility
        setTarget(target: string | Element | null): void;
    }

}

ol.source.Vector.prototype.getClosestFeatureWithinDistance = function getClosestFeatureWithinDistance(
    coordinate: ol.Coordinate, distance: number, filter: FilterPredicate = () => true): ol.Feature | null {

        // Find the closet feature with in the given distance
        // created from ol.source.Vector.prototype.getClosestFeatureToCoordinate
    const x = coordinate[0];
    const y = coordinate[1];
    let closestFeature: ol.Feature | null = null;
    let previousCityBlockDistance = Infinity;
    const extent: ol.Extent = [x - distance, y - distance, x + distance, y + distance];
    this.forEachFeatureInExtent(extent, feature => {
        if (filter(feature)) {
            const geo = feature.getGeometry();
            const coord = geo.getClosestPoint(coordinate);
            const minCityBlockDistance = Math.abs(x - coord[0]) + Math.abs(y - coord[1]);
            if (minCityBlockDistance <= distance
                && minCityBlockDistance < previousCityBlockDistance) {
                previousCityBlockDistance = minCityBlockDistance;
                closestFeature = feature;
            }
        }
    });
    return closestFeature;
};

export default ol;
