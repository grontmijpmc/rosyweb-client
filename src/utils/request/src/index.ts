import { Promise } from 'bluebird';
import * as Reqwest from 'reqwest';

export type Method = 'GET' | 'POST';

export interface Options {
    url: string;
    method?: Method;
    headers?: object;
    data?: string | object;
    type?: string;
    contentType?: string;
    crossOrigin?: boolean;
    success?: (response: any) => void;
    error?: (error: any) => void;
    complete?: (response: any) => void;
    jsonpCallback?: string;
}

function ajax<T>(config: Options | string): Promise<T> {
    const p = new Promise<T>((resolve, reject) => {
        if (typeof config === "string") {
            config = { url: config };
        }

        config.success = resolve;
        config.error = reject;

        Reqwest(config);
    });
    return p as any;
}

export default {
    ajax,
};