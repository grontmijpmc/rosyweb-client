import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { useStrict } from 'mobx';
import Application from './application';

useStrict(true);
// initialize store
// import store from './store';
// store.init();

require('../style/main.less'); // tslint:disable-line:no-var-requires

ReactDOM.render(<Application />, document.getElementById('container'));