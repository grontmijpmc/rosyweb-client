import * as React from 'react';
import * as $ from 'jquery';
import { observer, Provider } from 'mobx-react';
import { debounce } from 'lodash-es';
// import MapComponent from './components/map/map.js';
import { Splitter, Pane } from '@rosy/component-splitter';
import MapComponent from './components/map/newmap2';
import Legend from './components/legend';
import Menu from './components/menu';

import store from './store';
import Actions from './actions';
const actions = new Actions(store);
import * as types from './types';

import Roadlist from './components/roadlist';
import Filter from './components/filter/filter';
import Leftbar from './layout/leftbar';
import MainTabs from './layout/maintabs';
import FilterWorkingset from './components/filter/workingset/workingset';

function updatePaneSizes() {
    const $window = $(window);
    const element = $(".k-splitter:first");
    const roadListWidth = element.children(".k-pane:first").width() || 0;
    const roadListHeight = $window.height() || 0;

    const roadList = {
        visible: true,
        size: {
            width: roadListWidth,
            height: roadListHeight,
        },
    };

    const rightElements = element.children(".k-pane:last");
    const mapAndWindowsWidth = rightElements.width() || 0;

    const innerPanes = $(".k-pane", rightElements);

    const mapElement = innerPanes.first();

    const mapHeight = mapElement.height() || 0;

    const map = {
        visible: true,
        size: {
            width: mapAndWindowsWidth,
            height: mapHeight,
        },
    };

    const windowsHeight = (innerPanes
        .filter((index, el) => (index > 0))
        .map((index, el) => $(el).height())
        .get() as any) as number[];

    const windowDimensions = windowsHeight.map(h => ({
        visible: true,
        size: {
            width: mapAndWindowsWidth,
            height: h,
        },
    }));

    actions.setPaneSizes({
        roadList,
        map,
        windows: windowDimensions,
    });
}

const debouncedUpdatePaneSizes = debounce(updatePaneSizes, 400);

function updateWindowDimensions() {
    const $window = $(window);
    const width = $window.width();
    const height = $window.height();

    if (width && height) {
        actions.setWindowDimensions({ width, height });
        updatePaneSizes();
    }
}

const debouncedUpdateWindowDimension = debounce(updateWindowDimensions, 400);

$(window).ready(() => {
    updateWindowDimensions();
    const splitters = $('.k-splitter');
    splitters.each((index, element) => {
        const splitter = $(element).data('kendoSplitter');
        splitter.resize(true);
    });

    $(window).resize(debouncedUpdateWindowDimension);
});

@observer
class Application extends React.Component<{}, {}> {

    render() {
        return (
            <Provider actions={actions} ui={store.components.ui}>
                <div/>
            </Provider>
        );
    }
}

export default Application;
